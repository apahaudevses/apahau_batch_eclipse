#!/bin/sh

# -----------------------------------------
# file_get.sh
# -----------------------------------------
SHELL_ORIGINAL1="sh ./shell/file_get.sh"

# -----------------------------------------
# convert.sh
# -----------------------------------------
SHELL_ORIGINAL2="sh ./shell/convert.sh"

# -----------------------------------------
# shop_update.sh
# -----------------------------------------
SHELL_ORIGINAL3="sh ./shell/shop_update.sh"


# -----------------------------------------
# csvリスト生成
# 引数　local/dev/batch サーバタイプ
#　標準出力に
# -----------------------------------------
SHELL1="sh ./shell/all_list.sh dev"

# -----------------------------------------
# update文作成/実行
# 引数1 local/dev/batch サーバタイプ
# 引数2 db 出力先
# -----------------------------------------
SHELL2="java -jar update.jar dev db"

# -----------------------------------------
# insert文作成/実行
# 引数1 local/dev/batch サーバタイプ
# 引数2 db/text 出力先
# -----------------------------------------
SHELL3="java -jar insert.jar dev db"
#SHELL3="java -jar insert.jar dev text"

# -----------------------------------------
# delete文作成/実行
# 引数1 local/dev/batch サーバタイプ
# 引数2 db/db_physic/text 出力先
# -----------------------------------------
#SHELL4="java -jar delete.jar dev db"
SHELL4="java -jar delete.jar dev db_physic"
#SHELL4="java -jar delete.jar dev text"

BASE_DIR=$(cd $(dirname $0);pwd)
cd ${BASE_DIR}


# 処理したいコマンド変数をつなげる
#${SHELL_ORIGINAL1} && ${SHELL_ORIGINAL2} && ${SHELL_ORIGINAL3} && ${SHELL1} && ${SHELL2} && ${SHELL3} && ${SHELL4}

#${SHELL_ORIGINAL2} && ${SHELL_ORIGINAL3} && ${SHELL1} && ${SHELL2} && ${SHELL3} && ${SHELL4}

${SHELL_ORIGINAL2} && ${SHELL_ORIGINAL3} && ${SHELL1} && ${SHELL2} && ${SHELL3}
#${SHELL1} && ${SHELL2} && ${SHELL3} && ${SHELL4}

#!/bin/sh

#変数宣言
CSV_INPUT=./input_csv/asn_update_bukken.csv
SORT_FILE=./shell/tmp/sort.csv

#csvソート
LC_ALL=C sort $CSV_INPUT > $SORT_FILE
#server指定(MYSQL)
SERVER=$1
#条件分岐(サーバ設定)
echo $SERVER

if [ "${SERVER}" = "local" ]
then
	echo "local"
elif [ "${SERVER}" = "dev" ]
then
	echo "dev"
	# ---------------------
	#INSERT(リスト生成)
	# ---------------------
	mysql -h localhost -uroot -pmode183sen -N apahau_temp -e "SELECT ID FROM T_Apaman LEFT JOIN apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id WHERE T_Housing.Property_Management_Id is null GROUP BY ID;" > ./shell/tmp/ids_insert.txt

	# ---------------------
	#UPDATE(リスト生成)
	# ---------------------
	#mysql -h localhost -uroot -pmode183sen -N apahau_temp -e "select ID from T_Apaman left join apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id WHERE T_Apaman.Update_Date >= T_Housing.Converter_Update_Date;" > ./shell/tmp/ids_update.txt
	mysql -h localhost -uroot -pmode183sen -N apahau_temp -e "select ID from T_Apaman inner join apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id WHERE T_Apaman.Update_Date >= T_Housing.Converter_Update_Date;" > ./shell/tmp/ids_update.txt
#	UPDATE_DATE日付で絞らない
#	mysql -h localhost -uroot -pmode183sen -N apahau_temp -e "select ID from T_Apaman left join apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id" > ./shell/tmp/ids_update.txt

	# ---------------------
	#DELETE(リスト生成)
	# ---------------------
	CSV_OUTPUT=./input_csv/delete.csv
	#mysql -h localhost -uroot -pmode183sen -N apahau_chintai -e "SELECT Property_Management_Id FROM apahau_chintai.T_Housing LEFT JOIN apahau_temp.T_ApamanAll ON T_Housing.Property_Management_Id = T_ApamanAll.ID WHERE T_ApamanAll.ID is null AND T_Housing.Delete_FLG = 0 GROUP BY Property_Management_Id;" > $CSV_OUTPUT
	#キッカワさんが作っていたやつ
	mysql -h localhost -uroot -pmode183sen -N apahau_chintai -e "SELECT Housing_Id FROM apahau_chintai.T_Housing LEFT JOIN apahau_temp.T_Apaman ON T_Housing.Property_Management_Id = T_Apaman.ID WHERE T_Apaman.ID is null AND T_Housing.Converter_id = 'apaman' AND T_Housing.Delete_FLG = '0';" > $CSV_OUTPUT
elif [ "${SERVER}" = "batch" ]
then

	echo "batch"
	# ---------------------
	#INSERT(リスト生成)
	# ---------------------
	#mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "SELECT ID FROM T_Apaman LEFT JOIN apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id WHERE T_Housing.Property_Management_Id is null GROUP BY ID;" > ./shell/tmp/ids_insert.txt
	mysql -h localhost -uroot -pmode183sen -N apahau_temp -e "SELECT ID FROM T_Apaman LEFT JOIN apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id WHERE T_Housing.Property_Management_Id is null GROUP BY ID;" > ./shell/tmp/ids_insert.txt

	# ---------------------
	#UPDATE(リスト生成)
	# ---------------------
	#mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "select ID from T_Apaman left join apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id WHERE T_Apaman.Update_Date >= T_Housing.Converter_Update_Date;" > ./shell/tmp/ids_update.txt
	mysql -h localhost -uroot -pmode183sen -N apahau_temp -e "select ID from T_Apaman left join apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id WHERE T_Apaman.Update_Date >= T_Housing.Converter_Update_Date;" > ./shell/tmp/ids_update.txt
#	UPDATE_DATE日付で絞らない
#	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "select ID from T_Apaman left join apahau_chintai.T_Housing ON T_Apaman.ID = T_Housing.Property_Management_Id" > ./shell/tmp/ids_update.txt

	# ---------------------
	#DELETE(リスト生成)
	# ---------------------
	CSV_OUTPUT=./input_csv/delete.csv
	#mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "SELECT Property_Management_Id FROM apahau_chintai.T_Housing LEFT JOIN apahau_temp.T_ApamanAll ON T_Housing.Property_Management_Id = T_ApamanAll.ID WHERE T_ApamanAll.ID is null AND T_Housing.Delete_FLG = 0 GROUP BY Property_Management_Id;" > $CSV_OUTPUT
	#キッカワさんが作っていたやつ
	#mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "SELECT Housing_Id FROM apahau_chintai.T_Housing LEFT JOIN apahau_temp.T_Apaman ON T_Housing.Property_Management_Id = T_Apaman.ID WHERE T_Apaman.ID is null AND T_Housing.Converter_id = 'apaman' AND T_Housing.Delete_FLG = '0';" > $CSV_OUTPUT
	mysql -h localhost -uroot -pmode183sen -N apahau_temp -e "SELECT Housing_Id FROM apahau_chintai.T_Housing LEFT JOIN apahau_temp.T_Apaman ON T_Housing.Property_Management_Id = T_Apaman.ID WHERE T_Apaman.ID is null AND T_Housing.Converter_id = 'apaman' AND T_Housing.Delete_FLG = '0';" > $CSV_OUTPUT
else
	echo "else"
fi

# ------------------
# INSERT CSV
# ------------------
CSV_OUTPUT=./input_csv/insert.csv

#セレクトしたcsvファイル生成(INSERT)
cat ./shell/tmp/ids_insert.txt | while read bukken_id
do
	echo `look \"${bukken_id}\" ${SORT_FILE}`
done > $CSV_OUTPUT

# ------------------
# UPDATE CSV
# ------------------
CSV_OUTPUT=./input_csv/update.csv

#セレクトしたcsvファイル生成(UPDATE)
cat ./shell/tmp/ids_update.txt | while read bukken_id
do
	echo `look \"${bukken_id}\" ${SORT_FILE}`
done > $CSV_OUTPUT

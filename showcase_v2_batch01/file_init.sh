#!/bin/bash

DATA_DIR=/export/data/showcase-tv/chintai/
CSV_FILE=20140910_showwork_22.csv
SHOP_FILE=20140910_showwork_shop_22.csv
IMAGE_FILE=image/20140910_showwork_22.zip

TARGET_DIR=/export/apahau_batch/showcase/showwork/

cp ${DATA_DIR}${CSV_FILE} ${TARGET_DIR}
cp ${DATA_DIR}${SHOP_FILE} ${TARGET_DIR}
cp ${DATA_DIR}${IMAGE_FILE} ${TARGET_DIR}

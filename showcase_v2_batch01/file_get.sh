#!/bin/bash

#rm -f sent

CSV_TEMP=showwork/20140910_showwork.csv
SHOP_TEMP=showwork/20140910_showwork_shop.csv
IMAGE_TEMP=20140910_showwork

/bin/rm -r showwork/showwork/* > /dev/null 2>&1
cp ${CSV_TEMP} showcase_bukken.csv
cp ${SHOP_TEMP} showcase_shop.csv
unzip -o showwork/${IMAGE_TEMP}.zip > /dev/null
mv ${IMAGE_TEMP}/* showwork/showwork
/bin/rm -r ${IMAGE_TEMP} > /dev/null 2>&1

#nohup /bin/sh convert.sh &

#!/bin/bash

DATA_DIR=/export/data/showcase-tv/chintai/
CSV_FILE=20140910_showwork_22.csv
SHOP_FILE=20140910_showwork_shop_22.csv
IMAGE_FILE=20140910_showwork_22.zip

TARGET_DIR=/export/apahau_batch/showcase/showwork/

sudo mv ${DATA_DIR}${CSV_FILE} ${DATA_DIR}fin_${CSV_FILE}
sudo mv ${DATA_DIR}${SHOP_FILE} ${DATA_DIR}fin_${SHOP_FILE}
sudo mv ${DATA_DIR}image/${IMAGE_FILE} ${DATA_DIR}image/fin_${IMAGE_FILE}

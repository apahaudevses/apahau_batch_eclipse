#!/bin/sh

#rm showcase_list_insert.sql

value="DELETE FROM T_Showcase;"
echo $value > showcase_list_insert.sql

numLine=0
cat showcase_bukken.csv | while read line
do

	# ----------------------------
	# 配列にセット
	# ----------------------------
	#","区切り文字列をタブ区切りに変換
	value_tab=`echo ${line} `
	
	#配列にセット
	IFS="$(echo -e '\t' )"
	set -- ${line}

	if ((${numLine}==1))
	then
		valueA="insert into T_Showcase(Converter_User_Id, Property_Management_Id) value("
	else
		valueA=',('
	fi
	valueB=`echo ${3}`
	valueC=`echo ${4}`
	valueD=', '

	if ((${numLine} < 100))
	then
		valueE=")"
		numLine=`expr ${numLine} + 1`
	else
		valueE=');'
		numLine=1
	fi

	value=${valueA}${valueB}${valueD}${valueC}${valueE}

	if [ `echo "${1}" | sed -e 's/"//g'` = "3" ]
	then
		echo $value >> showcase_list_insert.sql
	fi
done
echo ";" >> showcase_list_insert.sql

value="DELETE FROM T_ShowcaseShops;"
echo $value > showcase_shop_insert.sql

numLine=0
cat showcase_shop.csv | while read line
do

	# ----------------------------
	# 配列にセット
	# ----------------------------
	#","区切り文字列をタブ区切りに変換
	value_tab=`echo ${line} `
	
	#配列にセット
	IFS="$(echo -e '\t' )"
	set -- ${line}

	if [ "${numLine}" = 1 ]
	then
		valueA="insert into T_ShowcaseShops(Converter_User_Id) value("
	else
		valueA=',('
	fi
	valueB=`echo ${2}`

	if ((${numLine} < 100))
	then
		valueE=")"
		numLine=`expr ${numLine} + 1`
	else
		valueE=');'
		numLine=1
	fi

	value=${valueA}${valueB}${valueE}

	if [ `echo "${1}" | sed -e 's/"//g'` = "3" ]
	then
		echo $value >> showcase_shop_insert.sql
	fi
done
echo ";" >> showcase_shop_insert.sql


mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai < showcase_list_insert.sql
mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai < showcase_shop_insert.sql

#mysql -h localhost -uapadb -pdak5t3a3ahfi5 -N apahau_chintai < showcase_list_insert.sql
#mysql -h localhost -uapadb -pdak5t3a3ahfi5 -N apahau_chintai < showcase_shop_insert.sql

#nohup /bin/sh shop_update.sh &

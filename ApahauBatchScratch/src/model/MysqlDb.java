package model;

import java.sql.*;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import module.Convert;

public class MysqlDb {
	/**
	 * DB接続情報
	 *
	 * 接続情報の初期値はlocalhost
	 */
	private String url_apahau_chintai = "jdbc:mysql://localhost/apahau_chintai?characterEncoding=utf8";
	private String url_apahau_search = "jdbc:mysql://localhost/apahau_search?characterEncoding=utf8";
	private String user = "root";
	private String pass = "";

//	private String host_name = "localhost";

	/**
	 * DB変数
	 */
	private Connection con_apahau_chintai = null;
	private Connection con_apahau_search = null;
	private PreparedStatement ps_apahau_chintai = null;
	private PreparedStatement ps_apahau_search = null;

	//ログ変数
	private Logger logger = Logger.getLogger(MysqlDb.class);

	//コンストラクタ
	public MysqlDb(String server){
		if("local".equals(server)) {
			url_apahau_chintai = "jdbc:mysql://localhost/apahau_chintai?characterEncoding=utf8";
			url_apahau_search = "jdbc:mysql://localhost/apahau_search?characterEncoding=utf8";
			user = "root";
			pass = "";
		}else if("dev".equals(server)) {
			url_apahau_chintai = "jdbc:mysql://localhost/apahau_chintai?characterEncoding=utf8";
			url_apahau_search = "jdbc:mysql://localhost/apahau_search?characterEncoding=utf8";
			user = "root";
			pass = "mode183sen";
		}else if("batch".equals(server)) {
			url_apahau_chintai = "jdbc:mysql://apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com/apahau_chintai?characterEncoding=utf8";
			url_apahau_search = "jdbc:mysql://apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com/apahau_search?characterEncoding=utf8";
//			host_name = "apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com";
			user = "apahau";
			pass = "FByPy7Nn";
		}

		//設定ファイルを読み込む
		PropertyConfigurator.configure("log4j.properties");

		try {
			//ドライバクラスをロード
			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e){
			e.printStackTrace();
			logger.error("ドライバーロードエラー" + e.getMessage());
		}
	}

	/**
	 * データベース open
	 *
	 * @return status
	 * @author OKUNO Tatsuji
	 *
	 */
	public void open_db() {
		try {
			con_apahau_chintai = DriverManager.getConnection(url_apahau_chintai, user, pass);
			con_apahau_search = DriverManager.getConnection(url_apahau_search, user, pass);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * データベース close
	 *
	 * @return status
	 * @author OKUNO Tatsuji
	 *
	 */
	public void close_db() {
		try {
			// close処理
			if (ps_apahau_chintai != null) {
				ps_apahau_chintai.close();
			}
			if (ps_apahau_search != null) {
				ps_apahau_search.close();
			}
			// close処理
			if (con_apahau_chintai != null) {
				con_apahau_chintai.close();
			}
			if (con_apahau_search != null) {
				con_apahau_search.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

/*
	public static void main(String[] args) throws SQLException {
		try {
			//driverロード
			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e){
			e.printStackTrace();

			//設定ファイルを読み込む
			PropertyConfigurator.configure("log4j.properties");
			logger.error("ドライバーロードエラー" + e.getMessage());
		}
	}
*/

	/**
	 * User_id取得
	 *
	 * @author masuda
	 * @param text Connected_1
	 */
	public int get_userid(String text) {
		String sql = "";
		//MYSQL接続(文字コードをUTF-8指定で)
		try {
//			url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
//			con = DriverManager.getConnection(url,user,pass);

			sql = "SELECT ID FROM T_ShopUser WHERE Connected_1 = " + text;

			// ステートメントオブジェクトを生成
			ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql);

			// クエリーを実行して結果セットを取得
			ResultSet rs = ps_apahau_chintai.executeQuery();

			int user_id = -1;

			// 検索された行数分ループ
			while(rs.next()) {
				//housing_idを取得
				user_id = rs.getInt("ID");

				// データの表示
				//System.out.println("user_id;"+" " + user_id);
			}
			// 結果セットをクローズ
			ps_apahau_chintai.close();

			/* データベース切断 */
//			con.close();

			return user_id;
		} catch (SQLException e) {
			out_logger_message(sql,e.getMessage());
			e.printStackTrace();
			return -1;
/*
		}finally{
			try{
				if (con != null){
					con.close();
				}
			}catch (SQLException e){

			}
*/
		}
	}

	/**
	 * Housing_id取得
	 *
	 * @author masuda
	 * @param text Property_Management_Id
	 * @param text Converter_User_Id
	 * @return housing_id
	 */
	public int get_housingid(String Property_Management_Id, String Converter_User_Id) {
		String sql = "";
		try {
//			url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
//			con = DriverManager.getConnection(url,user,pass);

			sql = "SELECT Housing_Id FROM T_Housing ";
			sql = sql + "WHERE Property_Management_Id = '" + Property_Management_Id + "'";
			sql = sql + " AND Converter_User_Id = '" + Converter_User_Id + "'";

			// ステートメントオブジェクトを生成
			ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql);

			// クエリーを実行して結果セットを取得
			ResultSet rs = ps_apahau_chintai.executeQuery();

			int housing_id = -1;

			// 検索された行数分ループ
			while(rs.next()) {
				//housing_idを取得
				housing_id = rs.getInt("Housing_Id");

				// データの表示
				//System.out.println("Housing_Id;"+" " + housing_id);
			}

			// 結果セットをクローズ
			ps_apahau_chintai.close();

			return housing_id;
		} catch (SQLException e) {
			out_logger_message(sql,e.getMessage());
			e.printStackTrace();

			return -1;
/*
		}finally{
			try{
				if (con != null){
					con.close();
				}
			}catch (SQLException e){

			}
*/
		}
	}

	/**
	 * INSERT用関数
	 *
	 * @param inputmap インサート用hashmap
	 * @return housing_id
	 * @author masuda
	 *
	 */
	public int insert_db(String database, String tablename, HashMap<String,String> inputmap, String type) {
		String sql = "";
		try {
			//Hashが空の時でも入れるか?
			//if(!inputmap.isEmpty()){
				/*
				 * 変数
				 */
				StringBuilder field_text = new StringBuilder();
				StringBuilder value_text = new StringBuilder();
				String field_sql;
				String value_sql;

				switch (type) {
				case "db":
/*
					//TODO 空の時はどうするか要確認
					url = "jdbc:mysql://" + host_name + "/" + database + "?characterEncoding=utf8";
					//MYSQL接続(文字コードをUTF-8指定で)
					con = DriverManager.getConnection(url,user,pass);
*/
					/*
					 * SQL文を作成
					 */
					value_text.append("'");
					ResultSet rs = null;
					int autoIncKey = -1;

					for (String key:inputmap.keySet()) {
						//field名設定
						field_text.append(key);
						field_text.append(",");

						//value設定
						value_text.append(inputmap.get(key));
						value_text.append("','");
					}

					//sql文連結
					//実行するSQL文とパラメータを指定する
					field_sql = field_text.toString().replaceAll(",$","");
					value_sql = value_text.toString().replaceAll(",'$","");
					sql = "INSERT INTO " + tablename +"(" + field_sql + ") VALUES(" + value_sql + ")";
					//System.out.println(sql);

					// クエリーを実行して結果セットを取得
					switch(database){
					case "apahau_chintai":
						ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql,java.sql.Statement.RETURN_GENERATED_KEYS);
						ps_apahau_chintai.executeUpdate();
						break;
					case "apahau_search":
						ps_apahau_search = con_apahau_search.prepareStatement(sql,java.sql.Statement.RETURN_GENERATED_KEYS);
						ps_apahau_search.executeUpdate();
						break;
					}
					if(tablename.equals("T_Housing")) {
						//auto-incrementの値取得
						rs = ps_apahau_chintai.getGeneratedKeys();
						if (rs.next()) {
							autoIncKey = rs.getInt(1);
						}
						// 結果セットをクローズ
						ps_apahau_chintai.close();
						field_text.setLength(0);
						value_text.setLength(0);
						return autoIncKey;
					}
					break;
				case "text":
					value_text.append("'");

					for (String key:inputmap.keySet()) {
						//field名設定
						field_text.append(key);
						field_text.append(",");

						//value設定
						value_text.append(inputmap.get(key));
						value_text.append("','");
					}

					//実行するSQL文とパラメータを指定する
					field_sql = field_text.toString().replaceAll(",$","");
					value_sql = value_text.toString().replaceAll(",'$","");


					sql = "INSERT INTO " + tablename +"(" + field_sql + ") VALUES(" + value_sql + ");";

					//INSERTのSQL文をテキストファイルで出力
					Convert convertobject = new Convert();
					convertobject.print_csv(tablename,field_text.toString(),sql,"INSERT");

					//System.out.println(sql);

					return -100;
//						break;
				default:
					break;
				}
				field_text.setLength(0);
				value_text.setLength(0);
			//}
			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			out_logger_message(sql,e.getMessage());
			return -1;
		} finally {
/*
			try{
				if (con != null){
					con.close();
				}
			}catch (SQLException e){

			}
*/
		}
	}

	/**
	 * UPDATE用関数
	 *
	 * @param dbname データベース名
	 * @param inputmap update用hashmap
	 * @param housing_id ハウジングID
	 * @return void
	 * @author masuda
	 *
	 *
	 */
	public void update_db(String database, String[] sql_array, int housing_id, String type) {
		String tmpText = "";
		int exist_flag = 1;

		try {
			switch (type) {
			case "db":
/*
				//hashがからでないときは実行
				url = "jdbc:mysql://" + host_name + "/" + database + "?characterEncoding=utf8";

				PreparedStatement ps = null;
				//MYSQL接続(文字コードをUTF-8指定で)
				con = DriverManager.getConnection(url,user,pass);
*/
				for(String value : sql_array) {
					if(!value.equals("")) {
						//配列のSQL文を実行
						//System.out.println(value);

						tmpText = value;
						// ステートメントオブジェクトを生成
						switch(database){
						case "apahau_chintai":
							ps_apahau_chintai = con_apahau_chintai.prepareStatement(value);
							// クエリーを実行して結果セットを取得
							exist_flag = ps_apahau_chintai.executeUpdate();
							break;
						case "apahau_search":
							ps_apahau_search = con_apahau_search.prepareStatement(value);
							// クエリーを実行して結果セットを取得
							exist_flag = ps_apahau_search.executeUpdate();
							break;
						}

						if(exist_flag == 0) {
							//ログ肥大化対策のためコメントアウト
							//out_logger_message(tmpText,"物件が見つからなかった");
						}

						// 結果セットをクローズ
						switch(database){
						case "apahau_chintai":
							ps_apahau_chintai.close();
							break;
						case "apahau_search":
							ps_apahau_search.close();
							break;
						}
					}
				}

				break;
			case "text":

				break;
			default:
				break;
			}
		} catch (SQLException e) {
			out_logger_message(tmpText,e.getMessage());
			e.printStackTrace();
/*
		}finally{
			try{
				if (con != null){
					con.close();
				}
			}catch (SQLException e){

			}
*/
		}
	}

	/**
	 * DELETE用関数
	 * @param id Property_Management_Id/Housing_Id
	 * @param type
	 * @param processed_date 処理対象日付
	 * @return void
	 * @author masuda
	 *
	 */
	public void delete_db(String id,String type,String processed_date) throws SQLException {
		String tmpText = "";
//		PreparedStatement ps_apahau_chintai = null;
		String sql;
		sql = "";
		int exist_flag = 1;

		try {
			switch (type) {
			case "db":
/*
				url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
				ps = null;
				//MYSQL接続(文字コードをUTF-8指定で)
				con = DriverManager.getConnection(url,user,pass);
*/
				/**
				 * 論理削除
				 */
				sql = "UPDATE T_Housing SET Delete_FLG = '1' WHERE Housing_Id = '" + String.valueOf(id) + "'";
				sql = sql + " AND Converter_Id = 'apaman'";
				if(processed_date != ""){
					sql = sql + " AND Updata_Date < '" + String.valueOf(processed_date) + "'";
				}
				ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
				exist_flag = ps_apahau_chintai.executeUpdate();

				//System.out.println(sql);

				if(exist_flag == 0) {
					//ログ肥大化対策のためコメントアウト
					//out_logger_message(sql,"物件が見つからなかった");
				}

				// 結果セットをクローズ
				ps_apahau_chintai.close();

				/*
				//いったん閉じる
				con.close();

				//hashがからでないときは実行
				url = "jdbc:mysql://" + host_name + "/apahau_search?characterEncoding=utf8";
				ps = null;

				//MYSQL接続(文字コードをUTF-8指定で)
				con = DriverManager.getConnection(url,user,pass);
*/
				/**
				 * 論理削除
				 */
				sql = "UPDATE T_SearchHousing SET Delete_FLG = '1' WHERE Housing_Id = '" + String.valueOf(id) + "'";
				ps_apahau_search = con_apahau_search.prepareStatement(sql.toString());
				exist_flag = ps_apahau_search.executeUpdate();

				if(exist_flag == 0) {
					//ログ肥大化対策のためコメントアウト
					//out_logger_message(sql,"物件が見つからなかった");
				}

				break;
			case "db_physic":
/*
				url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
				ps = null;
				//MYSQL接続(文字コードをUTF-8指定で)
				con = DriverManager.getConnection(url,user,pass);
*/
				/**
				 * 物理削除
				 */
				//T_Housing
				sql = "DELETE FROM T_Housing WHERE Housing_Id = '" + String.valueOf(id) + "'";
				sql = sql + " AND Converter_Id = 'apaman'";
				if(processed_date != ""){
					sql = sql + " AND Updata_Date < '" + String.valueOf(processed_date) + "'";
				}
				ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
				exist_flag = ps_apahau_chintai.executeUpdate();
				// 結果セットをクローズ
				ps_apahau_chintai.close();

				if(exist_flag == 1){
					//T_Equipment
					sql = "DELETE FROM T_Equipment WHERE Housing_Id = '" + String.valueOf(id) + "'";
					ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
					exist_flag = ps_apahau_chintai.executeUpdate();
					// 結果セットをクローズ
					ps_apahau_chintai.close();

					//T_ChatchCopy
					sql = "DELETE FROM T_ChatchCopy WHERE Housing_Id = '" + String.valueOf(id) + "'";
					ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
					exist_flag = ps_apahau_chintai.executeUpdate();
					// 結果セットをクローズ
					ps_apahau_chintai.close();

					//T_Image
					sql = "DELETE FROM T_Image WHERE Housing_Id = '" + String.valueOf(id) + "'";
					ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
					exist_flag = ps_apahau_chintai.executeUpdate();
					// 結果セットをクローズ
					ps_apahau_chintai.close();

					//T_NearbyAttractions
					sql = "DELETE FROM T_NearbyAttractions WHERE Housing_Id = '" + String.valueOf(id) + "'";
					ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
					exist_flag = ps_apahau_chintai.executeUpdate();
					// 結果セットをクローズ
					ps_apahau_chintai.close();

					//T_RoomInfo
					sql = "DELETE FROM T_RoomInfo WHERE Housing_Id = '" + String.valueOf(id) + "'";
					ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
					exist_flag = ps_apahau_chintai.executeUpdate();
					// 結果セットをクローズ
					ps_apahau_chintai.close();

					//T_Traffic
					sql = "DELETE FROM T_Traffic WHERE Housing_Id = '" + String.valueOf(id) + "'";
					ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
					exist_flag = ps_apahau_chintai.executeUpdate();
					// 結果セットをクローズ
					ps_apahau_chintai.close();

					//T_Article
					sql = "DELETE FROM T_Article WHERE Housing_Id = '" + String.valueOf(id) + "'";
					ps_apahau_chintai = con_apahau_chintai.prepareStatement(sql.toString());
					exist_flag = ps_apahau_chintai.executeUpdate();
					// 結果セットをクローズ
					ps_apahau_chintai.close();

					logger.debug("DELETE 実行 Housing_Id：[" + String.valueOf(id) + "]");
				}

				//System.out.println(sql);

				if(exist_flag == 0) {
					//ログ肥大化対策のためコメントアウト
					//out_logger_message(sql,"物件が見つからなかった");
				}
/*
				//いったん閉じる
				con.close();

				//hashがからでないときは実行
				url = "jdbc:mysql://" + host_name + "/apahau_search?characterEncoding=utf8";
				ps = null;

				//MYSQL接続(文字コードをUTF-8指定で)
				con = DriverManager.getConnection(url,user,pass);
*/
				/**
				 * 物理削除
				 */
				//T_SearchHousing
				sql = "DELETE FROM T_SearchHousing WHERE Housing_Id = '" + String.valueOf(id) + "'";
				ps_apahau_search = con_apahau_search.prepareStatement(sql.toString());
				exist_flag = ps_apahau_search.executeUpdate();
				// 結果セットをクローズ
				ps_apahau_chintai.close();

				//T_SearchTraffic
				sql = "DELETE FROM T_SearchTraffic WHERE Housing_Id = '" + String.valueOf(id) + "'";
				ps_apahau_search = con_apahau_search.prepareStatement(sql.toString());
				exist_flag = ps_apahau_search.executeUpdate();
				// 結果セットをクローズ
				ps_apahau_chintai.close();

				if(exist_flag == 0) {
					//ログ肥大化対策のためコメントアウト
					//out_logger_message(sql,"物件が見つからなかった");
				}

				break;
			case "text":
				//デバッグ用にINSERTする置換データ出力(CSV形式)
				sql = "UPDATE T_Housing SET Delete_FLG = '1' where Property_Management_Id = '" + String.valueOf(id) + "';\n";
				sql = sql + "UPDATE T_SearchHousing SET Delete_FLG = '1' WHERE Housing_Id = '" + String.valueOf(id) + "';";

				//System.out.println(sql);


				//SQL文　ファイル出力
				Convert convertobject = new Convert();
				convertobject.print_csv("delete","",sql,"DELETE");
				break;
			default:
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			out_logger_message(tmpText,e.getMessage());
/*
		} finally {
			try{
				if (con != null){
					con.close();
				}
			}catch (SQLException e){

			}
*/
		}
	}

	/**
	 * ロガー出力関数(error)
	 *
	 * @author masuda
	 * @param sql sql文
	 * @param message エラーメッセージ
	 */
	public void out_logger_message(String sql,String message) {
		String log_message = "(1)" + sql + "\n" + "(2)" + message;
		logger.error(log_message);
	}
}


package module;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;

public class Convert {

	/**
	 * 文字列に含まれる全角数字を半角数字に変換します。
	 * 全角「．」も半角人変換
	 *
	 * @param str 変換前文字列(null不可)
	 * @return 変換後文字列
	 */
	public static String fullWidthNumberToHalfWidthNumber(String str) {
		if (str == null){
			throw new IllegalArgumentException();
		}

		str = str.replace("．", ".");
		StringBuffer sb = new StringBuffer(str);
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if ('０' <= c && c <= '９') {
				sb.setCharAt(i, (char) (c - '０' + '0'));
			}
		}
		String result = sb.toString();
		sb.setLength(0);
		return result;
	}

	/**
	 * 空の時は0で返す(DBがint型のフィールド)
	 * @param text
	 * @return
	 */
	public String get_sqlintfield(String text) {
		if(text.equals("")) {
			return "0";
		}else {
			return text;
		}
	}

	/**
	 * Stringをint指定に
	 * @author masuda
	 * @param text
	 * @return
	 */
	public String convert_int(String text) {
		return text.replaceAll("\"","");
	}

	/**
	 * SQL文をファイルに出力する
	 *
	 * @author masuda
	 * @param tablename テーブル名
	 * @param fiels フィールドの値(,区切り)
	 * @param value 値(,区切り)
	 * @return void
	 *
	 */
	public void print_csv(String tablename,String fiels,String value,String command) {
		//出力先を作成する
//	  FileWriter fw;
		OutputStreamWriter fw = null;
		PrintWriter pw = null;

		try {
			fw = new OutputStreamWriter(new FileOutputStream("output_csv/" + command + "/" + tablename + ".csv",true),"UTF-8");
			pw = new PrintWriter(new BufferedWriter(fw));

			//内容を指定する
			pw.println(value);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			pw.close();
			fw = null;
			pw = null;
		}

	}

	/**
	 * テキスト部分のみを取得
	 */
	public String get_textonly(String text) {
		text = text.replaceAll("[0-9\\.]","");
		return text;
	}

	/**
	 * 数字・小数点のみを取得
	 */
	public String get_number(String text) {
		String[] ret = new String[2];
		String patternRegex = "([0-9\\.]*)";
		Pattern pattern = Pattern.compile(patternRegex);
		Matcher matcher = pattern.matcher(text);

		if(matcher.find()){
			ret[0] = matcher.group(1);
		}

		String result = ret[0].toString();
		return result;
	}

	/**
	 * 文字コードを取得
	 *
	 * @author masuda
	 * @param text
	 * @return　tmpArray Prefectural_Id/Property_Shiku/Property_Choson/Property_Aza
	 */
	public String get_azacd(String text1) {
		/*
		String[] tmpArray = new String[4];

		tmpArray[0] = text1.substring(1, 3);
		tmpArray[1] = text1.substring(3, 4);
		tmpArray[2] = text1.substring(6, 7);
		tmpArray[3] = text1.substring(8, 11);
		*/

		return text1.substring(8, 11);
	}

	/**
	 * 沿線ステーションコードを取得
	 *
	 * @author masuda
	 * @param text
	 * @return　text1 Routeコード
	 * @return　text2 Stationコード
	 *
	 *
	 * TODO 要確認
	 */
	public String get_routecd(String text1,String text2) {
		String cdtext1;
		String cdtext2;

		if(!text1.equals("0") && !text1.equals("")) {
			cdtext1 = text1.substring(0, 1);
			cdtext2 = text1.substring(1, 4);
		}else {
			cdtext1 = "";
			cdtext2 = "";
		}

		if(!text2.equals("0") && !text2.equals("")) {
			return cdtext1 + "-" + cdtext2 + "-" + text2;
		}else {
			return "";
		}

	}

	/**
	 * Floor_no設定
	 * Floor_Plan
	 *
	 * @param text
	 * @retrun tmpArray[0] Floor_Number
	 * @retrun tmpArray[1] Floor_Plan
	 *
	 */
	public String[] floor_no(String text) {
		String[] tmpArray = new String[2];
		switch (text) {
		case "80":
		case "81":
		case "82":
		case "83":
			tmpArray[0] = "0";
			tmpArray[1] = "0";
			break;
		default:
			tmpArray[0] = text.substring(0, 1);

			String tmpText = "";
			//TODO
			tmpText = text.substring(1, 2);
			if(tmpText.equals("1")) {
				tmpArray[1] = "2";
			}else if(tmpText.equals("2")) {
				tmpArray[1] = "3";
			}else if(tmpText.equals("3")) {
				tmpArray[1] = "7";
			}else if(tmpText.equals("4")) {
				tmpArray[1] = "5";
			}else if(tmpText.equals("5")) {
				tmpArray[1] = "9";
			}else if(tmpText.equals("0")) {
				tmpArray[1] = "0";
			}else {
				tmpArray[1] = "0";
			}
		}
		return tmpArray;
	}

	/**
	 * room_type
	 *
	 * @param text
	 * @return String ルームコード
	 */
	public String room_type(String text) {
		switch (text) {
		case "和":
		case "和室":
			return "1";

		case "洋":
		case "洋室":
			return "2";

		case "DK":
			return "3";

		case "LDK":
			return "4";

		case "L":
			return "5";

		case "D":
			return "6";

		case "K":
			return "7";

		case "LK":
			return "8";

		case "LD":
			return "9";

		case "S":
			return "10";

		default:
			return "0";

		}
	}

	/**
	 * opening_direction
	 *
	 * @param text
	 * @return String 開口方向
	 */
	public String opening_direction(String text) {
		switch (text) {
		case "北":
			return "1";
		case "北東":
			return "2";
		case "東":
			return "3";
		case "南東":
			return "4";
		case "南":
			return "5";
		case "南西":
			return "6";
		case "西":
			return "7";
		case "北西":
			return "8";
		default:
//			return "2";
			return "0";
		}
	}

	/**
	 *　ダブルクウォートを削除
	 * @author masuda
	 * @param text
	 * @return
	 */
	private String delete_quoto(String text) {
		return text.replaceAll("\"","");
	}

	/**
	 * structure指定
	 * @author masuda
	 * @param text
	 * @return
	 */
	public String structure(String text) {
		text = delete_quoto(text);
		/*
		 * ●CSV
		1	木造
		2	軽量鉄骨
		3	鉄筋コンクリート
		4	鉄骨鉄筋コンクリート
		5	ALC
		6	プレキャストコンクリート
		7	鉄筋ブロック
		8	その他
		9	鉄骨造
		10	舗装
		11	重量鉄骨
		*/
		switch (text) {
		case "1":
			return "1";
		case "2":
			return "2";
		case "3":
			return "7";
		case "4":
			return "8";
		case "5":
			return "3";
		case "6":
			return "5";
		case "7":
			return "9";
		case "8":
			return "11";
		case "9":
			return "4";
		case "10":
			return "11";
		case "11":
			return "10";
		default:
			//TODO 要確認
			return "0";
		}
	}

	/**
	 * property_type指定
	 *
	 * @author masuda
	 * @param text
	 * @return
	 */
	public String type(String text) {
		text = delete_quoto(text);
		/*
		 * ●CSV
		1	アパート
		2	マンション
		3	ハイツ
		4	コーポ
		5	テラスハウス
		6	タウンハウス
		7	一戸建て
		*/
		switch (text) {
		case "1":
			return "4";
		case "2":
			return "1";
		case "3":
			return "4";
		case "4":
			return "5";
		case "5":
			return "2";
		case "6":
			return "3";
		default:
			return "5";
		}
	}

	/**
	 * UPDATE SQL文生成
	 * @param tablename
	 * @param inputmap
	 * @param housing_id
	 * @return
	 */
	public String create_update_sql(String tablename, HashMap<String,String> inputmap, int housing_id) {
		if(!inputmap.isEmpty()){
			StringBuffer sql;
			String tmpText = "";

			// 検索SQL文を作成
			sql = new StringBuffer();
			sql = sql.append("UPDATE " + tablename + " SET ");

			for (String key:inputmap.keySet()) {
				sql.append(key + " = '" + inputmap.get(key) + "'");
				sql.append(",");
			}

			tmpText = sql.toString().replaceAll(",$","");
			tmpText = tmpText + " WHERE Housing_Id = '" + String.valueOf(housing_id) + "'";

			sql.setLength(0);

			return tmpText;
		}else {
			return "";
		}
	}

	/**
	 * 現在日時の取得
	 * @return
	 */
	public static String getCurrentDateString() {
		//==== 現在時刻を取得 ====//
		Date date = new Date();

		//==== 表示形式を設定 ====//
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		return sdf.format(date);
	}
}

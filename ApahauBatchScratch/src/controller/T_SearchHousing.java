package controller;

import java.util.HashMap;

import model.MysqlDb;
import module.Convert;


/**
 * T_SearchHousing変換
 *
 * @author masuda
 *
 */
public class T_SearchHousing extends Convert {
	private String OutputType = "";
	/**
	 * ハッシュマップ
	 * key: DBフィールド名
	 * value: 値
	 */
	private HashMap<String, String> hmap;

	/**
	 * 処理で利用する配列
	 * 一時的に利用する変数
	 */
	private String tmpArray[];
	private String tmpText;
	private int tmpInt;

	/**
	 * オブジェクト
	 */
//	private Convert convertobject = new Convert(); //module
	private MysqlDb mysqlobject; //mysql

	//コンストラクタ
	T_SearchHousing(String server,String type, MysqlDb mysqlobject){
		OutputType = type;
//		mysqlobject = new MysqlDb(server);
		this.mysqlobject = mysqlobject;
	}

	/**
	 * T_Housing 共通関数
	 * (INPUT/OUTPUT 共通ハッシュマップセット)
	 *
	 * @param text
	 */
	protected void common_method(String Mode, String[] apamanarray) {
		//ADD_Pref
		hmap.put("ADD_Pref", apamanarray[13]);
		//ADD_Shiku
		hmap.put("ADD_Shiku", apamanarray[14]);
		//ADD_Choson
		hmap.put("ADD_Choson", apamanarray[15]);
		//ADD_Aza
		hmap.put("ADD_Aza",get_azacd(apamanarray[12]));

		hmap.put("ADD_Branch",apamanarray[17]);
		hmap.put("Branch_FLG",apamanarray[293]);
		hmap.put("Rent",apamanarray[73]);

		//共益費
		//Common_Service_Fee
		//TODO データベースにない? Common_Service_Fee_Classification
		if( apamanarray[187].indexOf("円") >= 0 ) {
			hmap.put("Common_Service_Fee",get_number(apamanarray[187]));
		}else if( apamanarray[187].indexOf("ヶ月") >= 0 ) {
			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[187]);

			tmpInt = (int)(Float.parseFloat(get_number(tmpText)) * Float.parseFloat(apamanarray[73]));
			hmap.put("Common_Service_Fee",String.valueOf(tmpInt));
		}

		//Security_Deposits
		if(!get_number(apamanarray[75]).equals("")) {
			hmap.put("Security_Deposits","1");
		}else if(!get_number(apamanarray[77]).equals("")) {
			hmap.put("Security_Deposits","2");
		}

		//敷金保証金
		//Security_Deposits
		//Amount
		if(!"".equals(apamanarray[75])) {
			hmap.put("Security_Deposits","1");
			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[75]);
			hmap.put("Amount",get_number(tmpText));
		}else if(!"".equals(apamanarray[77])) {
			hmap.put("Security_Deposits","2");
			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[77]);
			hmap.put("Amount",get_number(tmpText));
		}

		//Security_Deposits_Classification
		if( apamanarray[75].indexOf("円") >= 0 || apamanarray[77].indexOf("円") >= 0) {
			hmap.put("Security_Deposits_Classification","2");
		}else if( apamanarray[75].indexOf("ヶ月") >= 0 || apamanarray[77].indexOf("ヶ月") >= 0) {
			hmap.put("Security_Deposits_Classification","1");
		}

		//礼金
		//Key_Money
		//Key_Money_Classification
		if( apamanarray[76].indexOf("円") >= 0 ) {
			hmap.put("Key_Money_Classification","2");
			hmap.put("Key_Money",get_number(apamanarray[76]));
		}else if( apamanarray[76].indexOf("ヶ月") >= 0 ) {
			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[76]);
			hmap.put("Key_Money",get_number(tmpText));
			hmap.put("Key_Money_Classification","1");
		}

		//Updated
		if(!get_number(apamanarray[79]).equals("")) {
			hmap.put("Updated","1");
		}else {
			hmap.put("Updated","0");
		}

		//Built_Years(T_Housing)
		hmap.put("Built_Years",apamanarray[55].replaceAll("$", "00"));
		hmap.put("Built_New_FLG",apamanarray[96]);
		hmap.put("Footprint_Square_Meters", apamanarray[52]);
		hmap.put("Floor", apamanarray[164]);
		hmap.put("Ground_Floor", apamanarray[49]);

		//Top_Floor
		if(apamanarray[49].equals(apamanarray[164])) {
			hmap.put("Top_Floor", "1");
		}else {
			hmap.put("Top_Floor", "0");
		}

		//Chatch_Copy
		hmap.put("Chatch_Copy", apamanarray[155] + apamanarray[156]);

		//Structure
		hmap.put("Structure", structure(apamanarray[51]));

		//Property_Type
		hmap.put("Property_Type", type(apamanarray[50]));
		hmap.put("Flooring", apamanarray[222]);
		hmap.put("Loft", apamanarray[223]);
		hmap.put("Bay_Window", apamanarray[227]);

		//Veranda_Balcony
		if(apamanarray[225].equals("1") || apamanarray[253].equals("1")) {
			hmap.put("Veranda_Balcony", "1");
		}else {
			hmap.put("Veranda_Balcony", "0");
		}

		//Private_Garden
		if(apamanarray[95].equals("1") || apamanarray[234].equals("1")) {
			hmap.put("Private_Garden", "1");
		}else {
			hmap.put("Private_Garden", "0");
		}
		hmap.put("Corner_Room", apamanarray[262]);
		hmap.put("Floor_Heating", apamanarray[248]);

		//Heating_Type
		if(apamanarray[210].equals("1")) {
			hmap.put("Heating_Type", "1");
		}else if(apamanarray[211].equals("1")) {
			hmap.put("Heating_Type", "2");
		}else {
			hmap.put("Heating_Type", "0");
		}

		//Stove_Type
		if(apamanarray[289].equals("1")) {
			hmap.put("Stove_Type", "1");
		}else if(apamanarray[290].equals("1")) {
			hmap.put("Stove_Type", "2");
		}else if(apamanarray[233].equals("1")) {
			hmap.put("Stove_Type", "3");
		}else if(apamanarray[239].equals("1")) {
			hmap.put("Stove_Type", "4");
		}else {
			hmap.put("Stove_Type", "0");
		}

		hmap.put("System_Kitchen", apamanarray[220]);
		hmap.put("Bus_Toilet", apamanarray[226]);
		hmap.put("Shower", apamanarray[203]);
		hmap.put("Reheating", apamanarray[205]);
		hmap.put("Bathroom_Dryer", apamanarray[242]);
		hmap.put("Cleaning_Toilet_Seat", apamanarray[247]);
		hmap.put("Shoes_Box", apamanarray[274]);
		hmap.put("Under_Floor_Storage", apamanarray[229]);
		hmap.put("Trunk_Room", apamanarray[252]);

		//Intercom
		if(!apamanarray[230].equals("1")) {
			hmap.put("Intercom", "1");
		}else if(!apamanarray[236].equals("1")){
			hmap.put("Intercom", "2");
		}else {
			hmap.put("Intercom", "0");
		}

		//Security_Auto_Lock
		if(apamanarray[90].equals("1") || apamanarray[194].equals("1")) {
			hmap.put("Security_Auto_Lock", "1");
		}else {
			hmap.put("Security_Auto_Lock", "0");
		}

		hmap.put("Security_Dimple_Key", apamanarray[283]);
		hmap.put("Security_Camera", apamanarray[268]);
		hmap.put("Common_Use_Delivery_Box", apamanarray[193]);

		//Move_In_Date
		if(!apamanarray[153].equals("")) {
			hmap.put("Move_In_Date", "1");
		}else {
			hmap.put("Move_In_Date", "0");
		}

		//Occupancy_Sex
		if(apamanarray[87].equals("1")) {
			hmap.put("Occupancy_Sex", "3");
		}else {
			hmap.put("Occupancy_Sex", "1");
		}

		//Occupancy_Single_Person
		if (Mode.equals("insert")) {
			hmap.put("Occupancy_Single_Person", "0");
		}

		hmap.put("Occupancy_Two_Person", apamanarray[93]);

		//Occupancy_Room_Share
		if(apamanarray[91].equals("1")) {
			hmap.put("Occupancy_Room_Share", "3");
		}else {
			hmap.put("Occupancy_Room_Share", "1");
		}

		//Occupancy_Pet
		if(apamanarray[82].equals("1")) {
			hmap.put("Occupancy_Pet", "3");
		}else {
			hmap.put("Occupancy_Pet", "1");
		}

		hmap.put("Occupancy_Office", apamanarray[84]);

		hmap.put("Occupancy_Instrument", apamanarray[83]);
		//Occupancy_Elderly
		if(apamanarray[88].equals("1")) {
			hmap.put("Occupancy_Elderly", "3");
		}else {
			hmap.put("Occupancy_Elderly", "1");
		}

		//Broadband
		if(apamanarray[259].equals("1")) {
			hmap.put("Broadband", "3");
		}else {
			hmap.put("Broadband", "0");
		}

		hmap.put("Line_Bs", apamanarray[214]);
		hmap.put("Line_Cs", apamanarray[215]);
		hmap.put("Line_Catv", apamanarray[216]);
		hmap.put("Line_Wired", apamanarray[217]);
		hmap.put("Elevator", apamanarray[192]);
		hmap.put("Washing_Machine_Storage", apamanarray[224]);
		hmap.put("Shampoo_Dresser", apamanarray[206]);
		hmap.put("All_Electric", apamanarray[254]);
		hmap.put("Fully_Furnished", apamanarray[280]);

		//Gas
		if(!apamanarray[289].equals("1")) {
			hmap.put("Gas", "1");
		}else if(!apamanarray[290].equals("1")){
			hmap.put("Gas", "2");
		}else {
			hmap.put("Gas", "0");
		}

		hmap.put("Parking", apamanarray[144]);
		hmap.put("Common_Use_Parking_Bicycles", apamanarray[199]);
		hmap.put("Common_Use_Motorcycle_parking", apamanarray[200]);
		hmap.put("Ventilation_System", apamanarray[288]);

		//Reform
		//TODO リフォーム　リノベーション　同じ?
		if(!apamanarray[275].equals("1") || !apamanarray[276].equals("1")) {
			hmap.put("Reform", "1");
		}else {
			hmap.put("Reform", "0");
		}

		hmap.put("Internet_Free", apamanarray[272]);

		String Path;
		Path = "http://img.apamanshop.com/fs/Original/img/images/";

		//Image_G_URL
		if(!apamanarray[99].equals("")) {
			hmap.put("Image_G_URL", Path + apamanarray[141].replaceAll("$", "/") + apamanarray[99]);
		}else {
			hmap.put("Image_G_URL", "9");
		}

		//Image_M_URL
		if(!apamanarray[114].equals("")) {
			hmap.put("Image_M_URL", Path + apamanarray[114]);
		}else {
			hmap.put("Image_M_URL", "");
		}

		//Contract_Type
		if(!apamanarray[163].equals("0")) {
			hmap.put("Contract_Type", "1");
		}else {
			hmap.put("Contract_Type", "2");
		}
		hmap.put("Barrier_Free", apamanarray[256]);

		hmap.put("View_FLG", "0");
		hmap.put("Delete_FLG", "0");

		//Floor_Number
		//Floor_Plan
		tmpArray = floor_no(apamanarray[63]);
		hmap.put("Floor_Number",tmpArray[0]);
		hmap.put("Floor_Plan",tmpArray[1]);

		//Shop_Id
		int shop_id;
		//TODO 一時コメントアウト
		shop_id = mysqlobject.get_userid(apamanarray[9]);
		hmap.put("Shop_Id", String.valueOf(shop_id));

	}

	/**
	 * Input用関数
	 *
	 * @author masuda
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected void insert(int housing_id,String[] apamanarray) {
		hmap = new HashMap<String,String>();
		/*
		 * input・update　共通セット
		 */
		common_method("insert", apamanarray);

		//Housing_Id
		hmap.put("Housing_Id", String.valueOf(housing_id));

		/*
		 * TODO 要確認　フィールドがない
		//Rooms_Facing_South
		if(apamanarray[71].equals("南")) {
			hmap.put("Rooms_Facing_South", "1");
		}else {
			hmap.put("Rooms_Facing_South", "0");
		}
		 */

		//Shop_Rank
		//TODO 要確認

		//データベースに保存
		if (OutputType.equals("db")) {
			//DB出力
			mysqlobject.insert_db("apahau_search","T_SearchHousing",hmap,"db");
		}else if(OutputType.equals("text")) {
			//Text出力
			mysqlobject.insert_db("apahau_search","T_SearchHousing",hmap,"text");
		}
		hmap = null;
	}

	/**
	 * update関数
	 *
	 * @author masuda
	 * @param apamanarray アパマン取得csv配列
	 * @return int housing_id
	 *
	 */
	protected String update(int housing_id,String[] apamanarray) {
		try {
			hmap = new HashMap<String,String>();
			/*
			 * input・update　共通セット
			 */
			common_method("update", apamanarray);

			/*
			 * UPDATE個別項目
			 */
			//Facing_South
			if(apamanarray[71].equals("南")) {
				hmap.put("Facing_South", "1");
			}else {
				hmap.put("Facing_South", "0");
			}

			/*
			 * DB UPDATE処理
			 */
			if (OutputType.equals("db")) {
				//DB出力
				//mysqlobject.update_db("apahau_search","T_SearchHousing",hmap,housing_id,"db");
				return create_update_sql("T_SearchHousing", hmap, housing_id);
			}else if(OutputType.equals("text")) {
				//Text出力
				//mysqlobject.update_db("apahau_search","T_SearchHousing",hmap,housing_id,"text");
			}
			return "";
		} finally {
			hmap = null;
		}
	}
}

package controller;

import java.util.HashMap;

import model.MysqlDb;
import module.Convert;

/**
 * T_ChatchCopy変換
 *
 * @author masuda
 *
 */
public class T_ChatchCopy extends Convert {
	/**
	 * ハッシュマップ
	 * key: DBフィールド名
	 * value: 値
	 */
	private HashMap<String, String> hmap;
	private String OutputType = "";

	/**
	 * 処理で利用する配列
	 * 一時的に利用する変数
	 */
	private String tmpArray[];
	private String tmpText;

	/**
	 * オブジェクト
	 */
	private MysqlDb mysqlobject;

	//コンストラクタ
	T_ChatchCopy(String server, String type, MysqlDb mysqlobject){
		OutputType = type;
//		mysqlobject = new MysqlDb(server);
		this.mysqlobject = mysqlobject;
	}

	/**
	 * T_Housing 共通関数
	 * (INPUT/OUTPUT 共通ハッシュマップセット)
	 *
	 * @param text
	 */
	protected void common_method(String[] apamanarray) {
		hmap.put("Chatch_Copy", apamanarray[155] + apamanarray[156]);
		hmap.put("Remarks", apamanarray[72]);
	}

	/**
	 * Input関数
	 *
	 * @author masuda
	 * @param apamanarray
	 * @return void
	 *
	 */
	public void insert(int housing_id,String[] apamanarray) {
		hmap = new HashMap<String,String>();
		/*
		 * input・update　共通セット
		 */
		common_method(apamanarray);

		//Housing_Id
		hmap.put("Housing_Id",String.valueOf(housing_id));
		/*
		 * INSERT個別項目
		 */

		if (OutputType.equals("db")) {
			//DB出力
			mysqlobject.insert_db("apahau_chintai","T_ChatchCopy",hmap,"db");
		}else if(OutputType.equals("text")) {
			//Text出力
			mysqlobject.insert_db("apahau_chintai","T_ChatchCopy",hmap,"text");
		}
		hmap = null;
	}

	/**
	 * update処理
	 *
	 * @author masuda
	 * @param housing_id 検索で利用するHousing_ID
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected String update(int housing_id,String[] apamanarray) {
		try {
			hmap = new HashMap<String,String>();
			/*
			 * input・update　共通セット
			 */
			common_method(apamanarray);
			/*
			 * UPDATE個別項目
			 */

			/*
			 * DB UPDATE処理
			 */
			if (OutputType.equals("db")) {
				//DB出力
				//mysqlobject.update_db("apahau_chintai","T_ChatchCopy",hmap,housing_id,"db");
				return create_update_sql("T_ChatchCopy",hmap,housing_id);
			}else if(OutputType.equals("text")) {
				//Text出力
				//mysqlobject.update_db("apahau_chintai","T_ChatchCopy",hmap,housing_id,"text");
			}
			return "";
		} finally {
			hmap = null;
		}
	}
}

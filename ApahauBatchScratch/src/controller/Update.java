package controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import model.MysqlDb;
import module.Convert;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 実際の処理プログラム(UPDATE文)
 * @author masuda
 *
 * ●コマンド
 * データベースアップデート
 * java -jar update local db
 * テキスト出力
 * java -jar update local text
 *
 */
public class Update {
	//ロガー
	protected static Logger logger = Logger.getLogger(Update.class );

	public static void main(String[] args) {
		//jar実行パス
		final String EXECPATH = System.getProperty("user.dir");

		//final String DELIMITER = "\\";
		final String DELIMITER = "/";

		InputStreamReader isr = null;
		BufferedReader br = null;

		//SQL実行　物件数　カウンター
		int bukken_counter;
		bukken_counter = 0;

		try {

			//設定ファイルを読み込む
			PropertyConfigurator.configure("log4j.properties");

			logger.debug("UPDATE 実行開始：[" + Convert.getCurrentDateString() + "]");

			MysqlDb mysqlobject = new MysqlDb(args[0]);		//mysqlオブジェクトのインスタンス化

			//isr = new InputStreamReader(new FileInputStream(EXECPATH + DELIMITER + "input_csv" + DELIMITER + "update.csv"),"Shift_JIS");
			isr = new InputStreamReader(new FileInputStream(EXECPATH + DELIMITER + "input_csv" + DELIMITER + "update.csv"),"UTF-8");
			br = new BufferedReader(isr);

			//読み込んだ行を保存
			String line;

			//アパハウcsvを入れておく配列
			String apamanarray[];

			int housing_id;
			housing_id = -1;

			/*
			 * それぞれのDB用インスタンス生成
			 */
			//apahau_chintai
			T_Housing t_housing = new T_Housing(args[0],args[1], mysqlobject);
			T_Traffic t_traffic = new T_Traffic(args[0],args[1], mysqlobject);
			T_NearbyAttractions t_nearbyattractions = new T_NearbyAttractions(args[0],args[1], mysqlobject);
			T_Image t_image = new T_Image(args[0],args[1], mysqlobject);
			T_RoomInfo t_roominfo = new T_RoomInfo(args[0],args[1], mysqlobject);
			T_Equipment t_equipment = new T_Equipment(args[0],args[1], mysqlobject);
			T_ChatchCopy t_chatchcopy = new T_ChatchCopy(args[0],args[1], mysqlobject);
			T_Article t_article = new T_Article(args[0],args[1], mysqlobject);
			//apahau_search
			T_SearchHousing t_searchhousing = new T_SearchHousing(args[0],args[1], mysqlobject);
			T_SearchTraffic t_searchtraffic = new T_SearchTraffic(args[0],args[1], mysqlobject);

			//ファイルの件数分ループして配列に格納します。
			while ((line = br.readLine()) != null ) {
				if(!line.equals("")) {
					//タブで分割にする
					line = line.replaceAll("\",\"","\t");
					line = line.replaceAll("^\"","");
					line = line.replaceAll("\"$","");

					//分割配列作成(タブ)
					apamanarray = line.split("\t");

					//apahau_chintai アップデートSQL 配列
					String apahau_chintai_sql[] = new String[8];
					//apahau_search アップデートSQL 配列
					String apahau_search_sql[] = new String[2];

					mysqlobject.open_db();							//mysqlオープン

					/*
					 * 実際の変換処理
					 */

					//Housing_id取得
					housing_id = mysqlobject.get_housingid(apamanarray[0], apamanarray[9]);

					if(housing_id > 0){
						//T_HOUSING変換(ハウジングIDを取得するため1回SQL文実行)
						apahau_chintai_sql[0] = t_housing.update(housing_id,apamanarray);
						//T_Traffic変換
						apahau_chintai_sql[1] = t_traffic.update(housing_id,apamanarray);
						//T_NearbyAttractions
						apahau_chintai_sql[2] = t_nearbyattractions.update(housing_id,apamanarray);
						//T_Image
						apahau_chintai_sql[3] = t_image.update(housing_id,apamanarray);
						//T_RoomInfo
						apahau_chintai_sql[4] = t_roominfo.update(housing_id,apamanarray);
						//T_Equipment
						apahau_chintai_sql[5] = t_equipment.update(housing_id,apamanarray);
						//T_ChatchCopy
						apahau_chintai_sql[6] = t_chatchcopy.update(housing_id,apamanarray);
						//T_Article
						apahau_chintai_sql[7] = t_article.update(housing_id,apamanarray);

						//●apahau_chintaiデータベースUPDATE
						mysqlobject.update_db("apahau_chintai",apahau_chintai_sql, housing_id, args[1]);

						//T_SearchHousing
						apahau_search_sql[0] = t_searchhousing.update(housing_id,apamanarray);
						//T_SearchTraffic
						apahau_search_sql[1] = t_searchtraffic.update(housing_id,apamanarray);

						//●apahau_searchデータベースUPDATE
						mysqlobject.update_db("apahau_search",apahau_search_sql, housing_id, args[1]);

						logger.debug("UPDATE 実行 Housing_Id：[" + housing_id + "] Property_Management_Id：[" + apamanarray[0] + "] Converter_User_Id：[" + apamanarray[9] + "]");

						bukken_counter++;

						//ガベージコレクションの呼び出し
						System.gc();
					}
					apahau_chintai_sql = null;
					apahau_search_sql = null;

					mysqlobject.close_db();							//mysqlクローズ

				}
			}
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			logger.debug("UPDATE 実行 物件数：[" + bukken_counter + "]");
			logger.debug("UPDATE 実行終了：[" + Convert.getCurrentDateString() + "]");

			if (br != null) try { br.close(); } catch (IOException e) {}
			if (isr != null) try { isr.close(); } catch (IOException e) {}
		}
	}
}
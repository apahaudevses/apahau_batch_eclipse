package controller;

import java.util.HashMap;

import model.MysqlDb;
import module.Convert;

/**
 * T_Equipment変換
 *
 * @author masuda
 *
 */
public class T_Equipment extends Convert {
	private String OutputType = "";

	/**
	 * ハッシュマップ
	 * key: DBフィールド名
	 * value: 値
	 */
	private HashMap<String, String> hmap;

	/**
	 * 処理で利用する配列
	 * 一時的に利用する変数
	 */
	private String tmpArray[];
	private String tmpText;

	/**
	 * オブジェクト
	 */
	private MysqlDb mysqlobject; //mysql

	//コンストラクタ
	T_Equipment(String server, String type, MysqlDb mysqlobject){
		OutputType = type;
//		mysqlobject = new MysqlDb(server);
		this.mysqlobject = mysqlobject;
	}

	/**
	 * T_Equipment 共通関数
	 * (INPUT/OUTPUT 共通ハッシュマップセット)
	 *
	 * @param text
	 */
	protected void common_method(String[] apamanarray) {
		hmap.put("Common_Use_Delivery_Box", apamanarray[193]);

		//91 ｵｰﾄﾛｯｸ/194 オートロック（0:無1:有）
		if(!apamanarray[90].equals("0") || !apamanarray[194].equals("0")) {
			hmap.put("Security_Auto_Lock", "1");
		}else {
			hmap.put("Security_Auto_Lock", "0");
		}

		//Emergency_Response
		if(!apamanarray[97].equals("0") || !apamanarray[195].equals("0")) {
			hmap.put("Emergency_Response", "1");
		}else {
			hmap.put("Emergency_Response", "0");
		}

		hmap.put("All_Electric", apamanarray[254]);
		hmap.put("Occupancy_Children", apamanarray[85]);
		hmap.put("Occupancy_Instrument", apamanarray[83]);
		hmap.put("Occupancy_Office", apamanarray[84]);
		hmap.put("Occupancy_Two_Person", apamanarray[93]);
		hmap.put("Occupancy_Sale_In_Lots", apamanarray[284]);
		hmap.put("Occupancy_Excellent_People_Wage", apamanarray[92]);
		hmap.put("Security_Camera", apamanarray[268]);
		hmap.put("Security_Dimple_Key", apamanarray[283]);
		hmap.put("Security_Glass", apamanarray[270]);
		hmap.put("Common_Use_Garbage_Storage", apamanarray[197]);
		hmap.put("Common_Use_Parking_Bicycles", apamanarray[199]);
		hmap.put("Common_Use_Motorcycle_parking", apamanarray[200]);
		hmap.put("Common_Use_Sunny", apamanarray[277]);
		hmap.put("Corner_Room", apamanarray[262]);
		hmap.put("Designers", apamanarray[255]);
		hmap.put("Barrier_Free", apamanarray[256]);
		hmap.put("Flooring", apamanarray[222]);
		hmap.put("Bay_Window", apamanarray[227]);
		hmap.put("Loft", apamanarray[223]);
		hmap.put("Sunroom", apamanarray[257]);
		hmap.put("Bed", apamanarray[249]);
		hmap.put("Lighting_Equipment", apamanarray[250]);
		hmap.put("Fully_Furnished", apamanarray[280]);
		hmap.put("Roof_Balcony", apamanarray[286]);
		hmap.put("Closet", apamanarray[231]);
		hmap.put("Walk_In_Closet", apamanarray[251]);
		hmap.put("Under_Floor_Storage", apamanarray[229]);
		hmap.put("Storeroom", apamanarray[269]);
		hmap.put("Trunk_Room", apamanarray[252]);
		hmap.put("Armoire", apamanarray[232]);
		hmap.put("Shoes_Box", apamanarray[274]);
		hmap.put("Bus_Toilet", apamanarray[226]);
		hmap.put("Shower", apamanarray[203]);
		hmap.put("Reheating", apamanarray[205]);
		hmap.put("Bathroom_Dryer", apamanarray[242]);
		hmap.put("Tv_With_Bus", apamanarray[243]);
		hmap.put("One_Tsubo", apamanarray[271]);
		hmap.put("Cleaning_Toilet_Seat", apamanarray[247]);
		hmap.put("Toilet_Seat", apamanarray[246]);
		hmap.put("Seperate", apamanarray[244]);
		hmap.put("Shampoo_Dresser", apamanarray[206]);
		hmap.put("Washing_Machine_Storage", apamanarray[224]);
		hmap.put("Dishwashers_Dryer", apamanarray[281]);
		hmap.put("Fridge", apamanarray[228]);
		hmap.put("System_Kitchen", apamanarray[220]);
		hmap.put("Kitchen_Face_To_Face", apamanarray[240]);
		hmap.put("Only_Cooling", apamanarray[208]);
		hmap.put("Heating_Only", apamanarray[209]);
		hmap.put("Floor_Heating", apamanarray[248]);
		hmap.put("Ventilation_System", apamanarray[288]);
		hmap.put("Central_Air_Conditioning", apamanarray[213]);
		hmap.put("Line_Bs", apamanarray[214]);
		hmap.put("Line_Cs", apamanarray[215]);
		hmap.put("Line_Catv", apamanarray[216]);
		hmap.put("Line_Wired", apamanarray[217]);
		hmap.put("Internet_Optical_Fiber", apamanarray[259]);
		hmap.put("Internet_Free", apamanarray[272]);

		//Gas
		if(!apamanarray[289].equals("0")) {
			hmap.put("Gas", "1");
		}else if(!apamanarray[290].equals("0")) {
			hmap.put("Gas", "2");
		}else {
			hmap.put("Gas", "0");
		}

		//Occupancy_Single_Person
		if(apamanarray[89].equals("1")) {
			hmap.put("Occupancy_Single_Person", "5");
		}else {
			hmap.put("Occupancy_Single_Person", "1");
		}

		//Occupancy_Elderly
		if(apamanarray[88].equals("1")) {
			hmap.put("Occupancy_Elderly", "3");
		}else {
			hmap.put("Occupancy_Elderly", "1");
		}

		//Occupancy_Corporation
		if(apamanarray[86].equals("1")) {
			hmap.put("Occupancy_Corporation", "3");
		}else {
			hmap.put("Occupancy_Corporation", "1");
		}

		//Occupancy_Pet
		if(apamanarray[82].equals("1")) {
			hmap.put("Occupancy_Pet", "3");
		}else {
			hmap.put("Occupancy_Pet", "1");
		}

		//Occupancy_Room_Share
		if(apamanarray[91].equals("1")) {
			hmap.put("Occupancy_Room_Share", "3");
		}else {
			hmap.put("Occupancy_Room_Share", "1");
		}

		//Occupancy_Sex
		if(apamanarray[87].equals("1")) {
			hmap.put("Occupancy_Sex", "3");
		}else {
			hmap.put("Occupancy_Sex", "1");
		}

		//Intercom
		if(!apamanarray[230].equals("0") || !apamanarray[236].equals("0")) {
			hmap.put("Emergency_Response", "1");
		}else {
			hmap.put("Emergency_Response", "0");
		}

		//Rooms_Facing_South
		if(apamanarray[71].equals("南")) {
			hmap.put("Rooms_Facing_South", "1");
		}else {
			hmap.put("Rooms_Facing_South", "0");
		}

		//Veranda_Balcony
		if(!apamanarray[225].equals("0") || !apamanarray[253].equals("0")) {
			hmap.put("Veranda_Balcony", "1");
		}else {
			hmap.put("Veranda_Balcony", "0");
		}

		//Storage_Space
		if(!hmap.get("Closet").equals("0") || !hmap.get("Walk_In_Closet").equals("0") || !hmap.get("Under_Floor_Storage").equals("0") || !hmap.get("Storeroom").equals("0") || !hmap.get("Trunk_Room").equals("0") || !hmap.get("Armoire").equals("0") ) {
			hmap.put("Storage_Space", "1");
		}else {
			hmap.put("Storage_Space", "0");
		}

		//Hot_Water_Supply
		if(!apamanarray[201].equals("0") || !apamanarray[202].equals("0")) {
			hmap.put("Hot_Water_Supply", "1");
		}else {
			hmap.put("Hot_Water_Supply", "0");
		}

		//Stove_Type
		if(apamanarray[289].equals("1")) {
			hmap.put("Stove_Type", "1");
		}else if(apamanarray[290].equals("1")) {
			hmap.put("Stove_Type", "2");
		}else if(apamanarray[233].equals("1")) {
			hmap.put("Stove_Type", "3");
		}else if(apamanarray[239].equals("1")) {
			hmap.put("Stove_Type", "4");
		}else {
			hmap.put("Stove_Type", "0");
		}

		//Heating_Type
		if(!apamanarray[210].equals("0")) {
			hmap.put("Heating_Type", "1");
		}else if(!apamanarray[211].equals("0")) {
			hmap.put("Heating_Type", "2");
		}else {
			hmap.put("Heating_Type", "0");
		}
	}

	/**
	 * Input関数
	 *
	 * @author masuda
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected void insert(int housing_id,String[] apamanarray) {
		hmap = new HashMap<String,String>();
		/*
		 * input・update　共通セット
		 */
		common_method(apamanarray);

		/*
		 * INSERT個別項目
		 */
		hmap.put("Housing_Id",String.valueOf(housing_id));
		hmap.put("Stove_Installation_Possible", apamanarray[263]);
		hmap.put("Private_Garden","0");

		/*
		 * DB INSERT処理
		 */
		if (OutputType.equals("db")) {
			//DB出力
			mysqlobject.insert_db("apahau_chintai","T_Equipment",hmap,"db");
		}else if(OutputType.equals("text")) {
			//Text出力
			mysqlobject.insert_db("apahau_chintai","T_Equipment",hmap,"text");
		}
		hmap = null;
	}

	/**
	 * update処理
	 *
	 * @author masuda
	 * @param housing_id 検索で利用するHousing_ID
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected String update(int housing_id,String[] apamanarray) {
		try {
			hmap = new HashMap<String,String>();
			/*
			 * input・update　共通セット
			 */
			common_method(apamanarray);

			/*
			 * UPDATE個別項目
			 */
			if(!apamanarray[95].equals("0") || !apamanarray[234].equals("0")) {
				hmap.put("Private_Garden", "1");
			}else {
				hmap.put("Private_Garden", "0");
			}

			/*
			 * DB UPDATE処理
			 */
			if (OutputType.equals("db")) {
				//DB出力
				//mysqlobject.update_db("apahau_chintai","T_Equipment",hmap,housing_id,"db");
				return create_update_sql("T_Equipment",hmap,housing_id);
			}else if(OutputType.equals("text")) {
				//Text出力
				//mysqlobject.update_db("apahau_chintai","T_Equipment",hmap,housing_id,"text");
			}
			return "";
		} finally {
			hmap = null;
		}
	}
}
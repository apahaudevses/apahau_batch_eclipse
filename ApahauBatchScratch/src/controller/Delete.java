package controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

import model.MysqlDb;
import module.Convert;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 実際の処理プログラム(DELETE文)
 * @author masuda
 *
 * ●コマンド
 * データベースインサート
 * java -jar delete local db
 * テキスト出力
 * java -jar delete local text
 *
 */
public class Delete {
	//ロガー
	protected static Logger logger = Logger.getLogger(Delete.class);

	public static void main(String[] args) {
		//jar実行パス
		final String EXECPATH = System.getProperty("user.dir");
		//final String DELIMITER = "\\";
		final String DELIMITER = "/";

		InputStreamReader isr = null;
		BufferedReader br = null;

		//SQL実行　物件数　カウンター
		int bukken_counter;
		bukken_counter = 0;

		try {

			//設定ファイルを読み込む
			PropertyConfigurator.configure("log4j.properties");

			MysqlDb mysqlobject = new MysqlDb(args[0]);		//mysqlオブジェクトのインスタンス化

			String processed_date = "";
			if(args[2] == ""){
				logger.debug("DELETE 実行開始：[" + Convert.getCurrentDateString() + "]");
			}else{
				logger.debug("DELETE 実行開始：[" + Convert.getCurrentDateString() + "] 指定日：[" + args[2] + "] より古いデータ対象");
				processed_date = args[2];
				if (processed_date.matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {
					processed_date = processed_date + " 00:00:00";
				}
			}

			isr = new InputStreamReader(new FileInputStream(EXECPATH + DELIMITER + "input_csv" + DELIMITER + "delete.csv"),"UTF-8");
			br = new BufferedReader(isr);

			//読み込んだ行を保存
			String line;

			//ファイルの件数分ループして配列に格納します。
			while ((line = br.readLine()) != null && !line.equals("")) {

				mysqlobject.open_db();							//mysqlオープン

				mysqlobject.delete_db(line,args[1],processed_date);

				bukken_counter++;

				mysqlobject.close_db();							//mysqlクローズ
			}

		} catch (SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

			logger.error(e.getMessage());

		} finally {
			logger.debug("DELETE 実行 物件数：[" + bukken_counter + "]");
			logger.debug("DELETE 実行終了：[" + Convert.getCurrentDateString() + "]");

			if (br != null) try { br.close(); } catch (IOException e) {}
			if (isr != null) try { isr.close(); } catch (IOException e) {}
		}
	}
}
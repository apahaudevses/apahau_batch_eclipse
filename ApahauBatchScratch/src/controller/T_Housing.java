package controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import model.MysqlDb;
import module.Convert;

/**
 * T_Housing変換
 *
 * @author masuda
 *
 */
public class T_Housing extends Convert {
	private String OutputType = "";
	/**
	 * ハッシュマップ
	 * key: DBフィールド名
	 * value: 値
	 */
//	private HashMap<String, String> hmap = new HashMap<String,String>();
	private HashMap<String, String> hmap;

	/**
	 * 処理で利用する配列
	 * 一時的に利用する変数
	 */
	private String tmpArray[];
	private String tmpText;

	/**
	 * オブジェクト
	 */
	private MysqlDb mysqlobject; //mysql

	/**
	 * データフォーマットインスタンス
	 */
	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Date d;

	//コンストラクタ
	T_Housing(String server, String type, MysqlDb mysqlobject){
		OutputType = type;
//		mysqlobject = new MysqlDb(server);
		this.mysqlobject = mysqlobject;
	}

	/**
	 * T_Housing 共通関数
	 * (INPUT/OUTPUT 共通ハッシュマップセット)
	 *
	 * @param text
	 */
	protected void common_method(String[] apamanarray) {
		hmap.put("Property_Type", type(apamanarray[50]));
		hmap.put("Property_Name_Phonetic", apamanarray[6]);
		hmap.put("Property_Branch", apamanarray[17]);
		hmap.put("Branch_FLG", apamanarray[293].replaceAll("[\n\r]", ""));
		hmap.put("Ido", apamanarray[291]);
		hmap.put("Keido", apamanarray[292]);
		hmap.put("Built_Years",apamanarray[55].replaceAll("$", "00"));
		hmap.put("Built_New_FLG",apamanarray[96]);
		hmap.put("Number_Of_Houses", apamanarray[48]);
		hmap.put("Ground_Floor", apamanarray[49]);
		hmap.put("Structure", structure(apamanarray[51]));
		hmap.put("Parking", apamanarray[144]);
		hmap.put("Parking_Distance", apamanarray[186]);
		hmap.put("Site_Stop_Number", apamanarray[145]);
		hmap.put("Parking_Type_6", apamanarray[261]);
		hmap.put("Appearance_Tiled", apamanarray[279]);
		hmap.put("Elevator", apamanarray[192]);
		hmap.put("Quiet_Residential_Area", apamanarray[278]);
		hmap.put("Converter_User_Id", apamanarray[9]);

		//TODO 非公開のときは入れない?
		//Property_Name
		if(apamanarray[7].equals("1")) {
			hmap.put("Property_Name", apamanarray[5]);
		}else {
			hmap.put("Property_Name", "");
		}

		//Property_1st_Zip_Code
		//Property_2nd_Zip_Code
		tmpArray = apamanarray[11].split("-");
		hmap.put("Property_1st_Zip_Code", tmpArray[0]);
		hmap.put("Property_2nd_Zip_Code", tmpArray[1]);

		//住所コード
		hmap.put("Property_Aza", get_azacd(apamanarray[12]));
		hmap.put("Prefectural_Id", apamanarray[13]);
		hmap.put("Property_Shiku", apamanarray[14]);
		hmap.put("Property_Choson", apamanarray[15]);

		//Updata_Date
		d = new Date();
		hmap.put("Updata_Date", df.format(d));

		//User_Id取得
		int user_id;
		//TODO 一時的にコメントアウト
		user_id = mysqlobject.get_userid(apamanarray[9]);
		hmap.put("User_Id",String.valueOf(user_id));

		//Display_FLG
		hmap.put("Display_FLG", "0");
		hmap.put("Delete_FLG", "0");
		hmap.put("Converter_id", "apaman");

		//Converter_Update_Date
		if(!apamanarray[191].equals("")){
			tmpText = apamanarray[191].substring(0, 4) + "-";
			tmpText = tmpText + apamanarray[191].substring(4, 6) + "-";
			tmpText = tmpText + apamanarray[191].substring(6, 8) + " ";
			tmpText = tmpText + apamanarray[165].substring(0, 2) + ":";
			tmpText = tmpText + apamanarray[165].substring(2, 4) + ":";
			tmpText = tmpText + apamanarray[165].substring(4, 6);

			hmap.put("Converter_Update_Date",tmpText);
		}
	}

	/**
	 * input関数
	 *
	 * @author masuda
	 * @param apamanarray
	 * @return int housing_id
	 *
	 */
	protected int insert(String[] apamanarray) {
		hmap = new HashMap<String,String>();
		/*
		 * input・update　共通セット
		 */
		common_method(apamanarray);

		/*
		 * INSERT個別項目
		 */
		hmap.put("Property_Management_Id", apamanarray[0]);

		d = new Date();
		hmap.put("Insert_Date", df.format(d));
		hmap.put("Converter_Insert_Date",apamanarray[190].substring(0, 4) + "-" + apamanarray[190].substring(4, 6) + "-" + apamanarray[190].substring(6, 8));

		/*
		 * DB UPDATE処理
		 */
		//Housing_id取得
		int Housing_id;
		Housing_id = -1;

		if (OutputType.equals("db")) {
			//DB出力
			Housing_id = mysqlobject.insert_db("apahau_chintai","T_Housing",hmap,"db");
		}else if(OutputType.equals("text")) {
			//Text出力
			Housing_id = mysqlobject.insert_db("apahau_chintai","T_Housing",hmap,"text");
		}
		hmap = null;
		return Housing_id;
	}

	/**
	 * update関数
	 *
	 * @author masuda
	 * @param apamanarray アパマン取得csv配列
	 * @return int housing_id
	 *
	 */
	protected String update(int housing_id, String[] apamanarray) {
		try {
			hmap = new HashMap<String,String>();
			/*
			 * input・update　共通セット
			 */
			common_method(apamanarray);

			/*
			 * UPDATE個別項目
			 */


			/*
			 * DB UPDATE処理
			 */
			if (OutputType.equals("db")) {
				//DB出力
				//mysqlobject.update_db("apahau_chintai","T_Housing",hmap,housing_id,"db");
				return create_update_sql("T_Housing", hmap, housing_id);
			}else if(OutputType.equals("text")) {
				//Text出力
				//mysqlobject.update_db("apahau_chintai","T_Housing",hmap,housing_id,"text");
			}
			hmap = null;
			return "";
		} finally {
			hmap = null;
		}
	}
}

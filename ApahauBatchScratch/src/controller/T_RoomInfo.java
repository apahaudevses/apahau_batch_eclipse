package controller;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.MysqlDb;
import module.Convert;

/**
 * T_RoomInfo変換
 * TODO ★料金系処理が必要なため途中まで★
 * どこからかは分割SQLに書かれている
 *
 * @author masuda
 *
 */
public class T_RoomInfo extends Convert {
	private String OutputType = "";
	/**
	 * ハッシュマップ
	 * key: DBフィールド名
	 * value: 値
	 */
	private HashMap<String, String> hmap;

	/**
	 * 処理で利用する配列
	 * 一時的に利用する変数
	 */
	private String tmpArray[];
	private String tmpText;
	private int tmpInt;

	/**
	 * オブジェクト
	 */
	private MysqlDb mysqlobject; //mysql

	/**
	 * 正規表現マッチング
	 */
	private String regex;
	private Pattern p;
	private Matcher m;

	//コンストラクタ
	T_RoomInfo(String server, String type, MysqlDb mysqlobject){
		OutputType = type;
//		mysqlobject = new MysqlDb(server);
		this.mysqlobject = mysqlobject;
	}

	/**
	 * T_RoomInfo 共通関数
	 * (INPUT/OUTPUT 共通ハッシュマップセット)
	 *
	 * @param text
	 */
	protected void common_method(String[] apamanarray) {
		hmap.put("Room_Number", apamanarray[4]);
		hmap.put("Basement", "1");
		hmap.put("Floor", apamanarray[164]);
		hmap.put("Opening_Direction",opening_direction(apamanarray[71]));
		hmap.put("Reform", apamanarray[276]);
		hmap.put("Renovation", apamanarray[275]);
		hmap.put("Rent",apamanarray[73]);

		/*
		 * TODO 要確認　フィールドがない
		//Top_Floor
		if(apamanarray[49].equals(apamanarray[164])) {
			hmap.put("Top_Floor", "1");
		}else {
			hmap.put("Top_Floor", "0");
		}
		*/

		//Move_In_Date
		if(!apamanarray[153].equals("")) {
			hmap.put("Move_In_Date","1");
		}else {
			hmap.put("Move_In_Date","0");
		}

		//Move_In_Year
		//Move_In_Month
		//Move_In_Time
		//TODO 要確認　Insertコメントアウトされている
		if(!apamanarray[153].equals("")) {
			//数字を半角に
			apamanarray[153] = fullWidthNumberToHalfWidthNumber(apamanarray[153]);
			//平成22年10月上旬
			regex = "([^\\d]*)(\\d+)年(\\d+)月([^\\d]*)";
			p = Pattern.compile(regex);
			m = p.matcher(apamanarray[153]);
			if (m.find()){
				switch (m.group(1)) {
				case "平成":
					hmap.put( "Move_In_Year", String.valueOf(1988 + Integer.parseInt(m.group(2))) );
					break;
				default:
					break;
				}
				//月
				hmap.put("Move_In_Month",m.group(3));
				//旬
				switch (m.group(4)) {
				case "上旬":
					hmap.put("Move_In_Time","1");
					break;
				case "中旬":
					hmap.put("Move_In_Time","2");
					break;
				case "下旬":
					hmap.put("Move_In_Time","3");
					break;
				default:
					hmap.put("Move_In_Time","0");
					break;
				}
			}

		}

		/*
		 * 料金系ここから
		 */
		//Parking_Fee_Deposit
		//Parking_Fee_Classification
		if(!apamanarray[184].equals("")) {
			hmap.put("Parking_Fee_Deposit", apamanarray[184]);
			hmap.put("Parking_Fee_Classification", "1");
		}else {
			hmap.put("Parking_Fee_Deposit", apamanarray[185]);
			hmap.put("Parking_Fee_Classification", "2");
		}

		//共益費
		//Common_Service_Fee
		//TODO ★要確認　管理費
		if( apamanarray[187].indexOf("円") >= 0 ) {
			hmap.put("Common_Service_Fee",get_number(apamanarray[187]));
		}else if( apamanarray[187].indexOf("ヶ月") >= 0 ) {
			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[187]);

			tmpInt = (int)(Float.parseFloat(get_number(tmpText)) * Float.parseFloat(apamanarray[73]));
			hmap.put("Common_Service_Fee",String.valueOf(tmpInt));
		}

		//保険料
		//Insurance
		if(!apamanarray[189].equals("") || !apamanarray[81].equals("")) {
			hmap.put("Insurance", "1");
		}else {
			hmap.put("Insurance", "2");
		}

		//Insurance_Number_Of_Years
		hmap.put("Insurance_Number_Of_Years",get_sqlintfield(apamanarray[189]));
		//Premium
		hmap.put("Premium",get_number(apamanarray[81]));

		//Contract_Type
		if(apamanarray[163].equals("0")) {
			hmap.put("Contract_Type", "1");
		}else {
			hmap.put("Contract_Type", "2");
		}

		//敷金保証金
		//Security_Deposits
		//Amount
		if(!"".equals(apamanarray[75])) {
			hmap.put("Security_Deposits","1");
			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[75]);
			hmap.put("Amount",get_number(tmpText));
		}else if(!"".equals(apamanarray[77])) {
			hmap.put("Security_Deposits","2");
			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[77]);
			hmap.put("Amount",get_number(tmpText));
		}

		//Security_Deposits_Classification
		if( apamanarray[75].indexOf("円") >= 0 || apamanarray[77].indexOf("円") >= 0) {
			hmap.put("Security_Deposits_Classification","2");
		}else if( apamanarray[75].indexOf("ヶ月") >= 0 || apamanarray[77].indexOf("ヶ月") >= 0) {
			hmap.put("Security_Deposits_Classification","1");
		}

		//礼金
		//Key_Money
		//Key_Money_Classification
		if( apamanarray[76].indexOf("円") >= 0 ) {
			hmap.put("Key_Money_Classification","2");
			hmap.put("Key_Money",get_number(apamanarray[76]));
		}else if( apamanarray[76].indexOf("ヶ月") >= 0 ) {
			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[76]);
			hmap.put("Key_Money",get_number(tmpText));
			hmap.put("Key_Money_Classification","1");
		}

		//更新料
		//Updated
		//Update_Amount
		if(!get_number(apamanarray[79]).equals("")) {
			hmap.put("Updated","1");
		}else {
			hmap.put("Updated","0");
		}

		//Update_Select
		if( apamanarray[79].indexOf("円") >= 0 ) {
			hmap.put("Update_Select","2");
			hmap.put("Update_Amount",get_number(apamanarray[79]));

		}else if( apamanarray[79].indexOf("ヶ月") >= 0 ) {
			hmap.put("Update_Select","1");

			tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[79]);
			hmap.put("Update_Amount",get_number(tmpText));
		}

		//Amortization_Insole_argument
		//csv
		//0:償却 2:解約引 3:敷引
		//DB
		//1償却／2.敷引
		switch (apamanarray[80]) {
		case "0":
			hmap.put("Amortization_Insole_argument","1");
			break;
		case "2":
			//TODO 要確認
			hmap.put("Amortization_Insole_argument","0");
			break;
		case "3":
			hmap.put("Amortization_Insole_argument","2");
			break;
		default:
			break;
		}

		//Amortization_Amount
		tmpText = fullWidthNumberToHalfWidthNumber(apamanarray[78]);
		hmap.put("Amortization_Amount",get_number(tmpText));

		//Amortization_Classification
		if( apamanarray[78].indexOf("円") >= 0 ) {
			hmap.put("Amortization_Classification","2");
		}else if( apamanarray[78].indexOf("ヶ月") >= 0 ) {
			hmap.put("Amortization_Classification","1");
		}else if( apamanarray[78].indexOf("％") >= 0 ) {
			hmap.put("Amortization_Classification","3");
		}

		/*
		 * TODO その他経費の入れ方　要確認
		 */
		//Other_Expenses_1
		//Other_Expenses_Tax_1
		//Other_Expenses_Nominal_1
		//Other_Expenses_Time_1
		if(!apamanarray[188].equals("")) {
			hmap.put("Other_Expenses_1", apamanarray[188]);
			hmap.put("Other_Expenses_Tax_1", "1");
			hmap.put("Other_Expenses_Nominal_1", "雑費");
			hmap.put("Other_Expenses_Time_1", "1");
		}else {
			hmap.put("Other_Expenses_Nominal_1", "");
		}

		//Other_Expenses_Tax_2
		//Other_Expenses_Nominal_2
		//Other_Expenses_Time_2
		if(!apamanarray[182].equals("") || !apamanarray[183].equals("")){
			hmap.put("Other_Expenses_Tax_2", "1");
			hmap.put("Other_Expenses_Nominal_2", "駐車場礼金");
			hmap.put("Other_Expenses_Time_2", "1");
		}else {
			hmap.put("Other_Expenses_Nominal_2", "");
		}

		//Other_Expenses_3
		//Other_Expenses_Tax_3
		//Other_Expenses_Nominal_3
		//Other_Expenses_Time_3
		if(!apamanarray[143].equals("")) {
			hmap.put("Other_Expenses_3",apamanarray[143]);
			hmap.put("Other_Expenses_Tax_3","1");
			hmap.put("Other_Expenses_Nominal_3","駐車場保証金");
			hmap.put("Other_Expenses_Time_3","1");
		}else {
			hmap.put("Other_Expenses_3","");
			hmap.put("Other_Expenses_Nominal_3","");
		}
		hmap.put("Parking_Fee", apamanarray[142]);
		/*
		 * 料金系ここまで
		 */

		hmap.put("Contract_Period_Month", get_sqlintfield(apamanarray[174]));
		hmap.put("Contract_Deadline_Year", get_sqlintfield(apamanarray[175]));
		hmap.put("Contract_Deadline_Month", get_sqlintfield(apamanarray[176]));
		hmap.put("Contract_Deadline_Day", get_sqlintfield(apamanarray[177]));

		//Form_Of_Transaction
		switch (apamanarray[8]) {
		case "仲介":
			hmap.put("Form_Of_Transaction", "3");
			break;
		case "専任媒介":
			hmap.put("Form_Of_Transaction", "4");
			break;
		case "貸主":
			hmap.put("Form_Of_Transaction", "1");
			break;
		case "代理":
			hmap.put("Form_Of_Transaction", "2");
			break;
		default:
			//TODO 確認
			break;
		}
		hmap.put("Based_Pricing_Service",apamanarray[157]);
		hmap.put("Service_Person",apamanarray[162]);
		hmap.put("Service_Phone",apamanarray[160]);

		//Floor_Number
		//Floor_Plan
		tmpArray = floor_no(apamanarray[63]);
		hmap.put("Floor_Number",tmpArray[0]);
		hmap.put("Floor_Plan",tmpArray[1]);

		//Room_Type_1
		//Room_Size_1
		hmap.put("Room_Type_1", room_type(get_textonly(apamanarray[64])));
		hmap.put("Room_Size_1", get_number(apamanarray[64]));

		//Room_Type_2
		//Room_Size_2
		hmap.put("Room_Type_2", room_type(get_textonly(apamanarray[65])));
		hmap.put("Room_Size_2", get_number(apamanarray[65]));

		//Room_Type_3
		//Room_Size_3
		hmap.put("Room_Type_3", room_type(get_textonly(apamanarray[66])));
		hmap.put("Room_Size_3", get_number(apamanarray[66]));

		//Room_Type_4
		//Room_Size_4
		hmap.put("Room_Type_4", room_type(get_textonly(apamanarray[67])));
		hmap.put("Room_Size_4", get_number(apamanarray[67]));

		//Room_Type_5
		//Room_Size_5
		hmap.put("Room_Type_5", room_type(get_textonly(apamanarray[68])));
		hmap.put("Room_Size_5", get_number(apamanarray[68]));

		//Room_Type_6
		//Room_Size_6
		hmap.put("Room_Type_6", room_type(get_textonly(apamanarray[69])));
		hmap.put("Room_Size_6", get_number(apamanarray[69]));

		//Room_Type_7
		//Room_Size_7
		hmap.put("Room_Type_7", room_type(get_textonly(apamanarray[70])));
		hmap.put("Room_Size_7", get_number(apamanarray[70]));

		hmap.put("Footprint_Square_Meters", apamanarray[52]);
		hmap.put("Footprint_Square_Tsubo", apamanarray[53]);

	}

	/**
	 * input関数
	 *
	 * @author masuda
	 * @param apamanarray
	 * @return void
	 *
	 *
	 * TODO　★料金系は要確認★
	 *
	 */
	protected void insert(int housing_id,String[] apamanarray) {
		hmap = new HashMap<String,String>();
		/*
		 * input・update　共通セット
		 */
		common_method(apamanarray);

		/*
		 * INSERT個別項目
		 */
		//Housing_Id
		hmap.put("Housing_Id",String.valueOf(housing_id));

		/*
		 * TODO 要確認　フィールドない
		hmap.put("Contract_Select", "");
		hmap.put("Contract_Period_Year", apamanarray[175]);
		*/

		//データベースに保存
		if (OutputType.equals("db")) {
			//DB出力
			mysqlobject.insert_db("apahau_chintai","T_RoomInfo",hmap,"db");
		}else if(OutputType.equals("text")) {
			//Text出力
			mysqlobject.insert_db("apahau_chintai","T_RoomInfo",hmap,"text");
		}
		hmap = null;
	}

	/**
	 * update処理
	 *
	 * @author masuda
	 * @param housing_id 検索で利用するHousing_ID
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected String update(int housing_id,String[] apamanarray) {
		try {
			hmap = new HashMap<String,String>();
			/*
			 * input・update　共通セット
			 */
			common_method(apamanarray);
			/*
			 * UPDATE個別項目
			 */

			/*
			 * DB UPDATE処理
			 */
			if (OutputType.equals("db")) {
				//DB出力
				//mysqlobject.update_db("apahau_chintai","T_RoomInfo",hmap,housing_id,"db");
				return create_update_sql("T_RoomInfo",hmap,housing_id);
			}else if(OutputType.equals("text")) {
				//Text出力
				//mysqlobject.update_db("apahau_chintai","T_RoomInfo",hmap,housing_id,"text");
			}
			return "";
		} finally {
			hmap = null;
		}
	}
}

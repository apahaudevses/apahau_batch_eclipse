package controller;

import java.util.HashMap;

import model.MysqlDb;
import module.Convert;


/**
 * T_Image変換
 *
 * @author masuda
 *
 */
public class T_Image extends Convert {
	private String OutputType = "";

	/**
	 * ハッシュマップ
	 * key: DBフィールド名
	 * value: 値
	 */
	private HashMap<String, String> hmap;

	/**
	 * 処理で利用する配列
	 * 一時的に利用する変数
	 */
	private String tmpArray[];
	private String tmpText;

	/**
	 * オブジェクト
	 */
	private MysqlDb mysqlobject; //mysql

	//コンストラクタ
	T_Image(String server,String type, MysqlDb mysqlobject){
		OutputType = type;
//		mysqlobject = new MysqlDb(server);
		this.mysqlobject = mysqlobject;
	}

	/**
	 * T_NearbyAttractions 共通関数
	 * (INPUT/OUTPUT 共通ハッシュマップセット)
	 *
	 * @param text
	 */
	protected void common_method(String[] apamanarray) {
		String image_path = "http://img.apamanshop.com/fs/Original/img/images/";
		image_path =  image_path + apamanarray[141] + "/";

		//Image_File_1
		if(!apamanarray[99].equals("")) {
			hmap.put("Image_File_1", image_path + apamanarray[99]);
		}else {
			hmap.put("Image_File_1", "");
		}

		//Image_File_2
		if(!apamanarray[114].equals("")) {
			hmap.put("Image_File_2", image_path + apamanarray[114]);
		}else {
			hmap.put("Image_File_2", "");
		}

		//Image_File_3
		if(!apamanarray[100].equals("")) {
			hmap.put("Image_File_3", image_path + apamanarray[100]);
		}else {
			hmap.put("Image_File_3", "");
		}

		//Image_File_4
		if(!apamanarray[101].equals("")) {
			hmap.put("Image_File_4", image_path + apamanarray[101]);
		}else {
			hmap.put("Image_File_4", "");
		}

		//Image_File_5
		if(!apamanarray[102].equals("")) {
			hmap.put("Image_File_5", image_path + apamanarray[102]);
		}else {
			hmap.put("Image_File_5", "");
		}
		//Image_File_6
		if(!apamanarray[103].equals("")) {
			hmap.put("Image_File_6", image_path + apamanarray[103]);
		}else {
			hmap.put("Image_File_6", "");
		}
		//Image_File_7
		if(!apamanarray[104].equals("")) {
			hmap.put("Image_File_7", image_path + apamanarray[104]);
		}else {
			hmap.put("Image_File_7", "");
		}

		//Image_File_8
		if(!apamanarray[105].equals("")) {
			hmap.put("Image_File_8", image_path + apamanarray[105]);
		}else {
			hmap.put("Image_File_8", "");
		}

		//Image_File_9
		if(!apamanarray[106].equals("")) {
			hmap.put("Image_File_9", image_path + apamanarray[106]);
		}else {
			hmap.put("Image_File_9", "");
		}

		//Image_File_10
		if(!apamanarray[107].equals("")) {
			hmap.put("Image_File_10", image_path + apamanarray[107]);
		}else {
			hmap.put("Image_File_10", "");
		}

		//Image_File_11
		if(!apamanarray[108].equals("")) {
			hmap.put("Image_File_11", image_path + apamanarray[108]);
		}else {
			hmap.put("Image_File_11", "");
		}

		//Image_File_12
		if(!apamanarray[109].equals("")) {
			hmap.put("Image_File_12", image_path + apamanarray[109]);
		}else {
			hmap.put("Image_File_12", "");
		}

		//Image_File_13
		if(!apamanarray[110].equals("")) {
			hmap.put("Image_File_13", image_path + apamanarray[110]);
		}else {
			hmap.put("Image_File_13", "");
		}

		//Image_File_14
		if(!apamanarray[111].equals("")) {
			hmap.put("Image_File_14", image_path + apamanarray[111]);
		}else {
			hmap.put("Image_File_14", "");
		}

		//Image_File_15
		if(!apamanarray[112].equals("")) {
			hmap.put("Image_File_15", image_path + apamanarray[112]);
		}else {
			hmap.put("Image_File_15", "");
		}

		//Image_File_16
		if(!apamanarray[113].equals("")) {
			hmap.put("Image_File_16", image_path + apamanarray[113]);
		}else {
			hmap.put("Image_File_16", "");
		}

		//Image_File_17
		if(!apamanarray[115].equals("")) {
			hmap.put("Image_File_17", image_path + apamanarray[115]);
		}else {
			hmap.put("Image_File_17", "");
		}

		//Image_File_18
		if(!apamanarray[116].equals("")) {
			hmap.put("Image_File_18", image_path + apamanarray[116]);
		}else {
			hmap.put("Image_File_18", "");
		}

		//Image_File_19
		if(!apamanarray[117].equals("")) {
			hmap.put("Image_File_19", image_path + apamanarray[117]);
		}else {
			hmap.put("Image_File_19", "");
		}

		//Image_File_20
		if(!apamanarray[119].equals("")) {
			hmap.put("Image_File_20", image_path + apamanarray[119]);
		}else {
			hmap.put("Image_File_20", "");
		}

		//Image_File_21
		if(!apamanarray[121].equals("")) {
			hmap.put("Image_File_21", image_path + apamanarray[121]);
		}else {
			hmap.put("Image_File_21", "");
		}

		//Image_File_22
		if(!apamanarray[123].equals("")) {
			hmap.put("Image_File_22", image_path + apamanarray[123]);
		}else {
			hmap.put("Image_File_22", "");
		}

		//Image_File_23
		if(!apamanarray[125].equals("")) {
			hmap.put("Image_File_23", image_path + apamanarray[125]);
		}else {
			hmap.put("Image_File_23", "");
		}

		//Image_File_24
		if(!apamanarray[127].equals("")) {
			hmap.put("Image_File_24", image_path + apamanarray[127]);
		}else {
			hmap.put("Image_File_24", "");
		}

		//Image_File_25
		if(!apamanarray[129].equals("")) {
			hmap.put("Image_File_25", image_path + apamanarray[129]);
		}else {
			hmap.put("Image_File_25", "");
		}

		//Image_File_26
		if(!apamanarray[131].equals("")) {
			hmap.put("Image_File_26", image_path + apamanarray[131]);
		}else {
			hmap.put("Image_File_26", "");
		}

		//Image_File_27
		if(!apamanarray[133].equals("")) {
			hmap.put("Image_File_27", image_path + apamanarray[133]);
		}else {
			hmap.put("Image_File_27", "");
		}

		//Image_File_28
		if(!apamanarray[135].equals("")) {
			hmap.put("Image_File_28", image_path + apamanarray[135]);
		}else {
			hmap.put("Image_File_28", "");
		}

		//Image_File_29
		if(!apamanarray[137].equals("")) {
			hmap.put("Image_File_29", image_path + apamanarray[137]);
		}else {
			hmap.put("Image_File_29", "");
		}

		//Image_File_30
		if(!apamanarray[139].equals("")) {
			hmap.put("Image_File_30", image_path + apamanarray[139]);
		}else {
			hmap.put("Image_File_30", "");
		}
	}

	/**
	 * input関数
	 *
	 * @author masuda
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected void insert(int housing_id,String[] apamanarray) {
		hmap = new HashMap<String,String>();
		/*
		 * input・update　共通セット
		 */
		common_method(apamanarray);

		/*
		 * INSERT個別項目
		 */
		//Housing_Id
		hmap.put("Housing_Id",String.valueOf(housing_id));
		/*
		 * DB INSERT処理
		 */
		if (OutputType.equals("db")) {
			//DB出力
			mysqlobject.insert_db("apahau_chintai","T_Image",hmap,"db");
		}else if(OutputType.equals("text")) {
			//Text出力
			mysqlobject.insert_db("apahau_chintai","T_Image",hmap,"text");
		}
		hmap = null;
	}

	/**
	 * update処理
	 *
	 * @author masuda
	 * @param housing_id 検索で利用するHousing_ID
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected String update(int housing_id,String[] apamanarray) {
		try {
			hmap = new HashMap<String,String>();
			/*
			 * input・update　共通セット
			 */
			common_method(apamanarray);
			/*
			 * UPDATE個別項目
			 */

			/*
			 * DB UPDATE処理
			 */
			if (OutputType.equals("db")) {
				//DB出力
				//mysqlobject.update_db("apahau_chintai","T_Image",hmap,housing_id,"db");
				return create_update_sql("T_Image",hmap,housing_id);
			}else if(OutputType.equals("text")) {
				//Text出力
				//mysqlobject.update_db("apahau_chintai","T_Image",hmap,housing_id,"text");
			}
			return "";
		} finally {
			hmap = null;
		}
	}
}

package controller;

import java.util.HashMap;

import model.MysqlDb;
import module.Convert;


/**
 * T_Equipment変換
 *
 * @author masuda
 *
 */

public class T_SearchTraffic extends Convert {
	private String OutputType = "";
	/**
	 * ハッシュマップ
	 * key: DBフィールド名
	 * value: 値
	 */
	private HashMap<String, String> hmap;

	/**
	 * 処理で利用する配列
	 * 一時的に利用する変数
	 */
	private String tmpArray[];
	private String tmpText;

	/**
	 * オブジェクト
	 */
//	private Convert convertobject = new Convert(); //module
	private MysqlDb mysqlobject; //mysql

	//コンストラクタ
	T_SearchTraffic(String server, String type, MysqlDb mysqlobject){
		OutputType = type;
//		mysqlobject = new MysqlDb(server);
		this.mysqlobject = mysqlobject;
	}

	/**
	 * T_SearchTraffic 共通関数
	 * (INPUT/OUTPUT 共通ハッシュマップセット)
	 *
	 * @param text
	 */
	protected void common_method(String[] apamanarray) {
		//沿線駅コード1
		tmpText = get_routecd(apamanarray[18],apamanarray[19]);
		hmap.put("Route_1",tmpText);
		hmap.put("Station_1",tmpText);
		hmap.put("Walk_Time_1", apamanarray[23]);
		hmap.put("Bus_Line_1", apamanarray[24]);
		hmap.put("Bus_Stop_1", apamanarray[25]);
		hmap.put("Bus_Ride_Time_1", apamanarray[26]);
		hmap.put("Bus_Stop_Walk_1", apamanarray[27]);

		//沿線駅コード2
		tmpText = get_routecd(apamanarray[28],apamanarray[29]);
		hmap.put("Route_2",tmpText);
		hmap.put("Station_2",tmpText);
		hmap.put("Walk_Time_2", apamanarray[33]);
		hmap.put("Bus_Line_2", apamanarray[34]);
		hmap.put("Bus_Stop_2", apamanarray[35]);
		hmap.put("Bus_Ride_Time_2", apamanarray[36]);
		hmap.put("Bus_Stop_Walk_2", apamanarray[37]);

		//沿線駅コード3
		tmpText = get_routecd(apamanarray[38],apamanarray[39]);
		hmap.put("Route_3",tmpText);
		hmap.put("Station_3",tmpText);
		hmap.put("Walk_Time_3", apamanarray[43]);
		hmap.put("Bus_Line_3", apamanarray[44]);
		hmap.put("Bus_Stop_3", apamanarray[45]);
		hmap.put("Bus_Ride_Time_3", apamanarray[46]);
		hmap.put("Bus_Stop_Walk_3", apamanarray[47]);
	}

	/**
	 * input関数
	 * Floor_Plan
	 *
	 * @param text
	 */
	protected void insert(int housing_id,String[] apamanarray) {
		hmap = new HashMap<String,String>();
		/*
		 * input・update　共通セット
		 */
		common_method(apamanarray);

		/*
		 * INSERT個別項目
		 */
		//Housing_Id
		hmap.put("Housing_Id",String.valueOf(housing_id));

		/*
		 * DB INSERT処理
		 */
		if (OutputType.equals("db")) {
			//DB出力
			mysqlobject.insert_db("apahau_search","T_SearchTraffic",hmap,"db");
		}else if(OutputType.equals("text")) {
			//Text出力
			mysqlobject.insert_db("apahau_search","T_SearchTraffic",hmap,"text");
		}
		hmap = null;

	}

	/**
	 * update処理
	 *
	 * @author masuda
	 * @param housing_id 検索で利用するHousing_ID
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected String update(int housing_id,String[] apamanarray) {
		try {
			hmap = new HashMap<String,String>();
			/*
			 * input・update　共通セット
			 */
			common_method(apamanarray);
			/*
			 * UPDATE個別項目
			 */

			/*
			 * DB UPDATE処理
			 */
			if (OutputType.equals("db")) {
				//DB出力
				//mysqlobject.update_db("apahau_search","T_SearchTraffic",hmap,housing_id,"db");
				return create_update_sql("T_SearchTraffic", hmap, housing_id);
			}else if(OutputType.equals("text")) {
				//Text出力
				//mysqlobject.update_db("apahau_search","T_SearchTraffic",hmap,housing_id,"text");
			}
			return "";
		} finally {
			hmap = null;
		}
	}
}

package controller;

import java.util.HashMap;

import model.MysqlDb;
import module.Convert;


/**
 * T_NearbyAttractions変換
 *
 * @author masuda
 *
 */

public class T_NearbyAttractions extends Convert {
	private String OutputType = "";
	/**
	 * ハッシュマップ
	 * key: DBフィールド名
	 * value: 値
	 */
	private HashMap<String, String> hmap;

	/**
	 * 処理で利用する配列
	 * 一時的に利用する変数
	 */
	private String tmpArray[];
	private String tmpText;

	/**
	 * オブジェクト
	 */
	private MysqlDb mysqlobject; //mysql


	//コンストラクタ
	T_NearbyAttractions(String server,String type, MysqlDb mysqlobject){
		OutputType = type;
//		mysqlobject = new MysqlDb(server);
		this.mysqlobject = mysqlobject;
	}

	/**
	 * T_NearbyAttractions 共通関数
	 * (INPUT/OUTPUT 共通ハッシュマップセット)
	 *
	 * @param text
	 */
	protected void common_method(String[] apamanarray) {
		/*
		 * TODO 要確認
		 */
		//Neighborhood_Category_Id_1
		//Neighborhood_Info_Distance_1
		if(!apamanarray[56].equals("") || !apamanarray[57].equals("")) {
			hmap.put("Neighborhood_Info_Name_1",apamanarray[56]);
			hmap.put("Neighborhood_Category_Id_1","9");
			hmap.put("Neighborhood_Info_Distance_1",get_sqlintfield(apamanarray[57]));
		}

		//Neighborhood_Category_Id_2
		//Neighborhood_Info_Distance_2
		if(!apamanarray[58].equals("") || !apamanarray[59].equals("")) {
			hmap.put("Neighborhood_Category_Id_2","10");
			hmap.put("Neighborhood_Info_Name_2",apamanarray[58]);
			hmap.put("Neighborhood_Info_Distance_2",get_sqlintfield(apamanarray[59]));
		}

		//Neighborhood_Category_Id_3
		//Neighborhood_Info_Distance_3
		if(!apamanarray[60].equals("")) {
			hmap.put("Neighborhood_Category_Id_3","3");
			hmap.put("Neighborhood_Info_Distance_3",get_sqlintfield(apamanarray[60]));
		}

		//Neighborhood_Category_Id_4
		//Neighborhood_Info_Distance_4
		if(!apamanarray[61].equals("")) {
			hmap.put("Neighborhood_Category_Id_4","2");
			hmap.put("Neighborhood_Info_Distance_4",get_sqlintfield(apamanarray[61]));
		}

		//Neighborhood_Category_Id_5
		//Neighborhood_Info_Distance_5
		if(!apamanarray[62].equals("")) {
			hmap.put("Neighborhood_Category_Id_5","12");
			hmap.put("Neighborhood_Info_Distance_5",get_sqlintfield(apamanarray[62]));
		}
	}

	/**
	 * input関数
	 *
	 * @author masuda
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected void insert(int housing_id,String[] apamanarray) {
		hmap = new HashMap<String,String>();
		/*
		 * input・update　共通セット
		 */
		common_method(apamanarray);

		/*
		 * INSERT個別項目
		 */
		//Housing_Id
		hmap.put("Housing_Id",String.valueOf(housing_id));

		/*
		 * DB INSERT処理
		 */
		if (OutputType.equals("db")) {
			//DB出力
			mysqlobject.insert_db("apahau_chintai","T_NearbyAttractions",hmap,"db");
		}else if(OutputType.equals("text")) {
			//Text出力
			mysqlobject.insert_db("apahau_chintai","T_NearbyAttractions",hmap,"text");
		}
		hmap = null;
	}

	/**
	 * update処理
	 *
	 * @author masuda
	 * @param housing_id 検索で利用するHousing_ID
	 * @param apamanarray
	 * @return void
	 *
	 */
	protected String update(int housing_id,String[] apamanarray) {
		try {
			hmap = new HashMap<String,String>();
			/*
			 * input・update　共通セット
			 */
			common_method(apamanarray);

			/*
			 * UPDATE個別項目
			 */

			/*
			 * DB UPDATE処理
			 */
			if (OutputType.equals("db")) {
				//DB出力
				//mysqlobject.update_db("apahau_chintai","T_NearbyAttractions",hmap,housing_id,"db");
				return create_update_sql("T_NearbyAttractions",hmap,housing_id);
			}else if(OutputType.equals("text")) {
				//Text出力
				//mysqlobject.update_db("apahau_chintai","T_NearbyAttractions",hmap,housing_id,"text");
			}
			return "";
		} finally {
			hmap = null;
		}
	}

}

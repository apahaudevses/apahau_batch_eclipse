package controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import model.MysqlDb;
import module.Convert;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 実際の処理プログラム(INSERT文)
 * @author masuda
 *
 * ●コマンド
 * データベースインサート
 * java -jar insert local db
 * テキスト出力
 * java -jar insert local text
 *
 */
public class Insert {
	//ロガー
	protected static Logger logger = Logger.getLogger(Insert.class);

	public static void main(String[] args) {
		//jar実行パス
		final String EXECPATH = System.getProperty("user.dir");

		//final String DELIMITER = "\\";
		final String DELIMITER = "/";

		InputStreamReader isr = null;
		BufferedReader br = null;

		//SQL実行　物件数　カウンター
		int bukken_counter;
		bukken_counter = 0;

		try {

			//設定ファイルを読み込む
			PropertyConfigurator.configure("log4j.properties");

			MysqlDb mysqlobject = new MysqlDb(args[0]);		//mysqlオブジェクトのインスタンス化

			isr = new InputStreamReader(new FileInputStream(EXECPATH + DELIMITER + "input_csv" + DELIMITER + "insert.csv"),"UTF-8");
			br = new BufferedReader(isr);

			//読み込んだ行を保存
			String line;

			//アパハウcsvを入れておく配列
			String apamanarray[];

			//メモリ
			String max_memory;			// 使用可能最大

			int housing_id;
			housing_id = -1;

			max_memory = Long.toString(Runtime.getRuntime().maxMemory());
			logger.debug("INSERT 実行開始：[" + Convert.getCurrentDateString() + "] メモリ使用可能最大：[" + max_memory + "]");

			/*
			 * それぞれのDB用インスタンス生成
			 */
			T_SearchHousing t_searchhousing = new T_SearchHousing(args[0],args[1], mysqlobject);
			T_ChatchCopy t_chatchcopy = new T_ChatchCopy(args[0],args[1], mysqlobject);
			T_Article t_article = new T_Article(args[0],args[1], mysqlobject);
			T_Equipment t_equipment = new T_Equipment(args[0],args[1], mysqlobject);
			T_Image t_image = new T_Image(args[0],args[1], mysqlobject);
			T_RoomInfo t_roominfo = new T_RoomInfo(args[0],args[1], mysqlobject);
			T_NearbyAttractions t_nearbyattractions = new T_NearbyAttractions(args[0],args[1], mysqlobject);
			T_Traffic t_traffic = new T_Traffic(args[0],args[1], mysqlobject);
			T_SearchTraffic t_searchtraffic = new T_SearchTraffic(args[0],args[1], mysqlobject);
			T_Housing t_housing = new T_Housing(args[0],args[1], mysqlobject);

			//ファイルの件数分ループして配列に格納します。
			while ((line = br.readLine()) != null) {
				if(!line.equals("")) {
					//System.out.println(line);
					//タブで分割にする
					line = line.replaceAll("\",\"","\t");
					line = line.replaceAll("^\"","");
					line = line.replaceAll("\"$","");

					//分割配列作成(タブ)
					apamanarray = line.split("\t");

					mysqlobject.open_db();							//mysqlオープン

					/*
					 * 実際の変換処理
					 */
					//T_HOUSING変換
					housing_id = t_housing.insert(apamanarray);

					if(housing_id > 0){
						//T_Traffic変換
						t_traffic.insert(housing_id,apamanarray);
						//T_NearbyAttractions
						t_nearbyattractions.insert(housing_id,apamanarray);
						//T_Image
						t_image.insert(housing_id,apamanarray);
						//T_RoomInfo
						t_roominfo.insert(housing_id,apamanarray);
						//T_Equipment
						t_equipment.insert(housing_id,apamanarray);
						//T_ChatchCopy
						t_chatchcopy.insert(housing_id,apamanarray);
						//T_Article
						t_article.insert(housing_id,apamanarray);
						//T_SearchHousing
						t_searchhousing.insert(housing_id,apamanarray);
						//T_SearchTraffic
						t_searchtraffic.insert(housing_id,apamanarray);

						logger.debug("INSERT 実行 Housing_Id：[" + housing_id + "]");

						bukken_counter++;

						//ガベージコレクションの呼び出し
						System.gc();
					}else{
						logger.debug("INSERT 実行 Housing_Id：[" + housing_id + "] Property_Management_Id：[" + apamanarray[0] + "]");
					}

					mysqlobject.close_db();							//mysqlクローズ

				}
			}

		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			logger.debug("INSERT 実行 物件数：[" + bukken_counter + "]");
			logger.debug("INSERT 実行終了：[" + Convert.getCurrentDateString() + "]");

			if (br != null) try { br.close(); } catch (IOException e) {}
			if (isr != null) try { isr.close(); } catch (IOException e) {}
		}
	}
}
#!/bin/sh

if [ -e ./showcase_insert_shop.sql ]
then
	rm -f ./showcase_insert_shop.sql
fi

if [ -e ./showcase_update_shop.sql ]
then
	rm -f ./showcase_update_shop.sql
fi

CSV_TARGET=showcase_shop.csv
numLine=1

valueC=','
valueD='"'
count='1'

#
#	店舗Update
#
valueID=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "select T_ShowcaseShops.ID from T_ShowcaseShops left join apahau_chintai.T_ShopUser on apahau_chintai.T_ShopUser.Connected_2 = T_ShowcaseShops.ID where apahau_chintai.T_ShopUser.Connected_2 is not null GROUP BY ID;"`
#valueID=`mysql -h localhost -uapadb -pdak5t3a3ahfi5 -N apahau_temp -e "select T_ShowcaseShops.ID from T_ShowcaseShops left join apahau_chintai.T_ShopUser on apahau_chintai.T_ShopUser.Connected_2 = T_ShowcaseShops.ID where apahau_chintai.T_ShopUser.Connected_2 is not null GROUP BY ID;"`

echo $valueID | sed -e 's/\s/\n/g' | while read line
do
	valueData=`cat ${CSV_TARGET} | grep \"${line}\"`
	if [ -n "${valueData}" ]
	then

		# ----------------------------
		# 配列にセット
		# ----------------------------
		#","区切り文字列をタブ区切りに変換
		value_tab=`echo ${valueData} `
		
		#配列にセット
		IFS="$(echo -e '\t' )"
		set -- ${valueData}

        value='UPDATE T_ShopUser SET '

#Estate_License
		valueA='Estate_License='
		valueE=`echo ${3} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Name
		valueA='Estate_Name='
		valueE=`echo ${4} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Parson
		valueA='Estate_Parson='
		valueE=`echo ${5} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Zip_Cd
		valueA='Estate_Zip_Cd='
		valueE=`echo ${6} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Address
		valueA='Estate_Address='
		valueE=`echo ${7} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Line
		valueA='Estate_Line='
		valueE=`echo ${8} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Station
		valueA='Estate_Station='
		valueE=`echo ${9} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Walk
		valueA='Estate_Walk='
		valueE=`echo ${10} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Service_Time
		valueA='Estate_Service_Time='
		valueE=`echo ${11} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Holiday
		valueA='Estate_Holiday='
		valueE=`echo ${12} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Phone
		valueA='Estate_Phone='
		valueE=`echo ${13} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Fax
		valueA='Estate_Fax='
		valueE=`echo ${14} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_URL
		valueA='Estate_URL='
		valueE=`echo ${15} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Mail
		valueA='Estate_Mail='
		valueE=`echo ${16} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Chatch
		valueA='Estate_Chatch='
		valueE=`echo ${17} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Img_1
		valueA='Estate_Img_1='
		valueE=`echo ${18} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Img_2
		valueA='Estate_Img_2='
		valueE=`echo ${19} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Img_3
		valueA='Estate_Img_3='
		valueE=`echo ${20} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}

#Connected_2
		valueA=' WHERE Connected_2='
		valueE=`echo ${2} | sed -e 's/"//g'`
		value=${value}${valueA}${valueD}${valueE}${valueD}

		valueB=';'
		value=${value}${valueB}
		count=`expr ${count} + 1`

        echo $value >> showcase_update_shop.sql
	fi
done

#
#	店舗Insert
#

valueID=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "select T_ShowcaseShops.ID from T_ShowcaseShops left join apahau_chintai.T_ShopUser on apahau_chintai.T_ShopUser.Connected_2 = T_ShowcaseShops.ID where apahau_chintai.T_ShopUser.Connected_2 is null GROUP BY ID;"`
#valueID=`mysql -h localhost -uapadb -pdak5t3a3ahfi5 -N apahau_temp -e "select T_ShowcaseShops.ID from T_ShowcaseShops left join apahau_chintai.T_ShopUser on apahau_chintai.T_ShopUser.Connected_2 = T_ShowcaseShops.ID where apahau_chintai.T_ShopUser.Connected_2 is null GROUP BY ID;"`


echo $valueID | sed -e 's/\s/\n/g' | while read line
do
	valueData=`cat ${CSV_TARGET} | grep \"${line}\"`
	if [ -n "${valueData}" ]
	then

		# ----------------------------
		# 配列にセット
		# ----------------------------
		#","区切り文字列をタブ区切りに変換
		value_tab=`echo ${valueData} `
		
		#配列にセット
		IFS="$(echo -e '\t' )"
		set -- ${valueData}

        if ((${numLine}==1))
	    then
	        valueA='insert into T_ShopUser(Estate_License,Estate_Name,Estate_Parson,Estate_Zip_Cd,Estate_Address,Estate_Line,Estate_Station,Estate_Walk,Estate_Service_Time,Estate_Holiday,Estate_Phone,Estate_Fax,Estate_URL,Estate_Mail,Estate_Chatch,Estate_Img_1,Estate_Img_2,Estate_Img_3,Estate_Rank,Estate_Rank_State,Existence_FLG,Estate_Connected,Connected_2) value('
	    else
	        valueA=',('
	    fi
#Estate_License
		valueE=`echo ${3} | sed -e 's/"//g'`
		value=${valueA}${valueD}${valueE}${valueD}${valueC}
#Estate_Name
		valueE=`echo ${4} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Parson
		valueE=`echo ${5} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Zip_Cd
		valueE=`echo ${6} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Address
		valueE=`echo ${7} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Line
		valueE=`echo ${8} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Station
		valueE=`echo ${9} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Walk
		valueE=`echo ${10} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Service_Time
		valueE=`echo ${11} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Holiday
		valueE=`echo ${12} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Phone
		valueE=`echo ${13} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Fax
		valueE=`echo ${14} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_URL
		valueE=`echo ${15} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Mail
		valueE=`echo ${16} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Chatch
		valueE=`echo ${17} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Img_1
		valueE=`echo ${18} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Img_2
		valueE=`echo ${19} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Img_3
		valueE=`echo ${20} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Rank
		valueE="10"
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Rank_State
		valueE="1"
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Existence_FLG
		valueE="1"
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Estate_Connected
		valueE="2"
		value=${value}${valueD}${valueE}${valueD}${valueC}
#Connected_2
		valueE=`echo ${2} | sed -e 's/"//g'`
		value=${value}${valueD}${valueE}${valueD}

		if [ "${numLine}" -lt 1000 ]
		then
			valueB=')'
		else
			valueB=');'
		fi
		value=${value}${valueB}
		echo ${value} >> showcase_insert_shop.sql

		count=`expr ${count} + 1`
	fi
done

#【TODO】INSERT/UPDATEの仕様は？

#mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < showcase_update_shop.sql
mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < showcase_insert_shop.sql

#mysql -h localhost -uapadb -pdak5t3a3ahfi5 -N apahau_chintai < showcase_update_shop.sql
#mysql -h localhost -uapadb -pdak5t3a3ahfi5 -N apahau_chintai < showcase_insert_shop.sql

#nohup /bin/sh update_housing.sh &

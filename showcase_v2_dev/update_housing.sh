#!/bin/sh

if [ -e ./update_housing.sql ]
then
	rm -f ./update_housing.sql
fi

if [ -e ./update_searchhousing.sql ]
then
	rm -f ./update_searchhousing.sql
fi

numLine=0
year=`date +%Y`
oldyear=`expr "${year}" - 2`
newyear=`expr "${year}" + 10`
valueC=','
valueD='"'

CSV_TARGET=./showcase_bukken.csv
shopcode="ShowcaseTv"
errorFLG="FALSE"

# デバッグモード（無条件必須チェックの無効化）
debugMODE="FALSE"

cat $CSV_TARGET | while read valueData
do
	if [ -n "${valueData}" ]
	then

		# レコードタイプ
		type=`echo ${valueData} | cut -d " " -f 1 | sed -e 's/"//g'`
		# レコードタイプが 3:[物件情報新規登録・更新] 以外は読み飛ばす
		if [ "${type}" != "3" ]
		then
			continue
		fi
		# 貴社サイト店舗ID
		Converter_User_Id=`echo ${valueData} | cut -d " " -f 3 | sed -e 's/"//g'`
		# 貴社物件管理番号
		Property_Management_Id=`echo ${valueData} | cut -d " " -f 4 | sed -e 's/"//g'`

		# 貴社サイト店舗IDおよび貴社物件管理番号に値があること
		if [ "${Property_Management_Id}" == "" -o "${Converter_User_Id}" == "" ]
		then
			echo 貴社サイト店舗IDおよびおよび貴社物件管理番号に値がありません。処理をパスします。
			if [ "${debugMODE}" != "TRUE" ]
			then
				continue
			fi
		fi

		# ------------------------------------------------------------
		# 登録済み物件レコードの物件ID（PRIMARY KEY）を取得
		# ------------------------------------------------------------

		valueF="'"`echo ${Converter_User_Id}`"'"
		valueG="'"`echo ${Property_Management_Id}`"'"
#		Housing_Id=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Housing_Id FROM T_Housing WHERE Converter_User_Id = ${valueF} AND Property_Management_Id = ${valueG};"`
		Housing_Id=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Housing_Id FROM T_Housing WHERE Converter_User_Id = ${valueF} AND Property_Management_Id = ${valueG};"`
echo SELECT Housing_Id FROM T_Housing WHERE Converter_User_Id = ${valueF} AND Property_Management_Id = ${valueG};
		# ------------------------------------------------------------
		# 登録済み物件レコードがなければ処理しない
		# ------------------------------------------------------------

		##############################################################
		# テスト用
		#Housing_Id="1000"
		##############################################################

		if [ "${Housing_Id}" == "" ]
		then
			echo T_Housing にレコードがありません。処理をパスします。
			continue
		fi

		# ------------------------------------------------------------
		# 登録済み物件レコードの物件更新日時を取得
		# ------------------------------------------------------------

#		Update_Date=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Update_Date FROM T_Showcase WHERE Converter_User_Id = ${valueF} AND Property_Management_Id = ${valueG};"`
#テーブル「T_Showcase」のDB変更に伴う修正（84行：apahau_chintai->apahau_temp）
		Update_Date=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_temp -e "SELECT Update_Date FROM T_Showcase WHERE Converter_User_Id = ${valueF} AND Property_Management_Id = ${valueG};"`

echo SELECT Update_Date FROM T_Showcase WHERE Converter_User_Id = ${valueF} AND Property_Management_Id = ${valueG};
echo 'Update_Date:['$Update_Date']'

		# -n 文字列の長さが０でなければ true
		# ------------------------------------------------------------
		# 配列にセット
		# ------------------------------------------------------------
		i=1
		while [ $i -le 542 ]
		do
			LINE[$i]=`echo ${valueData} | cut -d " " -f $i | sed -e 's/"//g'`
			i=`expr $i + 1`
		done
		valueData=

		if [ "${errorFLG}" == "FALSE" ]
		then

		##############################################################
		# T_Housing
		##############################################################
			value="UPDATE T_Housing SET "

		#Housing_Id
		#Property_Type（無条件必須）
			if [ "${LINE[7]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Property_Type='
			valueE=`echo ${LINE[7]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_Name（無条件必須）
			if [ "${LINE[8]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Property_Name='
			valueE=`echo ${LINE[8]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_Name_Phonetic
			valueA='Property_Name_Phonetic='
			valueE=`echo ${LINE[9]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_1st_Zip_Code（無条件必須）
			if [ "${LINE[12]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Property_1st_Zip_Code='
			valueE=`echo ${LINE[12]} | cut -c1-3`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_2nd_Zip_Code
			valueA='Property_2nd_Zip_Code='
			valueE=`echo ${LINE[12]} | cut -c4-7`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Prefectural_Id（無条件必須）
			if [ "${LINE[13]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Prefectural_Id='
			valueE=`echo ${LINE[13]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_Shiku（無条件必須）
			if [ "${LINE[14]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Property_Shiku='
			valueE=`echo ${LINE[14]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_Choson（無条件必須）
			if [ "${LINE[15]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Property_Choson='
			valueE=`echo ${LINE[15]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_Aza（無条件必須）
			if [ "${LINE[16]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Property_Aza='
			valueE=`echo ${LINE[16]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_Branch（無条件必須）
			if [ "${LINE[17]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Property_Branch='
			valueE=`echo ${LINE[17]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Branch_FLG
			valueA='Branch_FLG='
			valueE=`echo ${LINE[18]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Ido
			valueA='Ido='
			valueE=`echo ${LINE[19]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Keido
			valueA='Keido='
			valueE=`echo ${LINE[20]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Built_Years（無条件必須）
			if [ "${LINE[53]}" == "" -o "${LINE[54]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Built_Years='
			valueX=`echo ${LINE[53]}`
			valueY=`echo ${LINE[54]}`
			if [ "${valueY}" -gt 0 -a "${valueY}" -lt 10 ]
			then
				valueY=`echo 0${valueY}`
			else
				valueY=`echo ${valueY}`
			fi
			if [ "${LINE[55]}" == "" ]
			then
				valueZ="00"
			else
				if [ "${LINE[55]}" -gt 0 -a "${LINE[55]}" -lt 32 ]
				then
					valueZ=`echo ${LINE[55]}`
				else
					valueZ="00"
				fi
			fi
			valueE=${valueX}${valueY}${valueZ}
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Built_New_FLG
			valueA='Built_New_FLG='
			valueE=`echo ${LINE[56]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Number_Of_Houses
			valueA='Number_Of_Houses='
			valueE=`echo ${LINE[57]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Ground_Floor
			valueA='Ground_Floor='
			valueE=`echo ${LINE[58]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Underground_Floor
			valueA='Underground_Floor='
			valueE=`echo ${LINE[59]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Structure
			valueA='Structure='
			valueE=`echo ${LINE[60]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking
			valueA='Parking='
			valueE=`echo ${LINE[61]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Distance
			valueA='Parking_Distance='
			valueE=`echo ${LINE[62]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Site_Stop_Number
			valueA='Site_Stop_Number='
			valueE=`echo ${LINE[63]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Off_Site_Stop_Number
			valueA='Off_Site_Stop_Number='
			valueE=`echo ${LINE[64]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type
			valueA='Parking_Type='
			valueE=`echo ${LINE[65]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_1
			valueA='Parking_Type_1='
			valueE=`echo ${LINE[66]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_2
			valueA='Parking_Type_2='
			valueE=`echo ${LINE[67]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_3
			valueA='Parking_Type_3='
			valueE=`echo ${LINE[68]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_4
			valueA='Parking_Type_4='
			valueE=`echo ${LINE[69]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_5
			valueA='Parking_Type_5='
			valueE=`echo ${LINE[70]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_6
			valueA='Parking_Type_6='
			valueE=`echo ${LINE[71]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_7
			valueA='Parking_Type_7='
			valueE=`echo ${LINE[72]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_8
			valueA='Parking_Type_8='
			valueE=`echo ${LINE[73]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_9
			valueA='Parking_Type_9='
			valueE=`echo ${LINE[74]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_10
			valueA='Parking_Type_10='
			valueE=`echo ${LINE[75]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_11
			valueA='Parking_Type_11='
			valueE=`echo ${LINE[76]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_12
			valueA='Parking_Type_12='
			valueE=`echo ${LINE[77]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_13
			valueA='Parking_Type_13='
			valueE=`echo ${LINE[78]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_14
			valueA='Parking_Type_14='
			valueE=`echo ${LINE[79]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_15
			valueA='Parking_Type_15='
			valueE=`echo ${LINE[80]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_16
			valueA='Parking_Type_16='
			valueE=`echo ${LINE[81]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_17
			valueA='Parking_Type_17='
			valueE=`echo ${LINE[82]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_18
			valueA='Parking_Type_18='
			valueE=`echo ${LINE[83]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_19
			valueA='Parking_Type_19='
			valueE=`echo ${LINE[84]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Type_20
			valueA='Parking_Type_20='
			valueE=`echo ${LINE[85]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#External_Insulation_Method
			valueA='External_Insulation_Method='
			valueE=`echo ${LINE[86]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Fire_Resistant_Construction
			valueA='Fire_Resistant_Construction='
			valueE=`echo ${LINE[87]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Insulation
			valueA='Insulation='
			valueE=`echo ${LINE[88]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Earthquake_Construction
			valueA='Earthquake_Construction='
			valueE=`echo ${LINE[89]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Wind_resistant_Construction
			valueA='Wind_resistant_Construction='
			valueE=`echo ${LINE[90]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Appearance_Tiled
			valueA='Appearance_Tiled='
			valueE=`echo ${LINE[91]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Elevator
			valueA='Elevator='
			valueE=`echo ${LINE[92]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Elevator_Number
			valueA='Elevator_Number='
			valueE=`echo ${LINE[93]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Quiet_Residential_Area
			valueA='Quiet_Residential_Area='
			valueE=`echo ${LINE[94]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Starting_Station
			valueA='Starting_Station='
			valueE=`echo ${LINE[95]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Flat_Terrain
			valueA='Flat_Terrain='
			valueE=`echo ${LINE[96]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Lush
			valueA='Lush='
			valueE=`echo ${LINE[97]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Two_Family_Dwelling
			valueA='Two_Family_Dwelling='
			valueE=`echo ${LINE[98]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Insert_Date
		#Updata_Date
			valueA='Updata_Date='
			valueE=`date +"%Y-%m-%d %H:%M:%S" | sed -e 's/"//g'`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Show_End_Date（無条件必須）
			if [ "${LINE[6]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Show_End_Date='
			valueE=`echo ${LINE[6]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#User_Id
			valueA='User_Id='
#			valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Id FROM T_ShopUser WHERE Connected_2 ='${LINE[3]}' limit 1;"`
			valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Id FROM T_ShopUser WHERE Connected_2 = '${LINE[3]}' limit 1;"`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Display_FLG（無条件必須）
			if [ "${LINE[10]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Display_FLG='
			valueE=`echo ${LINE[10]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Delete_FLG
			valueA='Delete_FLG='
			if [ "${LINE[11]}" == "" ]
			then
				valueE=0
			else
				if [ "${LINE[11]}" == "2" ]
				then
					valueE=2
				elif [ "${LINE[11]}" == "4" -o "${LINE[11]}" == "1" ]
				then
					valueE=1
				else
					valueE=0
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Converter_id
			valueA='Converter_id='
			valueE="ShowcaseTv"
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Converter_User_Id
			valueA='Converter_User_Id='
			valueE=`echo ${LINE[3]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Converter_Insert_Date
			valueA='Converter_Insert_Date='
			valueE=""
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Converter_Update_Date
			valueA='Converter_Update_Date='
			valueE=""
			value=${value}${valueA}${valueD}${valueE}${valueD}

			valueB=" WHERE Housing_Id="
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_housing.sql

		##############################################################
		# T_Traffic
		##############################################################
			value="UPDATE T_Traffic SET "

		#Housing_Id
		#Route_1
			valueA='Route_1='
			valueF=`echo ${LINE[21]}`
			valueG="-"
			valueH=`echo ${LINE[22]}`

#			valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = '${valueF}${valueG}${valueH}';"`
			valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = '${valueF}${valueG}${valueH}';"`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Station_1
			valueA='Station_1='
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}

		#Walk_Time_1
			valueA='Walk_Time_1='
			valueE=`echo ${LINE[23]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Line_1
			valueA='Bus_Line_1='
			valueE=`echo ${LINE[24]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_1
			valueA='Bus_Stop_1='
			valueE=`echo ${LINE[25]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_Walk_1
			valueA='Bus_Stop_Walk_1='
			valueE=`echo ${LINE[26]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Ride_Time_1
			valueA='Bus_Ride_Time_1='
			valueE=`echo ${LINE[27]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Transport_1
			valueA='Other_Transport_1='
			valueE=`echo ${LINE[28]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Route_2
			valueA='Route_2='
			valueF=`echo ${LINE[29]}`
			valueG="-"
			valueH=`echo ${LINE[30]}`

#			valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = '${valueF}${valueG}${valueH}';"`
			valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = '${valueF}${valueG}${valueH}';"`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Station_2
			valueA='Station_2='
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}

		#Walk_Time_2
			valueA='Walk_Time_2='
			valueE=`echo ${LINE[31]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Line_2
			valueA='Bus_Line_2='
			valueE=`echo ${LINE[32]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_2
			valueA='Bus_Stop_2='
			valueE=`echo ${LINE[33]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_Walk_2
			valueA='Bus_Stop_Walk_2='
			valueE=`echo ${LINE[34]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Ride_Time_2
			valueA='Bus_Ride_Time_2='
			valueE=`echo ${LINE[35]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Transport_2
			valueA='Other_Transport_2='
			valueE=`echo ${LINE[36]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Route_3
			valueA='Route_3='
			valueF=`echo ${LINE[37]}`
			valueG="-"
			valueH=`echo ${LINE[38]}`

#			valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = '${valueF}${valueG}${valueH}';"`
			valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = '${valueF}${valueG}${valueH}';"`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Station_3
			valueA='Station_3='
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_Time_3
			valueA='Walk_Time_3='
			valueE=`echo ${LINE[39]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Line_3
			valueA='Bus_Line_3='
			valueE=`echo ${LINE[40]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_3
			valueA='Bus_Stop_3='
			valueE=`echo ${LINE[41]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_Walk_3
			valueA='Bus_Stop_Walk_3='
			valueE=`echo ${LINE[42]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Ride_Time_3
			valueA='Bus_Ride_Time_3='
			valueE=`echo ${LINE[43]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Transport_3
			valueA='Other_Transport_3='
			valueE=`echo ${LINE[44]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Route_4
			valueA='Route_4='
			valueF=`echo ${LINE[45]}`
			valueG="-"
			valueH=`echo ${LINE[46]}`

#			valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = '${valueF}${valueG}${valueH}';"`
			valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = '${valueF}${valueG}${valueH}';"`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Station_4
			valueA='Station_4='
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_Time_4
			valueA='Walk_Time_4='
			valueE=`echo ${LINE[47]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Line_4
			valueA='Bus_Line_4='
			valueE=`echo ${LINE[48]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_4
			valueA='Bus_Stop_4='
			valueE=`echo ${LINE[49]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_Walk_4
			valueA='Bus_Stop_Walk_4='
			valueE=`echo ${LINE[50]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Ride_Time_4
			valueA='Bus_Ride_Time_4='
			valueE=`echo ${LINE[51]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Transport_4
			valueA='Other_Transport_4='
			valueE=`echo ${LINE[52]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}

			valueB=" WHERE Housing_Id = "
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_housing.sql

		##############################################################
		# T_RoomInfo
		##############################################################
			value="UPDATE T_RoomInfo SET "

		#Housing_Id
		#Room_Number
			valueA='Room_Number='
			valueE=`echo ${LINE[131]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Basement
			valueA='Basement='
			valueE=`echo ${LINE[132]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Floor
			valueA='Floor='
			valueE=`echo ${LINE[133]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Opening_Direction
			valueA='Opening_Direction='
			valueE=`echo ${LINE[134]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Current_State
			valueA='Current_State='
			valueE=`echo ${LINE[135]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Move_In_Date
			valueA='Move_In_Date='
			valueE=`echo ${LINE[136]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Move_In_Year
			valueA='Move_In_Year='
			valueE=`echo ${LINE[137]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Move_In_Month
			valueA='Move_In_Month='
			valueE=`echo ${LINE[138]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Move_In_Time
			valueA='Move_In_Time='
			valueE=`echo ${LINE[139]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Conditions
			valueA='Occupancy_Conditions='
			valueE=`echo ${LINE[140]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Reform
			valueA='Reform='
			valueE=`echo ${LINE[141]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Renovation
			valueA='Renovation='
			valueE=`echo ${LINE[142]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Die_Cast
			valueA='Die_Cast='
			valueE=`echo ${LINE[143]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Skeleton
			valueA='Skeleton='
			valueE=`echo ${LINE[144]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Rent（無条件必須）
			if [ "${LINE[145]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Rent='
			valueE=`echo ${LINE[145]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Service_Fee（無条件必須）
			if [ "${LINE[146]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Common_Service_Fee='
			valueE=`echo ${LINE[146]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Service_Fee_Classification
			valueA='Common_Service_Fee_Classification='
			valueE="2"
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Service_Fee_Tax
			valueA='Common_Service_Fee_Tax='
			valueE="1"
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Deposits
			valueA='Security_Deposits='
			valueE=`echo ${LINE[147]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Amount
			valueA='Amount='
			valueE=`echo ${LINE[148]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Deposits_Classification
			valueA='Security_Deposits_Classification='
			valueE=`echo ${LINE[149]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Conditions_Increase
			valueA='Conditions_Increase='
			valueE=`echo ${LINE[150]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Conditions_Pet
			valueA='Conditions_Pet='
			valueE=`echo ${LINE[151]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Conditions_Tabacco
			valueA='Conditions_Tabacco='
			valueE=`echo ${LINE[152]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Conditions_Children
			valueA='Conditions_Children='
			valueE=`echo ${LINE[153]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Conditions_Other
			valueA='Conditions_Other='
			valueE=`echo ${LINE[154]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Increase
			valueA='Increase='
			valueE=`echo ${LINE[155]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Increase_Classification
			valueA='Increase_Classification='
			valueE=`echo ${LINE[156]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Key_Money
			valueA='Key_Money='
			valueE=`echo ${LINE[157]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Key_Money_Classification
			valueA='Key_Money_Classification='
			valueE=`echo ${LINE[158]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Fee
			valueA='Parking_Fee='
			valueE=`echo ${LINE[159]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Fee_Tax
			valueA='Parking_Fee_Tax='
			valueE=`echo ${LINE[160]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Fee_Deposit
			valueA='Parking_Fee_Deposit='
			valueE=`echo ${LINE[161]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking_Fee_Classification
			valueA='Parking_Fee_Classification='
			valueE=`echo ${LINE[162]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Amortization_Insole_argument
			valueA='Amortization_Insole_argument='
			valueE=`echo ${LINE[163]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Amortization_Amount
			valueA='Amortization_Amount='
			valueE=`echo ${LINE[164]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Amortization_Classification
			valueA='Amortization_Classification='
			valueE=`echo ${LINE[165]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Amortization_Time
			valueA='Amortization_Time='
			valueE=`echo ${LINE[166]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Key_Exchange_Fee
			valueA='Key_Exchange_Fee='
			valueE=`echo ${LINE[167]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Key_Exchange_Tax
			valueA='Key_Exchange_Tax='
			valueE=`echo ${LINE[168]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_1
			valueA='Other_Expenses_1='
			valueE=`echo ${LINE[169]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Tax_1
			valueA='Other_Expenses_Tax_1='
			valueE=`echo ${LINE[170]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Nominal_1
			valueA='Other_Expenses_Nominal_1='
			valueE=`echo ${LINE[171]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Time_1
			valueA='Other_Expenses_Time_1='
			valueE=`echo ${LINE[172]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_2
			valueA='Other_Expenses_2='
			valueE=`echo ${LINE[173]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Tax_2
			valueA='Other_Expenses_Tax_2='
			valueE=`echo ${LINE[174]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Nominal_2
			valueA='Other_Expenses_Nominal_2='
			valueE=`echo ${LINE[175]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Time_2
			valueA='Other_Expenses_Time_2='
			valueE=`echo ${LINE[176]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_3
			valueA='Other_Expenses_3='
			valueE=`echo ${LINE[177]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Tax_3
			valueA='Other_Expenses_Tax_3='
			valueE=`echo ${LINE[178]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Nominal_3
			valueA='Other_Expenses_Nominal_3='
			valueE=`echo ${LINE[179]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Time_3
			valueA='Other_Expenses_Time_3='
			valueE=`echo ${LINE[180]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_4
			valueA='Other_Expenses_4='
			valueE=`echo ${LINE[181]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Tax_4
			valueA='Other_Expenses_Tax_4='
			valueE=`echo ${LINE[182]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Nominal_4
			valueA='Other_Expenses_Nominal_4='
			valueE=`echo ${LINE[183]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_Time_4
			valueA='Other_Expenses_Time_4='
			valueE=`echo ${LINE[184]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Expenses_5
		#Other_Expenses_Tax_5
		#Other_Expenses_Nominal_5
		#Other_Expenses_Time_5
		#Other_Expenses_6
		#Other_Expenses_Tax_6
		#Other_Expenses_Nominal_6
		#Other_Expenses_Time_6
		#Other_Expenses_7
		#Other_Expenses_Tax_7
		#Other_Expenses_Nominal_7
		#Other_Expenses_Time_7
		#Other_Expenses_8
		#Other_Expenses_Tax_8
		#Other_Expenses_Nominal_8
		#Other_Expenses_Time_8
		#Brokers_Commission
			valueA='Brokers_Commission='
			valueE=`echo ${LINE[185]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Brokers_Amount
			valueA='Brokers_Amount='
			valueE=`echo ${LINE[186]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Brokers_Classification
			valueA='Brokers_Classification='
			valueE=`echo ${LINE[187]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Brokers_Tax
			valueA='Brokers_Tax='
			valueE=`echo ${LINE[188]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Updated
			valueA='Updated='
			valueE=`echo ${LINE[189]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Update_Amount
			valueA='Update_Amount='
			valueE=`echo ${LINE[190]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Update_Select
			valueA='Update_Select='
			valueE=`echo ${LINE[191]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Free_Rent
			valueA='Free_Rent='
			valueE=`echo ${LINE[192]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Guarantor（無条件必須）
			if [ "${LINE[193]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Guarantor='
			valueE=`echo ${LINE[193]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Insurance_Companies_Use
			valueA='Insurance_Companies_Use='
			valueE=`echo ${LINE[194]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Utilization_Form
			valueA='Utilization_Form='
			valueE=`echo ${LINE[195]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Company_Name
			valueA='Company_Name='
			valueE=`echo ${LINE[196]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Guarantee_Charge
			valueA='Guarantee_Charge='
			valueE=`echo ${LINE[197]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Insurance（無条件必須）
			if [ "${LINE[198]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Insurance='
			valueE=`echo ${LINE[198]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Insurance_Number_Of_Years
			valueA='Insurance_Number_Of_Years='
			valueE=`echo ${LINE[199]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Premium
			valueA='Premium='
			valueE=`echo ${LINE[200]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Contract_Type（無条件必須）
			if [ "${LINE[201]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Contract_Type='
			valueE=`echo ${LINE[201]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Contract_Select（無条件必須）
			if [ "${LINE[202]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Contract_Select='
			valueE=`echo ${LINE[202]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Contract_Period_Year
			valueA='Contract_Period_Year='
			valueE=`echo ${LINE[203]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Contract_Period_Month
			valueA='Contract_Period_Month='
			valueE=`echo ${LINE[204]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Contract_Deadline_Year
			valueA='Contract_Deadline_Year='
			valueE=`echo ${LINE[205]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Contract_Deadline_Month
			valueA='Contract_Deadline_Month='
			valueE=`echo ${LINE[206]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Contract_Deadline_Day
			valueA='Contract_Deadline_Day='
			valueE=`echo ${LINE[207]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Form_Of_Transaction（無条件必須）
			if [ "${LINE[208]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Form_Of_Transaction='
			valueE=`echo ${LINE[208]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Based_Pricing_Service
			valueA='Based_Pricing_Service='
			valueE=`echo ${LINE[209]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Service_Person
			valueA='Service_Person='
			valueE=`echo ${LINE[210]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Service_Phone
			valueA='Service_Phone='
			valueE=`echo ${LINE[211]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Confirmed_Year
			valueA='Confirmed_Year='
			valueE=`echo ${LINE[212]} | cut -b1-4`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Confirmed_Month
			valueA='Confirmed_Month='
			valueE=`echo ${LINE[212]} | cut -b5-6`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Confirmed_Day
			valueA='Confirmed_Day='
			valueE=`echo ${LINE[212]} | cut -b7-8`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Floor_Number（無条件必須）
			if [ "${LINE[213]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Floor_Number='
			valueE=`echo ${LINE[213]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Floor_Plan（無条件必須）
			if [ "${LINE[214]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Floor_Plan='
			valueE=`echo ${LINE[214]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_1
			valueA='Room_Type_1='
			valueE=`echo ${LINE[215]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_1
			valueA='Room_Size_1='
			valueE=`echo ${LINE[216]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_2
			valueA='Room_Type_2='
			valueE=`echo ${LINE[217]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_2
			valueA='Room_Size_2='
			valueE=`echo ${LINE[218]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_3
			valueA='Room_Type_3='
			valueE=`echo ${LINE[219]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_3
			valueA='Room_Size_3='
			valueE=`echo ${LINE[220]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_4
			valueA='Room_Type_4='
			valueE=`echo ${LINE[221]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_4
			valueA='Room_Size_4='
			valueE=`echo ${LINE[222]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_5
			valueA='Room_Type_5='
			valueE=`echo ${LINE[223]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_5
			valueA='Room_Size_5='
			valueE=`echo ${LINE[224]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_6
			valueA='Room_Type_6='
			valueE=`echo ${LINE[225]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_6
			valueA='Room_Size_6='
			valueE=`echo ${LINE[226]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_7
			valueA='Room_Type_7='
			valueE=`echo ${LINE[227]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_7
			valueA='Room_Size_7='
			valueE=`echo ${LINE[228]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_8
			valueA='Room_Type_8='
			valueE=`echo ${LINE[229]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_8
			valueA='Room_Size_8='
			valueE=`echo ${LINE[230]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_9
			valueA='Room_Type_9='
			valueE=`echo ${LINE[231]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_9
			valueA='Room_Size_9='
			valueE=`echo ${LINE[232]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Type_10
			valueA='Room_Type_10='
			valueE=`echo ${LINE[233]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Room_Size_10
			valueA='Room_Size_10='
			valueE=`echo ${LINE[234]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Footprint_Square_Meters（無条件必須）
			if [ "${LINE[235]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Footprint_Square_Meters='
			valueE=`echo ${LINE[235]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Footprint_Square_Tsubo
			valueA='Footprint_Square_Tsubo='
			valueE=`echo ${LINE[236]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}

			valueB=" WHERE Housing_Id = "
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_housing.sql

		##############################################################
		# T_Equipment
		##############################################################
			value="UPDATE T_Equipment SET "

		#Housing_Id
		#Water_Supply（無条件必須）
			if [ "${LINE[237]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Water_Supply='
			valueE=`echo ${LINE[237]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Sewer（無条件必須）
			if [ "${LINE[238]}" == "" ]
			then
				if [ "${debugMODE}" != "TRUE" ]
				then
					continue
				fi
			fi
			valueA='Sewer='
			valueE=`echo ${LINE[238]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Gas
			valueA='Gas='
			valueE=`echo ${LINE[239]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#All_Electric
			valueA='All_Electric='
			valueE=`echo ${LINE[240]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Eco_Will
			valueA='Eco_Will='
			valueE=`echo ${LINE[241]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Eco_Cute
			valueA='Eco_Cute='
			valueE=`echo ${LINE[242]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Eco_Jaws
			valueA='Eco_Jaws='
			valueE=`echo ${LINE[243]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Solar_Power
			valueA='Solar_Power='
			valueE=`echo ${LINE[244]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Children
			valueA='Occupancy_Children='
			valueE=`echo ${LINE[245]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Instrument
			valueA='Occupancy_Instrument='
			valueE=`echo ${LINE[246]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Office
			valueA='Occupancy_Office='
			valueE=`echo ${LINE[247]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Two_Person
			valueA='Occupancy_Two_Person='
			valueE=`echo ${LINE[248]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Family
			valueA='Occupancy_Family='
			valueE=`echo ${LINE[249]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Sale_In_Lots
			valueA='Occupancy_Sale_In_Lots='
			valueE=`echo ${LINE[250]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Excellent_People_Wage
			valueA='Occupancy_Excellent_People_Wage='
			valueE=`echo ${LINE[251]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Student
			valueA='Occupancy_Student='
			valueE=`echo ${LINE[252]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Single_Person
			valueA='Occupancy_Single_Person='
			valueE=`echo ${LINE[253]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Elderly
			valueA='Occupancy_Elderly='
			valueE=`echo ${LINE[254]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Corporation
			valueA='Occupancy_Corporation='
			valueE=`echo ${LINE[255]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Pet
			valueA='Occupancy_Pet='
			valueE=`echo ${LINE[256]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Room_Share
			valueA='Occupancy_Room_Share='
			valueE=`echo ${LINE[257]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Sex
			valueA='Occupancy_Sex='
			valueE=`echo ${LINE[258]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Alien
			valueA='Occupancy_Alien='
			valueE=`echo ${LINE[259]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Management_From
			valueA='Management_From='
			valueE=`echo ${LINE[260]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Emergency_Response
			valueA='Emergency_Response='
			valueE=`echo ${LINE[261]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Strengthening_District
			valueA='Strengthening_District='
			valueE=`echo ${LINE[262]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Intercom
			valueA='Intercom='
			valueE=`echo ${LINE[263]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Camera
			valueA='Security_Camera='
			valueE=`echo ${LINE[264]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Motion_Sensors
			valueA='Security_Motion_Sensors='
			valueE=`echo ${LINE[265]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Fingerprint_Authentication
			valueA='Security_Fingerprint_Authentication='
			valueE=`echo ${LINE[266]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Vein_Authentication
			valueA='Security_Vein_Authentication='
			valueE=`echo ${LINE[267]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Key_Card
			valueA='Security_Key_Card='
			valueE=`echo ${LINE[268]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Key
			valueA='Security_Key='
			valueE=`echo ${LINE[269]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Door_Touch_Key
			valueA='Security_Door_Touch_Key='
			valueE=`echo ${LINE[270]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Door_Remote_Control_Key
			valueA='Security_Door_Remote_Control_Key='
			valueE=`echo ${LINE[271]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Dimple_Key
			valueA='Security_Dimple_Key='
			valueE=`echo ${LINE[272]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Double_Lock
			valueA='Security_Double_Lock='
			valueE=`echo ${LINE[273]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Electronic_Lock
			valueA='Security_Electronic_Lock='
			valueE=`echo ${LINE[274]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Auto_Lock
			valueA='Security_Auto_Lock='
			valueE=`echo ${LINE[275]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Shutter
			valueA='Security_Shutter='
			valueE=`echo ${LINE[276]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Glass
			valueA='Security_Glass='
			valueE=`echo ${LINE[277]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Fire_Alarm_System
			valueA='Fire_Alarm_System='
			valueE=`echo ${LINE[278]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Delivery_Box
			valueA='Common_Use_Delivery_Box='
			valueE=`echo ${LINE[279]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Cleaning_Box
			valueA='Common_Use_Cleaning_Box='
			valueE=`echo ${LINE[280]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Garbage_Storage
			valueA='Common_Use_Garbage_Storage='
			valueE=`echo ${LINE[281]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Parking_Bicycles
			valueA='Common_Use_Parking_Bicycles='
			valueE=`echo ${LINE[282]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Motorcycle_parking
			valueA='Common_Use_Motorcycle_parking='
			valueE=`echo ${LINE[283]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Athletic
			valueA='Common_Use_Athletic='
			valueE=`echo ${LINE[284]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Pool
			valueA='Common_Use_Pool='
			valueE=`echo ${LINE[285]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Kids_Room
			valueA='Common_Use_Kids_Room='
			valueE=`echo ${LINE[286]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Launderette
			valueA='Common_Use_Launderette='
			valueE=`echo ${LINE[287]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Theater_Room
			valueA='Common_Use_Theater_Room='
			valueE=`echo ${LINE[288]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Pet_Only_Facility
			valueA='Common_Use_Pet_Only_Facility='
			valueE=`echo ${LINE[289]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Big_Bath
			valueA='Common_Use_Big_Bath='
			valueE=`echo ${LINE[290]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_On_Site_Playground
			valueA='Common_Use_On_Site_Playground='
			valueE=`echo ${LINE[291]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Set_Post
			valueA='Common_Use_Set_Post='
			valueE=`echo ${LINE[292]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Sunny
			valueA='Common_Use_Sunny='
			valueE=`echo ${LINE[293]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Light_court
			valueA='Light_court='
			valueE=`echo ${LINE[294]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Two_surface_lighting
			valueA='Two_surface_lighting='
			valueE=`echo ${LINE[295]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Three_surface_lighting
			valueA='Three_surface_lighting='
			valueE=`echo ${LINE[296]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#All_Rooms_lighting
			valueA='All_Rooms_lighting='
			valueE=`echo ${LINE[297]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Three_way_House
			valueA='Three_way_House='
			valueE=`echo ${LINE[298]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Four_way_House
			valueA='Four_way_House='
			valueE=`echo ${LINE[299]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Without_Front_Building
			valueA='Without_Front_Building='
			valueE=`echo ${LINE[300]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#All_Rooms_Direction
			valueA='All_Rooms_Direction='
			valueE=`echo ${LINE[301]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Second_Floor_Ldk
			valueA='Second_Floor_Ldk='
			valueE=`echo ${LINE[302]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Second_Floor_Living
			valueA='Second_Floor_Living='
			valueE=`echo ${LINE[303]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Corner_Room
			valueA='Corner_Room='
			valueE=`echo ${LINE[304]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Top_Floor
			valueA='Top_Floor='
			valueE=`echo ${LINE[305]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Onsen
			valueA='Onsen='
			valueE=`echo ${LINE[306]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Designers
			valueA='Designers='
			valueE=`echo ${LINE[307]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Good_Ventilation
			valueA='Good_Ventilation='
			valueE=`echo ${LINE[308]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Good_View
			valueA='Good_View='
			valueE=`echo ${LINE[309]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Rooms_Facing_South
			valueA='Rooms_Facing_South='
			valueE=`echo ${LINE[310]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Barrier_Free
			valueA='Barrier_Free='
			valueE=`echo ${LINE[311]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Slope
		#Distribution
			valueA='Distribution='
			valueE=`echo ${LINE[312]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Flooring
			valueA='Flooring='
			valueE=`echo ${LINE[313]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bay_Window
			valueA='Bay_Window='
			valueE=`echo ${LINE[314]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Skylight
			valueA='Skylight='
			valueE=`echo ${LINE[315]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Blow_By
			valueA='Blow_By='
			valueE=`echo ${LINE[316]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Maisonette
			valueA='Maisonette='
			valueE=`echo ${LINE[317]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Under_Ground_Room
			valueA='Under_Ground_Room='
			valueE=`echo ${LINE[318]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Loft
			valueA='Loft='
			valueE=`echo ${LINE[319]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Breadth_Loft
			valueA='Breadth_Loft='
			valueE=`echo ${LINE[320]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Free_Space
			valueA='Free_Space='
			valueE=`echo ${LINE[321]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Sunroom
			valueA='Sunroom='
			valueE=`echo ${LINE[322]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Soundproof_Room
			valueA='Soundproof_Room='
			valueE=`echo ${LINE[323]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Cushion_Floor
			valueA='Cushion_Floor='
			valueE=`echo ${LINE[324]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Entrance_Porch
			valueA='Entrance_Porch='
			valueE=`echo ${LINE[325]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Entrance_Hall
			valueA='Entrance_Hall='
			valueE=`echo ${LINE[326]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Entrance_Handrail
			valueA='Entrance_Handrail='
			valueE=`echo ${LINE[327]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Carpet_Upholstery
			valueA='Carpet_Upholstery='
			valueE=`echo ${LINE[328]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bed
			valueA='Bed='
			valueE=`echo ${LINE[329]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Storage_Bed
			valueA='Storage_Bed='
			valueE=`echo ${LINE[330]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Lighting_Equipment
			valueA='Lighting_Equipment='
			valueE=`echo ${LINE[331]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Auto_Light
			valueA='Auto_Light='
			valueE=`echo ${LINE[332]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Indirect_Lighting
			valueA='Indirect_Lighting='
			valueE=`echo ${LINE[333]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#All_Rooms_With_Lighting
			valueA='All_Rooms_With_Lighting='
			valueE=`echo ${LINE[334]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Curtains
			valueA='Curtains='
			valueE=`echo ${LINE[335]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Blind_With
			valueA='Blind_With='
			valueE=`echo ${LINE[336]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Roll_Screen_With
			valueA='Roll_Screen_With='
			valueE=`echo ${LINE[337]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Fully_Furnished
			valueA='Fully_Furnished='
			valueE=`echo ${LINE[338]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#All_Rooms_Flooring
			valueA='All_Rooms_Flooring='
			valueE=`echo ${LINE[339]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#South_Side_Living
			valueA='South_Side_Living='
			valueE=`echo ${LINE[340]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Double_Glazing
			valueA='Double_Glazing='
			valueE=`echo ${LINE[341]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Veranda_Balcony
			valueA='Veranda_Balcony='
			valueE=`echo ${LINE[342]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Balcony_Breadth
			valueA='Balcony_Breadth='
			valueE=`echo ${LINE[343]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Terrace
			valueA='Terrace='
			valueE=`echo ${LINE[344]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Roof_Balcony
			valueA='Roof_Balcony='
			valueE=`echo ${LINE[345]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Two_wey_Balcony
			valueA='Two_wey_Balcony='
			valueE=`echo ${LINE[346]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Two_Side_Balcony
			valueA='Two_Side_Balcony='
			valueE=`echo ${LINE[347]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Three_Surface_Balcony
			valueA='Three_Surface_Balcony='
			valueE=`echo ${LINE[348]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#L_Shaped_Balcony
			valueA='L_Shaped_Balcony='
			valueE=`echo ${LINE[349]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Inner_Balcony
			valueA='Inner_Balcony='
			valueE=`echo ${LINE[350]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Wood_Deck
			valueA='Wood_Deck='
			valueE=`echo ${LINE[351]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Outdoor_Power
			valueA='Outdoor_Power='
			valueE=`echo ${LINE[352]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shutter_Shutters
			valueA='Shutter_Shutters='
			valueE=`echo ${LINE[353]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Private_Garden
			valueA='Private_Garden='
			valueE=`echo ${LINE[354]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Courtyard
			valueA='Courtyard='
			valueE=`echo ${LINE[355]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Spot_Garden
			valueA='Spot_Garden='
			valueE=`echo ${LINE[356]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Storage_Space
			valueA='Storage_Space='
			valueE=`echo ${LINE[357]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Closet
			valueA='Closet='
			valueE=`echo ${LINE[358]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Closet_number
			valueA='Closet_number='
			valueE=`echo ${LINE[359]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_In_Closet
			valueA='Walk_In_Closet='
			valueE=`echo ${LINE[360]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_In_Closet_Number
			valueA='Walk_In_Closet_Number='
			valueE=`echo ${LINE[361]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shoes_Walk_in_Closet
			valueA='Shoes_Walk_in_Closet='
			valueE=`echo ${LINE[362]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_Through_Closet
			valueA='Walk_Through_Closet='
			valueE=`echo ${LINE[363]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Under_Floor_Storage
			valueA='Under_Floor_Storage='
			valueE=`echo ${LINE[364]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Attic_Storage
			valueA='Attic_Storage='
			valueE=`echo ${LINE[365]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Under_Stairs_Storage
			valueA='Under_Stairs_Storage='
			valueE=`echo ${LINE[366]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Entrance_Storage
			valueA='Entrance_Storage='
			valueE=`echo ${LINE[367]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Lift_Wall_Storage
			valueA='Lift_Wall_Storage='
			valueE=`echo ${LINE[368]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#All_Living_Room_Storage
			valueA='All_Living_Room_Storage='
			valueE=`echo ${LINE[369]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Storeroom
			valueA='Storeroom='
			valueE=`echo ${LINE[370]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Trunk_Room
			valueA='Trunk_Room='
			valueE=`echo ${LINE[371]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Warehouse
			valueA='Warehouse='
			valueE=`echo ${LINE[372]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Armoire
			valueA='Armoire='
			valueE=`echo ${LINE[373]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shoes_Box
			valueA='Shoes_Box='
			valueE=`echo ${LINE[374]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Ceiling_High_Clogs_Box
			valueA='Ceiling_High_Clogs_Box='
			valueE=`echo ${LINE[375]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Toilet
			valueA='Bus_Toilet='
			valueE=`echo ${LINE[376]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shower
			valueA='Shower='
			valueE=`echo ${LINE[377]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Reheating
			valueA='Reheating='
			valueE=`echo ${LINE[378]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bathroom_Dryer
			valueA='Bathroom_Dryer='
			valueE=`echo ${LINE[379]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Auto_Bus
			valueA='Auto_Bus='
			valueE=`echo ${LINE[380]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Dressing_Room
			valueA='Dressing_Room='
			valueE=`echo ${LINE[381]}R`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Hot_Water_Supply
			valueA='Hot_Water_Supply='
			valueE=`echo ${LINE[382]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Three_Point_Hot_Water_Supply
			valueA='Three_Point_Hot_Water_Supply='
			valueE=`echo ${LINE[383]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Tv_With_Bus
			valueA='Tv_With_Bus='
			valueE=`echo ${LINE[384]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#With_Audio_Bus
			valueA='With_Audio_Bus='
			valueE=`echo ${LINE[385]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Sauna
			valueA='Sauna='
			valueE=`echo ${LINE[386]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Whirlpool
			valueA='Whirlpool='
			valueE=`echo ${LINE[387]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Mist_Sauna
			valueA='Mist_Sauna='
			valueE=`echo ${LINE[388]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#One_Tsubo
			valueA='One_Tsubo='
			valueE=`echo ${LINE[389]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Two_Places
			valueA='Two_Places='
			valueE=`echo ${LINE[390]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Cleaning_Toilet_Seat
			valueA='Cleaning_Toilet_Seat='
			valueE=`echo ${LINE[391]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Toilet_Seat
			valueA='Toilet_Seat='
			valueE=`echo ${LINE[392]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Tankless
			valueA='Tankless='
			valueE=`echo ${LINE[393]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Two_Places_Toilet
			valueA='Two_Places_Toilet='
			valueE=`echo ${LINE[394]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Three_Places_Toilet
			valueA='Three_Places_Toilet='
			valueE=`echo ${LINE[395]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#The_Door_To_The_Toilet
			valueA='The_Door_To_The_Toilet='
			valueE=`echo ${LINE[396]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#The_Window_To_The_Washroom
			valueA='The_Window_To_The_Washroom='
			valueE=`echo ${LINE[397]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Wash_Basin
			valueA='Wash_Basin='
			valueE=`echo ${LINE[398]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Seperate
			valueA='Seperate='
			valueE=`echo ${LINE[399]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shampoo_Dresser
			valueA='Shampoo_Dresser='
			valueE=`echo ${LINE[400]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Tree_Sided_mirror_With_Vanity
			valueA='Tree_Sided_mirror_With_Vanity='
			valueE=`echo ${LINE[401]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Two_bowl_Basin
			valueA='Two_bowl_Basin='
			valueE=`echo ${LINE[402]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Washing_Machine
			valueA='Washing_Machine='
			valueE=`echo ${LINE[403]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Washing_Machine_Storage
			valueA='Washing_Machine_Storage='
			valueE=`echo ${LINE[404]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Stove_Installation_Possible
			valueA='Stove_Installation_Possible='
			valueE=`echo ${LINE[405]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Stove_Installed
			valueA='Stove_Installed='
			if [ -n "${LINE[406]}" ]
			then
				valueE=1
			else
				valueE=0
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Stove_Installation_Number
			valueA='Stove_Installation_Number='
			valueE=`echo ${LINE[406]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Stove_Type
			valueA='Stove_Type='
			valueE=`echo ${LINE[407]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Glass_Top_Stove
			valueA='Glass_Top_Stove='
			valueE=`echo ${LINE[408]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Grill
			valueA='Grill='
			valueE=`echo ${LINE[409]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Gas_Oven_Range
			valueA='Gas_Oven_Range='
			valueE=`echo ${LINE[410]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Gas_Oven
			valueA='Gas_Oven='
			valueE=`echo ${LINE[411]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Two_Places_Kitchen
			valueA='Two_Places_Kitchen='
			valueE=`echo ${LINE[412]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Back_Door
			valueA='Back_Door='
			valueE=`echo ${LINE[413]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Water_Filter
			valueA='Water_Filter='
			valueE=`echo ${LINE[414]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Dishwashers_Dryer
			valueA='Dishwashers_Dryer='
			valueE=`echo ${LINE[415]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Pantry
			valueA='Pantry='
			valueE=`echo ${LINE[416]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Cupboard
			valueA='Cupboard='
			valueE=`echo ${LINE[417]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Microwave
			valueA='Microwave='
			valueE=`echo ${LINE[418]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Fridge
			valueA='Fridge='
			valueE=`echo ${LINE[419]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#System_Kitchen
			valueA='System_Kitchen='
			valueE=`echo ${LINE[420]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Kitchen_Counter
			valueA='Kitchen_Counter='
			valueE=`echo ${LINE[421]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Kitchen_Face_To_Face
			valueA='Kitchen_Face_To_Face='
			valueE=`echo ${LINE[422]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#L_shaped_Kitchen
			valueA='L_shaped_Kitchen='
			valueE=`echo ${LINE[423]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Island_Kitchen
			valueA='Island_Kitchen='
			valueE=`echo ${LINE[424]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Only_Cooling
			valueA='Only_Cooling='
			valueE=`echo ${LINE[425]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Heating_Only
			valueA='Heating_Only='
			valueE=`echo ${LINE[426]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Heating_Type
			valueA='Heating_Type='
			valueE=`echo ${LINE[427]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Air_Conditioning
			valueA='Air_Conditioning='
			valueE=`echo ${LINE[428]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Air_Conditioning_Number
			valueA='Air_Conditioning_Number='
			valueE=`echo ${LINE[429]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Floor_Heating
			valueA='Floor_Heating='
			valueE=`echo ${LINE[430]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Ventilation_System
			valueA='Ventilation_System='
			valueE=`echo ${LINE[431]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Central_Air_Conditioning
			valueA='Central_Air_Conditioning='
			valueE=`echo ${LINE[432]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Passive_Ventilation
			valueA='Passive_Ventilation='
			valueE=`echo ${LINE[433]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Built_In_Air_Conditioning
			valueA='Built_In_Air_Conditioning='
			valueE=`echo ${LINE[434]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Hot_Water_Heater_Room
			valueA='Hot_Water_Heater_Room='
			valueE=`echo ${LINE[435]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Please_Stand_Digging
			valueA='Please_Stand_Digging='
			valueE=`echo ${LINE[436]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Underfloor_Ventilation
			valueA='Underfloor_Ventilation='
			valueE=`echo ${LINE[437]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Heating
			valueA='Heating='
			valueE=`echo ${LINE[438]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Ceiling_Fan
			valueA='Ceiling_Fan='
			valueE=`echo ${LINE[439]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Bs
			valueA='Line_Bs='
			valueE=`echo ${LINE[440]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Cs
			valueA='Line_Cs='
			valueE=`echo ${LINE[441]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Catv
			valueA='Line_Catv='
			valueE=`echo ${LINE[442]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Wired
			valueA='Line_Wired='
			valueE=`echo ${LINE[443]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Uhf
			valueA='Line_Uhf='
			valueE=`echo ${LINE[444]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Internet_Optical_Fiber
			valueA='Internet_Optical_Fiber='
			valueE=`echo ${LINE[445]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Internet_Catv
			valueA='Internet_Catv='
			valueE=`echo ${LINE[446]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Internet_Lan
			valueA='Internet_Lan='
			valueE=`echo ${LINE[447]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Internet_Isdn
			valueA='Internet_Isdn='
			valueE=`echo ${LINE[448]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Internet_Free
			valueA='Internet_Free='
			valueE=`echo ${LINE[449]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}

			valueB=" WHERE Housing_Id = "
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_housing.sql

		##############################################################
		# T_ChatchCopy
		##############################################################
			value="UPDATE T_ChatchCopy SET "

		#Housing_Id
		#Chatch_Copy
			valueA='Chatch_Copy='
			valueE=`echo ${LINE[450]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Remarks
			valueA='Remarks='
			valueE=`echo ${LINE[451]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Memo
			valueA='Memo='
			valueE=`echo ${LINE[452]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}

			valueB=" WHERE Housing_Id = "
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_housing.sql

		##############################################################
		# 画像ファイルのコピー先フォルダの作成および変数の初期化
		##############################################################

			# 画像ファイルのコピー先フォルダ
			valueH=/var/www/html/
#			valueH=/mnt/s3/chintai/
			valueI=images/
			valueI=${valueI}${Housing_Id}"/"
			valueL=${valueH}${valueI}*

			if [ "${Housing_Id}" != "" ]
			then
				if [ -e "${valueH}${valueI}" ]
				then
					# 画像ファイルのコピー先フォルダにあるファイルを全て削除
					for filepath in ${valueL}
					do
						if [ -f ${filepath} ]
						then
							rm ${filepath}
						fi
					done
#				else
#					mkdir -m 755 ${valueH}${valueI}
				fi
			fi
			# 画像ファイルのコピー元フォルダ
			valueJ=`echo "./showwork/showwork/${LINE[3]}/" | sed -e 's/"//g'`

		##############################################################
		# T_NearbyAttractions
		##############################################################
			value="UPDATE T_NearbyAttractions SET "

		#Housing_Id
		#Neighborhood_Category_Id_1
			valueA='Neighborhood_Category_Id_1='
			valueE=`echo ${LINE[99]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Name_1
			valueA='Neighborhood_Info_Name_1='
			if [ -n "${LINE[99]}" ]
			then
				valueE=`echo ${LINE[101]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Distance_1
			valueA='Neighborhood_Info_Distance_1='
			if [ -n "${LINE[99]}" ]
			then
				valueE=`echo ${LINE[100]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_File_1
			valueA='Neighborhood_Info_File_1='
			if [ "${LINE[102]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[102]=`echo "${LINE[102]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[102]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[102]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[102]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[102]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[102]} ${valueH}${valueI}${valueK}
						LINE[102]=${valueK}
					fi
					valueE=${valueI}${LINE[102]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Category_Id_2
			valueA='Neighborhood_Category_Id_2='
			valueE=`echo ${LINE[103]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Name_2
			valueA='Neighborhood_Info_Name_2='
			if [ -n "${LINE[103]}" ]
			then
				valueE=`echo ${LINE[105]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Distance_2
			valueA='Neighborhood_Info_Distance_2='
			if [ -n "${LINE[103]}" ]
			then
				valueE=`echo ${LINE[104]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_File_2
			valueA='Neighborhood_Info_File_2='
			if [ "${LINE[106]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[106]=`echo "${LINE[106]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[106]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[106]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[106]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[106]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[106]} ${valueH}${valueI}${valueK}
						LINE[106]=${valueK}
					fi
					valueE=${valueI}${LINE[106]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Category_Id_3
			valueA='Neighborhood_Category_Id_3='
			valueE=`echo ${LINE[107]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Name_3
			valueA='Neighborhood_Info_Name_3='
			if [ -n "${LINE[107]}" ]
			then
			valueE=`echo ${LINE[109]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Distance_3
			valueA='Neighborhood_Info_Distance_3='
			if [ -n "${LINE[107]}" ]
			then
				valueE=`echo ${108]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_File_3
			valueA='Neighborhood_Info_File_3='
			if [ "${LINE[110]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[110]=`echo "${LINE[110]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[110]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[110]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[110]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[110]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[110]} ${valueH}${valueI}${valueK}
						LINE[110]=${valueK}
					fi
					valueE=${valueI}${LINE[110]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Category_Id_4
			valueA='Neighborhood_Category_Id_4='
			valueE=`echo ${LINE[111]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Name_4
			valueA='Neighborhood_Info_Name_4='
			if [ -n "${LINE[111]}" ]
			then
				valueE=`echo ${LINE[113]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Distance_4
			valueA='Neighborhood_Info_Distance_4='
			if [ -n "${LINE[111]}" ]
			then
				valueE=`echo ${LINE[112]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_File_4
			valueA='Neighborhood_Info_File_4='
			if [ "${LINE[114]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[114]=`echo "${LINE[114]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[114]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[114]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[114]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[114]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[114]} ${valueH}${valueI}${valueK}
						LINE[114]=${valueK}
					fi
					valueE=${valueI}${LINE[114]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Category_Id_5
			valueA='Neighborhood_Category_Id_5='
			valueE=`echo ${LINE[115]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Name_5
			valueA='Neighborhood_Info_Name_5='
			if [ -n "${LINE[115]}" ]
			then
				valueE=`echo ${LINE[117]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Distance_5
			valueA='Neighborhood_Info_Distance_5='
			if [ -n "${LINE[115]}" ]
			then
				valueE=`echo ${116]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_File_5
			valueA='Neighborhood_Info_File_5='
			if [ "${LINE[118]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[118]=`echo "${LINE[118]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[118]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[118]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[118]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[118]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[118]} ${valueH}${valueI}${valueK}
						LINE[118]=${valueK}
					fi
					valueE=${valueI}${LINE[118]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Category_Id_6
			valueA='Neighborhood_Category_Id_6='
			valueE=`echo ${LINE[119]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Name_6
			valueA='Neighborhood_Info_Name_6='
			if [ -n "${LINE[119]}" ]
			then
				valueE=`echo ${LINE[121]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Distance_6
			valueA='Neighborhood_Info_Distance_6='
			if [ -n "${LINE[119]}" ]
			then
				valueE=`echo ${LINE[120]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_File_6
			valueA='Neighborhood_Info_File_6='
			if [ "${LINE[122]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[122]=`echo "${LINE[122]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[122]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[122]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[122]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[122]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[122]} ${valueH}${valueI}${valueK}
						LINE[122]=${valueK}
					fi
					valueE=${valueI}${LINE[122]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Category_Id_7
			valueA='Neighborhood_Category_Id_7='
			valueE=`echo ${LINE[123]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Name_7
			valueA='Neighborhood_Info_Name_7='
			if [ -n "${LINE[123]}" ]
			then
				valueE=`echo ${LINE[125]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Distance_7
			valueA='Neighborhood_Info_Distance_7='
			if [ -n "${LINE[123]}" ]
			then
				valueE=`echo ${LINE[124]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_File_7
			valueA='Neighborhood_Info_File_7='
			if [ "${LINE[126]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[126]=`echo "${LINE[126]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[126]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[126]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[126]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[126]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[126]} ${valueH}${valueI}${valueK}
						LINE[126]=${valueK}
					fi
					valueE=${valueI}${LINE[126]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Category_Id_8
			valueA='Neighborhood_Category_Id_8='
			valueE=`echo ${LINE[127]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Name_8
			valueA='Neighborhood_Info_Name_8='
			if [ -n "${LINE[127]}" ]
			then
				valueE=`echo ${LINE[129]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_Distance_8
			valueA='Neighborhood_Info_Distance_8='
			if [ -n "${LINE[127]}" ]
			then
				valueE=`echo ${LINE[128]}`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Neighborhood_Info_File_8
			valueA='Neighborhood_Info_File_8='
			if [ "${LINE[130]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[130]=`echo "${LINE[130]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[130]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[130]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[130]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[130]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[130]} ${valueH}${valueI}${valueK}
						LINE[130]=${valueK}
					fi
					valueE=${valueI}${LINE[130]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}
		#Neighborhood_Category_Id_9
		#Neighborhood_Info_Name_9
		#Neighborhood_Info_Distance_9
		#Neighborhood_Info_File_9
		#Neighborhood_Category_Id_10
		#Neighborhood_Info_Name_10
		#Neighborhood_Info_Distance_10
		#Neighborhood_Info_File_10

			valueB=" WHERE Housing_Id = "
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_housing.sql

		##############################################################
		# T_Image
		##############################################################
			value="UPDATE T_Image SET "

		#Housing_Id
		#Image_Category_Id_1
			valueA='Image_Category_Id_1='
			valueE=`echo ${LINE[453]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_1
			valueA='Image_Name_1='
			valueE=`echo ${LINE[454]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_1
			valueA='Image_File_1='
			if [ "${LINE[453]}" == "" -o "{$LINE[455]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[455]=`echo "${LINE[455]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[455]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[455]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[455]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[455]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
echo cp ${valueJ}${LINE[455]} ${valueH}${valueI}${valueK}
						cp ${valueJ}${LINE[455]} ${valueH}${valueI}${valueK}
						LINE[455]=${valueK}
					fi
					valueE=${valueI}${LINE[455]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_2
			valueA='Image_Category_Id_2='
			valueE=`echo ${LINE[456]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_2
			valueA='Image_Name_2='
			valueE=`echo ${LINE[457]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_2
			valueA='Image_File_2='
			if [ "${LINE[456]}" == "" -o "${LINE[458]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[458]=`echo "${LINE[458]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[458]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[458]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[458]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[458]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[458]} ${valueH}${valueI}${valueK}
						LINE[458]=${valueK}
					fi
					valueE=${valueI}${LINE[458]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_3
			valueA='Image_Category_Id_3='
			valueE=`echo ${LINE[459]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_3
			valueA='Image_Name_3='
			valueE=`echo ${LINE[460]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_3
			valueA='Image_File_3='
			if [ "${LINE[459]}" == "" -o "${LINE[461]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[461]=`echo "${LINE[461]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[461]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[461]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[461]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[461]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[461]} ${valueH}${valueI}${valueK}
						LINE[461]=${valueK}
					fi
					valueE=${valueI}${LINE[461]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_4
			valueA='Image_Category_Id_4='
			valueE=`echo ${LINE[462]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_4
			valueA='Image_Name_4='
			valueE=`echo ${LINE[463]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_4
			valueA='Image_File_4='
			if [ "${LINE[462]}" == "" -o "${LINE[464]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[464]=`echo "${LINE[464]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[464]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[464]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[464]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[464]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[464]} ${valueH}${valueI}${valueK}
						LINE[464]=${valueK}
					fi
					valueE=${valueI}${LINE[464]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_5
			valueA='Image_Category_Id_5='
			valueE=`echo ${LINE[465]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_5
			valueA='Image_Name_5='
			valueE=`echo ${LINE[466]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_5
			valueA='Image_File_5='
			if [ "${LINE[465]}" == "" -o "${LINE[467]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[467]=`echo "${LINE[467]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[467]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[467]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[467]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[467]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[467]} ${valueH}${valueI}${valueK}
						LINE[467]=${valueK}
					fi
					valueE=${valueI}${LINE[467]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_6
			valueA='Image_Category_Id_6='
			valueE=`echo ${LINE[468]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_6
			valueA='Image_Name_6='
			valueE=`echo ${LINE[469]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_6
			valueA='Image_File_6='
			if [ "${LINE[468]}" == "" -o "${LINE[470]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[470]=`echo "${LINE[470]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[470]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[470]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[470]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[470]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[470]} ${valueH}${valueI}${valueK}
						LINE[470]=${valueK}
					fi
					valueE=${valueI}${LINE[470]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_7
			valueA='Image_Category_Id_7='
			valueE=`echo ${LINE[471]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_7
			valueA='Image_Name_7='
			valueE=`echo ${LINE[472]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_7
			valueA='Image_File_7='
			if [ "${LINE[471]}" == "" -o "${LINE[473]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[473]=`echo "${LINE[473]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[473]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[473]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[473]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[473]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[473]} ${valueH}${valueI}${valueK}
						LINE[473]=${valueK}
					fi
					valueE=${valueI}${LINE[473]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_8
			valueA='Image_Category_Id_8='
			valueE=`echo ${LINE[474]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_8
			valueA='Image_Name_8='
			valueE=`echo ${LINE[475]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_8
			valueA='Image_File_8='
			if [ "${LINE[474]}" == "" -o "${LINE[476]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[476]=`echo "${LINE[476]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[476]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[476]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[476]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[476]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[476]} ${valueH}${valueI}${valueK}
						LINE[476]=${valueK}
					fi
					valueE=${valueI}${LINE[476]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_9
			valueA='Image_Category_Id_9='
			valueE=`echo ${LINE[477]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_9
			valueA='Image_Name_9='
			valueE=`echo ${LINE[478]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_9
			valueA='Image_File_9='
			if [ "${LINE[477]}" == "" -o "${LINE[479]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[479]=`echo "${LINE[479]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[479]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[479]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[479]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[479]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[479]} ${valueH}${valueI}${valueK}
						LINE[479]=${valueK}
					fi
					valueE=${valueI}${LINE[479]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_10
			valueA='Image_Category_Id_10='
			valueE=`echo ${LINE[480]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_10
			valueA='Image_Name_10='
			valueE=`echo ${LINE[481]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_10
			valueA='Image_File_10='
			if [ "${LINE[480]}" == "" -o "${LINE[482]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[482]=`echo "${LINE[482]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[482]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[482]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[482]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[482]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[482]} ${valueH}${valueI}${valueK}
						LINE[482]=${valueK}
					fi
					valueE=${valueI}${LINE[482]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_11
			valueA='Image_Category_Id_11='
			valueE=`echo ${LINE[483]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_11
			valueA='Image_Name_11='
			valueE=`echo ${LINE[484]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_11
			valueA='Image_File_11='
			if [ "${LINE[483]}" == "" -o "${LINE[485]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[485]=`echo "${LINE[485]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[485]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[485]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[485]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[485]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[485]} ${valueH}${valueI}${valueK}
						LINE[485]=${valueK}
					fi
					valueE=${valueI}${LINE[485]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_12
			valueA='Image_Category_Id_12='
			valueE=`echo ${LINE[486]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_12
			valueA='Image_Name_12='
			valueE=`echo ${LINE[487]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_12
			valueA='Image_File_12='
			if [ "${LINE[486]}" == "" -o "${LINE[488]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[488]=`echo "${LINE[488]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[488]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[488]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[488]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[488]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[488]} ${valueH}${valueI}${valueK}
						LINE[488]=${valueK}
					fi
					valueE=${valueI}${LINE[488]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_13
			valueA='Image_Category_Id_13='
			valueE=`echo ${LINE[489]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_13
			valueA='Image_Name_13='
			valueE=`echo ${LINE[490]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_13
			valueA='Image_File_13='
			if [ "${LINE[489]}" == "" -o "${LINE[491]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[491]=`echo "${LINE[491]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[491]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[491]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[491]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[491]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[491]} ${valueH}${valueI}${valueK}
						LINE[491]=${valueK}
					fi
					valueE=${valueI}${LINE[491]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_14
			valueA='Image_Category_Id_14='
			valueE=`echo ${LINE[492]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_14
			valueA='Image_Name_14='
			valueE=`echo ${LINE[493]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_14
			valueA='Image_File_14='
			if [ "${LINE[492]}" == "" -o "${LINE[494]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[494]=`echo "${LINE[494]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[494]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[494]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[494]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[494]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[494]} ${valueH}${valueI}${valueK}
						LINE[494]=${valueK}
					fi
					valueE=${valueI}${LINE[494]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_15
			valueA='Image_Category_Id_15='
			valueE=`echo ${LINE[495]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_15
			valueA='Image_Name_15='
			valueE=`echo ${LINE[496]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_15
			valueA='Image_File_15='
			if [ "${LINE[495]}" == "" -o "${LINE[497]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[497]=`echo "${LINE[497]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[497]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[497]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[497]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[497]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[497]} ${valueH}${valueI}${valueK}
						LINE[497]=${valueK}
					fi
					valueE=${valueI}${LINE[497]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_16
			valueA='Image_Category_Id_16='
			valueE=`echo ${LINE[498]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_16
			valueA='Image_Name_16='
			valueE=`echo ${LINE[499]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_16
			valueA='Image_File_16='
			if [ "${LINE[498]}" == "" -o "${LINE[500]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[500]=`echo "${LINE[500]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[500]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[500]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[500]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[500]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[500]} ${valueH}${valueI}${valueK}
						LINE[500]=${valueK}
					fi
					valueE=${valueI}${LINE[500]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_17
			valueA='Image_Category_Id_17='
			valueE=`echo ${LINE[501]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_17
			valueA='Image_Name_17='
			valueE=`echo ${LINE[502]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_17
			valueA='Image_File_17='
			if [ "${LINE[501]}" == "" -o "${LINE[503]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[503]=`echo "${LINE[503]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[503]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[503]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[503]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[503]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[503]} ${valueH}${valueI}${valueK}
						LINE[503]=${valueK}
					fi
					valueE=${valueI}${LINE[503]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_18
			valueA='Image_Category_Id_18='
			valueE=`echo ${LINE[504]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_18
			valueA='Image_Name_18='
			valueE=`echo ${LINE[505]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_18
			valueA='Image_File_18='
			if [ "${LINE[504]}" == "" -o "${LINE[506]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[506]=`echo "${LINE[506]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[506]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[506]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[506]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[506]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[506]} ${valueH}${valueI}${valueK}
						LINE[506]=${valueK}
					fi
					valueE=${valueI}${LINE[506]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_19
			valueA='Image_Category_Id_19='
			valueE=`echo ${LINE[507]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_19
			valueA='Image_Name_19='
			valueE=`echo ${LINE[508]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_19
			valueA='Image_File_19='
			if [ "${LINE[507]}" == "" -o "${LINE[509]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[509]=`echo "${LINE[509]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[509]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[509]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[509]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[509]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[509]} ${valueH}${valueI}${valueK}
						LINE[509]=${valueK}
					fi
					valueE=${valueI}${LINE[509]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_20
			valueA='Image_Category_Id_20='
			valueE=`echo ${LINE[510]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_20
			valueA='Image_Name_20='
			valueE=`echo ${LINE[511]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_20
			valueA='Image_File_20='
			if [ "${LINE[510]}" == "" -o "${LINE[512]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[512]=`echo "${LINE[512]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[512]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[512]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[512]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[512]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[512]} ${valueH}${valueI}${valueK}
						LINE[512]=${valueK}
					fi
					valueE=${valueI}${LINE[512]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_21
			valueA='Image_Category_Id_21='
			valueE=`echo ${LINE[513]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_21
			valueA='Image_Name_21='
			valueE=`echo ${LINE[514]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_21
			valueA='Image_File_21='
			if [ "${LINE[513]}" == "" -o "${LINE[515]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[515]=`echo "${LINE[515]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[515]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[515]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[515]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[515]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[515]} ${valueH}${valueI}${valueK}
						LINE[515]=${valueK}
					fi
					valueE=${valueI}${LINE[515]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_22
			valueA='Image_Category_Id_22='
			valueE=`echo ${LINE[516]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_22
			valueA='Image_Name_22='
			valueE=`echo ${LINE[517]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_22
			valueA='Image_File_22='
			if [ "${LINE[516]}" == "" -o "${LINE[518]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[518]=`echo "${LINE[518]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[518]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[518]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[518]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[518]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[518]} ${valueH}${valueI}${valueK}
						LINE[518]=${valueK}
					fi
					valueE=${valueI}${LINE[518]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_23
			valueA='Image_Category_Id_23='
			valueE=`echo ${LINE[519]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_23
			valueA='Image_Name_23='
			valueE=`echo ${LINE[520]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_23
			valueA='Image_File_23='
			if [ "${LINE[519]}" == "" -o "${LINE[521]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[521]=`echo "${LINE[521]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[521]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[521]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[521]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[521]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[521]} ${valueH}${valueI}${valueK}
						LINE[521]=${valueK}
					fi
					valueE=${valueI}${LINE[521]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_24
			valueA='Image_Category_Id_24='
			valueE=`echo ${LINE[522]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_24
			valueA='Image_Name_24='
			valueE=`echo ${LINE[523]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_24
			valueA='Image_File_24='
			if [ "${LINE[522]}" == "" -o "${LINE[524]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[524]=`echo "${LINE[524]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[524]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[524]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[524]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[524]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[524]} ${valueH}${valueI}${valueK}
						LINE[524]=${valueK}
					fi
					valueE=${valueI}${LINE[524]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_25
			valueA='Image_Category_Id_25='
			valueE=`echo ${LINE[525]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_25
			valueA='Image_Name_25='
			valueE=`echo ${LINE[526]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_25
			valueA='Image_File_25='
			if [ "${LINE[525]}" == "" -o "${LINE[527]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[527]=`echo "${LINE[527]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[527]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[527]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[527]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[527]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[527]} ${valueH}${valueI}${valueK}
						LINE[527]=${valueK}
					fi
					valueE=${valueI}${LINE[527]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_26
			valueA='Image_Category_Id_26='
			valueE=`echo ${LINE[528]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_26
			valueA='Image_Name_26='
			valueE=`echo ${LINE[529]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_26
			valueA='Image_File_26='
			if [ "${LINE[528]}" == "" -o "${LINE[530]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[530]=`echo "${LINE[530]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[530]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[530]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[530]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[530]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[530]} ${valueH}${valueI}${valueK}
						LINE[530]=${valueK}
					fi
					valueE=${valueI}${LINE[530]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_27
			valueA='Image_Category_Id_27='
			valueE=`echo ${LINE[531]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_27
			valueA='Image_Name_27='
			valueE=`echo ${LINE[532]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_27
			valueA='Image_File_27='
			if [ "${LINE[531]}" == "" -o "${LINE[533]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[533]=`echo "${LINE[533]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[533]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[533]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[533]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[533]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[533]} ${valueH}${valueI}${valueK}
						LINE[533]=${valueK}
					fi
					valueE=${valueI}${LINE[533]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_28
			valueA='Image_Category_Id_28='
			valueE=`echo ${LINE[534]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_28
			valueA='Image_Name_28='
			valueE=`echo ${LINE[535]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_28
			valueA='Image_File_28='
			if [ "${LINE[534]}" == "" -o "${LINE[536]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[536]=`echo "${LINE[536]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[536]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[536]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[536]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[536]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[536]} ${valueH}${valueI}${valueK}
						LINE[536]=${valueK}
					fi
					valueE=${valueI}${LINE[536]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_29
			valueA='Image_Category_Id_29='
			valueE=`echo ${LINE[537]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_29
			valueA='Image_Name_29='
			valueE=`echo ${LINE[538]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_29
			valueA='Image_File_29='
			if [ "${LINE[537]}" == "" -o "${LINE[539]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[539]=`echo "${LINE[539]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[539]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[539]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[539]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[539]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[539]} ${valueH}${valueI}${valueK}
						LINE[539]=${valueK}
					fi
					valueE=${valueI}${LINE[539]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Category_Id_30
			valueA='Image_Category_Id_30='
			valueE=`echo ${LINE[540]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Name_30
			valueA='Image_Name_30='
			valueE=`echo ${LINE[541]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_File_30
			valueA='Image_File_30='
			if [ "${LINE[540]}" == "" -o "${LINE[542]}" == "" ]
			then
				valueE=""
			else
				# 画像ファイル名は小文字に変換する
				LINE[542]=`echo "${LINE[542]}" | sed -e 's/"//g'`
				valueK=`echo "${LINE[542]}" | tr '[A-Z]' '[a-z]'`
				if [ `echo "${LINE[542]}" | cut -c 1-4` == "http" ]
				then
					valueE=${LINE[542]}
				else
					# コピー元の画像ファイルが実際に存在する場合にコピーを実行
					if [ -e "${valueJ}${LINE[542]}" ]
					then
						# /var/www/html/images/${Housing_Id}/ に画像ファイルをコピーする
						cp ${valueJ}${LINE[542]} ${valueH}${valueI}${valueK}
						LINE[542]=${valueK}
					fi
					valueE=${valueI}${LINE[542]}
				fi
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}

			valueB=" WHERE Housing_Id = "
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_housing.sql

		##############################################################
		# T_SearchHousing
		##############################################################
			value="UPDATE T_SearchHousing SET "

		#ADD_Pref
			valueA='ADD_Pref='
			valueE=`echo ${LINE[13]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#ADD_Shiku
			valueA='ADD_Shiku='
			valueE=`echo ${LINE[14]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#ADD_Choson
			valueA='ADD_Choson='
			valueE=`echo ${LINE[15]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#ADD_Aza
			valueA='ADD_Aza='
			valueE=`echo ${LINE[16]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#ADD_Branch
			valueA='ADD_Branch='
			valueE=`echo ${LINE[17]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Branch_FLG
			valueA='Branch_FLG='
			valueE=`echo ${LINE[18]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Rent
			valueA='Rent='
			valueE=`echo ${LINE[145]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Service_Fee
			valueA='Common_Service_Fee='
			valueE=`echo ${LINE[146]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Service_Fee_Classification
			valueA='Common_Service_Fee_Classification='
			valueE="0"
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Deposits
			valueA='Security_Deposits='
			valueE=`echo ${LINE[147]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Amount
			valueA='Amount='
			valueE=`echo ${LINE[148]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Deposits_Classification
			valueA='Security_Deposits_Classification='
			valueE=`echo ${LINE[149]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Key_Money
			valueA='Key_Money='
			valueE=`echo ${LINE[157]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Key_Money_Classification
			valueA='Key_Money_Classification='
			valueE=`echo ${LINE[158]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Floor_Number
			valueA='Floor_Number='
			valueE=`echo ${LINE[213]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Floor_Plan
			valueA='Floor_Number='
			valueE=`echo ${LINE[214]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Updated
			valueA='Updated='
			valueE=`echo ${LINE[189]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Built_Years
			valueA='Built_Years='
			valueX=`echo ${LINE[53]} | sed -e 's/"//g'`
			valueY=`echo ${LINE[54]} | sed -e 's/"//g'`
			valueZ=`echo ${LINE[55]} | sed -e 's/"//g'`
			if [ "${valueY}" -gt 0 -a "${valueY}" -lt 10 ]
			then
			  	  valueY=`echo 0${valueY}`
			else
			 	   valueY=`echo ${valueY}`
			fi
			if [ -n "${valueZ}" ]
			then
				if [ "${valueZ}" -gt 0 -a "${valueZ}" -lt 32 ]
				then
					valueZ=`echo ${valueZ}`
				else
					valueZ="00"
				fi
			else
			 	   valueZ="00"
			fi
			valueE=`echo ${valueX}${valueY}${valueZ} | sed -e 's/"//g'`
#			valueE=${valueX}${valueY}${valueZ}
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Built_New_FLG
			valueA='Built_New_FLG='
			valueE=`echo ${LINE[56]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Footprint_Square_Meters
			valueA='Footprint_Square_Meters='
			valueE=`echo ${LINE[235]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Floor
			valueA='Floor='
			valueE=`echo ${LINE[133]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Ground_Floor
			valueA='Ground_Floor='
			valueE=`echo ${LINE[58]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Chatch_Copy
			valueA='Chatch_Copy='
			valueE=`echo ${LINE[450]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Structure
			valueA='Structure='
			valueE=`echo ${LINE[60]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Property_Type
			valueA='Property_Type='
			valueE=`echo ${LINE[7]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Flooring
			valueA='Flooring='
			valueE=`echo ${LINE[313]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Loft
			valueA='Loft='
			valueE=`echo ${LINE[319]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bay_Window
			valueA='Bay_Window='
			valueE=`echo ${LINE[314]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Veranda_Balcony
			valueA='Veranda_Balcony='
			valueE=`echo ${LINE[342]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Private_Garden
			valueA='Private_Garden='
			valueE=`echo ${LINE[354]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Top_Floor
			valueA='Top_Floor='
			valueE=`echo ${LINE[305]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Corner_Room
			valueA='Corner_Room='
			valueE=`echo ${LINE[304]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Facing_South
			valueA='Facing_South='
			if [ "${LINE[304]}" == "1" ]
			then
				valueE=`echo ${304}`
			else
				valueE=0
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Air_Conditioning
			valueA='Air_Conditioning='
			valueE=`echo ${LINE[428]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Floor_Heating
			valueA='Floor_Heating='
			valueE=`echo ${LINE[430]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Heating_Type
			valueA='Heating_Type='
			valueE=`echo ${LINE[427]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Stove_Type
			valueA='Stove_Type='
			valueE=`echo ${LINE[407]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Stove_Installation_Number
			valueA='Stove_Installation_Number='
			valueE=`echo ${LINE[406]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#System_Kitchen
			valueA='System_Kitchen='
			valueE=`echo ${LINE[420]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Kitchen_Counter
			valueA='Kitchen_Counter='
			valueE=`echo ${LINE[421]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Toilet
			valueA='Bus_Toilet='
			valueE=`echo ${LINE[376]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shower
			valueA='Shower='
			valueE=`echo ${LINE[377]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Reheating
			valueA='Reheating='
			valueE=`echo ${LINE[378]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bathroom_Dryer
			valueA='Bathroom_Dryer='
			valueE=`echo ${LINE[379]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Cleaning_Toilet_Seat
			valueA='Cleaning_Toilet_Seat='
			valueE=`echo ${LINE[391]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shoes_Box
			valueA='Shoes_Box='
			valueE=`echo ${LINE[374]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_In_Closet
			valueA='Walk_In_Closet='
			valueE=`echo ${LINE[360]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Under_Floor_Storage
			valueA='Under_Floor_Storage='
			valueE=`echo ${LINE[364]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Trunk_Room
			valueA='Trunk_Room='
			valueE=`echo ${LINE[371]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Intercom
			valueA='Intercom='
			valueE=`echo ${LINE[263]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Auto_Lock
			valueA='Security_Auto_Lock='
			valueE=`echo ${LINE[275]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Dimple_Key
			valueA='Security_Dimple_Key='
			valueE=`echo ${LINE[272]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Key_Card
			valueA='Security_Key_Card='
			valueE=`echo ${LINE[268]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Security_Camera
			valueA='Security_Camera='
			valueE=`echo ${LINE[264]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Delivery_Box
			valueA='Common_Use_Delivery_Box='
			valueE=`echo ${LINE[279]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Management_From
			valueA='Management_From='
			valueE=`echo ${LINE[260]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Move_In_Date
			valueA='Move_In_Date='
			valueE=`echo ${LINE[136]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Sex
			valueA='Occupancy_Sex='
			valueE=`echo ${LINE[258]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Single_Person
			valueA='Occupancy_Single_Person='
			valueE=`echo ${LINE[253]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Two_Person
			valueA='Occupancy_Two_Person='
			valueE=`echo ${LINE[248]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Family
			valueA='Occupancy_Family='
			valueE=`echo ${LINE[249]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Room_Share
			valueA='Occupancy_Room_Share='
			valueE=`echo ${LINE[257]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Pet
			valueA='Occupancy_Pet='
			valueE=`echo ${LINE[256]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Office
			valueA='Occupancy_Office='
			valueE=`echo ${LINE[247]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Alien
			valueA='Occupancy_Alien='
			valueE=`echo ${LINE[259]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Instrument
			valueA='Occupancy_Instrument='
			valueE=`echo ${LINE[246]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Occupancy_Elderly
			valueA='Occupancy_Elderly='
			valueE=`echo ${LINE[246]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Guarantor
			valueA='Guarantor='
			valueE=`echo ${LINE[193]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Broadband
			valueA='Broadband='
			if [ "${LINE[445]}" == "1" -o "${LINE[446]}" == "1" -o "${LINE[447]}" == "1" -o "${LINE[448]}" == "1" ]
			then
				valueE=1
			else
				valueE=0
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Cs
			valueA='Line_Cs='
			valueE=`echo ${LINE[441]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Bs
			valueA='Line_Bs='
			valueE=`echo ${LINE[440]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Catv
			valueA='Line_Catv='
			valueE=`echo ${LINE[442]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Line_Wired
			valueA='Line_Wired='
			valueE=`echo ${LINE[443]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Elevator
			valueA='Elevator='
			valueE=`echo ${LINE[92]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Washing_Machine_Storage
			valueA='Washing_Machine_Storage='
			valueE=`echo ${LINE[404]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shampoo_Dresser
			valueA='Shampoo_Dresser='
			valueE=`echo ${LINE[400]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Water_Filter
			valueA='Water_Filter='
			valueE=`echo ${LINE[414]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#All_Electric
			valueA='All_Electric='
			valueE=`echo ${LINE[240]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Fully_Furnished
			valueA='Fully_Furnished='
			valueE=`echo ${LINE[338]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Gas
			valueA='Gas='
			valueE=`echo ${LINE[239]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Parking
			valueA='Parking='
			valueE=`echo ${LINE[6]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Site_Stop_Number
			valueA='Site_Stop_Number='
			valueE=`echo ${LINE[63]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Parking_Bicycles
			valueA='Common_Use_Parking_Bicycles='
			valueE=`echo ${LINE[282]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Common_Use_Motorcycle_parking
			valueA='Common_Use_Motorcycle_parking='
			valueE=`echo ${LINE[283]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Ventilation_System
			valueA='Ventilation_System='
			valueE=`echo ${LINE[431]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Reform
			valueA='Reform='
			if [ -n "${LINE[141]}" -a -n "${LINE[142]}" ]
			then
				if [ "${LINE[141]}" == "1" -o "${LINE[142]}" == "1" ]
				then
					valueE=1
				else
					valueE=0
				fi
			else
				valueE=0
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Free_Rent
			valueA='Free_Rent='
			valueE=`echo ${LINE[192]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Internet_Free
			valueA='Internet_Free='
			valueE=`echo ${LINE[449]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_Number
		#Image_G_URL
			valueA='Image_G_URL='
			if [ -n "${LINE[453]}" -a -n "${LINE[455]}" ]
			then
				if [ `echo ${LINE[455]} | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE[455]}
				else
					valueE=${valueI}${LINE[455]}
				fi
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Image_M_URL
			valueA='Image_M_URL='
			if [ -n "${LINE[456]}" -a -n "${LINE[458]}" ]
			then
				if [ `echo "${LINE[458]}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE[458]}
				else
					valueE=${valueI}${LINE[458]}
				fi
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Contract_Type
			valueA='Contract_Type='
			valueE=`echo ${LINE[201]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Entrance_Handrail
			valueA='Entrance_Handrail='
			valueE=`echo ${LINE[327]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Slope
		#Barrier_Free
			valueA='Barrier_Free='
			valueE=`echo ${LINE[311]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shop_Id
			valueA='Shop_Id='
#			valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Id FROM T_ShopUser WHERE Connected_2 ='${LINE[3]}' limit 1;"`
			valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Id FROM T_ShopUser WHERE Connected_2 ='${LINE[3]}' limit 1;"`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Shop_Rank
			valueA='Shop_Rank='
			valueE="0"
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#View_FLG
			valueA='View_FLG='
			valueE=`echo ${LINE[10]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Delete_FLG
			valueA='Delete_FLG='
			valueE=`echo ${LINE[11]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}

			valueB=" WHERE Housing_Id = "
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_searchhousing.sql

		##############################################################
		# T_SearchTraffic
		##############################################################
			value="UPDATE T_SearchTraffic SET "

		#Route_1
			valueA='Route_1='
			valueH=`echo ${LINE[21]}`
			valueI=`echo ${LINE[22]}`
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ "${valueZ}" != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Station_1
			valueA='Station_1='
			valueE=""
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_Time_1
			valueA='Walk_Time_1='
			valueE=`echo ${LINE[23]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Line_1
			valueA='Bus_Line_1='
			valueE=`echo ${LINE[24]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_1
			valueA='Bus_Stop_1='
			valueE=`echo ${LINE[25]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_Walk_1
			valueA='Bus_Stop_Walk_1='
			valueE=`echo ${LINE[26]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Ride_Time_1
			valueA='Bus_Ride_Time_1='
			valueE=`echo ${LINE[27]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Transport_1
			valueA='Other_Transport_1='
			valueE=`echo ${LINE[28]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Route_2
			valueA='Route_2='
			valueH=`echo ${LINE[29]}`
			valueI=`echo ${LINE[30]}`
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ ${valueZ} != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Station_2
			valueA='Station_2='
			valueE=""
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_Time_2
			valueA='Walk_Time_2='
			valueE=`echo ${LINE[31]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Line_2
			valueA='Bus_Line_2='
			valueE=`echo ${LINE[32]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_2
			valueA='Bus_Stop_2='
			valueE=`echo ${LINE[33]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_Walk_2
			valueA='Bus_Stop_Walk_2='
			valueE=`echo ${LINE[34]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Ride_Time_2
			valueA='Bus_Ride_Time_2='
			valueE=`echo ${LINE[35]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Transport_2
			valueA='Other_Transport_2='
			valueE=`echo ${LINE[36]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Route_3
			valueA='Route_3='
			valueH=`echo ${LINE[37]}`
			valueI=`echo ${LINE[38]}`
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ "${valueZ}" != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "SELECT Kin_Cd FROM M_Train WHERE Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Station_3
			valueA='Station_3='
			valueE=""
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Walk_Time_3
			valueA='Walk_Time_3='
			valueE=`echo ${LINE[39]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Line_3
			valueA='Bus_Line_3='
			valueE=`echo ${LINE[40]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_3
			valueA='Bus_Stop_3='
			valueE=`echo ${LINE[41]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Stop_Walk_3
			valueA='Bus_Stop_Walk_3='
			valueE=`echo ${LINE[42]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Bus_Ride_Time_3
			valueA='Bus_Ride_Time_3='
			valueE=`echo ${LINE[43]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}${valueC}
		#Other_Transport_3
			valueA='Other_Transport_3='
			valueE=`echo ${LINE[44]}`
			value=${value}${valueA}${valueD}${valueE}${valueD}

			valueB=" WHERE Housing_Id = "
			value=${value}${valueB}${valueD}${Housing_Id}${valueD}
			valueB=";"
			value=${value}${valueB}

			echo ${value} >> update_searchhousing.sql

			#ワンループ測定END
#			echo END=$(date +%c) $(printf '%03d' $(expr `date +%N` / 1000000)) >> one_loop_def.txt
		fi
#		echo END=$(date +%c) $(printf '%03d' $(expr `date +%N` / 1000000)) >> one_loop_def.txt
	fi
done

if [ -e ./update_housing.sql ]
then
#	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai < update_housing.sql
###	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < update_housing.sql
	:
fi

if [ -e ./update_searchhousing.sql ]
then
#	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_search < update_searchhousing.sql
###	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_search < update_searchhousing.sql
	:
fi

#nohup /bin/sh insert_housing.sh &

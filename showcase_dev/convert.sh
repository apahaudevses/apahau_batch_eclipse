#!/bin/sh

#rm showcase_list_insert.sql

value="DELETE FROM T_Showcase;"
echo $value > showcase_list_insert.sql

numLine=0
cat showcase_bukken.csv | cut -f 4 | while read line
do
	if ((${numLine}==1))
	then
		valueA="insert into T_Showcase(ID) value("
	else
		valueA=',('
	fi
	valueB=${line}

	if ((${numLine} < 100))
	then
		valueC=")"
		numLine=`expr ${numLine} + 1`
	else
		valueC=');'
		numLine=1
	fi

	value=${valueA}${valueB}${valueC}

	if [ "${line}" != "" ]
	then
		echo $value >> showcase_list_insert.sql
	fi
done
echo ";" >> showcase_list_insert.sql

value="DELETE FROM T_ShowcaseShops;"
echo $value > showcase_shop_insert.sql

numLine=0
cat showcase_shop.csv | while read line
do

		# ----------------------------
		# 配列にセット
		# ----------------------------
		#","区切り文字列をタブ区切りに変換
		value_tab=`echo ${line} `
		
		#配列にセット
		IFS="$(echo -e '\t' )"
		set -- ${line}


	if [ "${numLine}" = 1 ]
	then
		valueA="insert into T_ShowcaseShops(ID) value("
	else
		valueA=',('
	fi
	valueB=`echo ${2}`

	if ((${numLine} < 100))
	then
		valueC=")"
		numLine=`expr ${numLine} + 1`
	else
		valueC=');'
		numLine=1
	fi

	value=${valueA}${valueB}${valueC}

	if [ `echo "${1}" | sed -e 's/"//g'` = "3" ]
	then
		echo $value >> showcase_shop_insert.sql
	fi
done
echo ";" >> showcase_shop_insert.sql


#mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp < showcase_list_insert.sql
#mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp < showcase_shop_insert.sql

mysql -h localhost -uapadb -pdak5t3a3ahfi5 -N apahau_temp < showcase_list_insert.sql
mysql -h localhost -uapadb -pdak5t3a3ahfi5 -N apahau_temp < showcase_shop_insert.sql

#nohup /bin/sh shop_update.sh &

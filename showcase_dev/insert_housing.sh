#!/bin/sh

if [ -e ./inser_id_list.txt ]
then
	rm -f ./inser_id_list.txt
fi

if [ -e ./insert_housing.sql ]
then
	rm -f ./insert_housing.sql
fi

if [ -e ./insert_traffic.sql ]
then
	rm -f ./insert_traffic.sql
fi

if [ -e ./insert_nearbyattractions.sql ]
then
	rm -f ./insert_nearbyattractions.sql
fi

if [ -e ./insert_image.sql ]
then
	rm -f ./insert_image.sql
fi

if [ -e ./insert_roominfo.sql ]
then
	rm -f ./insert_roominfo.sql
fi

if [ -e ./insert_roominfo.sql ]
then
	rm -f ./insert_roominfo.sql
fi

if [ -e ./insert_equipment.sql ]
then
	rm -f ./insert_equipment.sql
fi

if [ -e ./insert_chatchcopy.sql ]
then
	rm -f ./insert_chatchcopy.sql
fi

if [ -e ./insert_article.sql ]
then
	rm -f ./insert_article.sql
fi

if [ -e ./insert_searchhousing.sql ]
then
	rm -f ./insert_searchhousing.sql
fi

if [ -e ./insert_searchtraffic.sql ]
then
	rm -f ./insert_searchtraffic.sql
fi

CSV_TARGET=./showcase_bukken.csv
numLine=1
errorFLG="FALSE"

#valueID=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "SELECT ID FROM T_Showcase LEFT JOIN apahau_chintai.T_Housing ON T_Showcase.ID = T_Housing.Property_Management_Id WHERE T_Housing.Property_Management_Id is null GROUP BY ID;"`
valueID=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_temp -e "SELECT ID FROM T_Showcase LEFT JOIN apahau_chintai.T_Housing ON T_Showcase.ID = T_Housing.Property_Management_Id WHERE T_Housing.Property_Management_Id is null GROUP BY ID;"`
#mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_temp -e "SELECT ID FROM T_Showcase LEFT JOIN apahau_chintai.T_Housing ON T_Showcase.ID = T_Housing.Property_Management_Id WHERE T_Housing.Property_Management_Id is null GROUP BY ID;">inser_id_list.txt
#mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_temp -e "SELECT ID FROM T_Showcase LEFT JOIN apahau_chintai.T_Housing ON T_Showcase.ID = T_Housing.Property_Management_Id WHERE T_Housing.Property_Management_Id is null GROUP BY ID;">inser_id_list.txt

valueC=','
valueD='"'
count='1'
echo $valueID | sed -e "s/ /\n/g" | while read bukken_id
#cat inser_id_list.txt | while read bukken_id
do
	valueData=`echo $CSV_TARGET | xargs grep -E \"${bukken_id}\" `

	if [ -n "${valueData}" ]
	then


		# ----------------------------
		# 配列にセット
		# ----------------------------
		#","区切り文字列をタブ区切りに変換
		value_tab=`echo ${valueData} `
		
		#配列にセット
		IFS="$(echo -e '\t' )"
		set -- ${valueData}

		# ----------------------------
		# ショップID
		# ----------------------------
		
		valueF=`echo ${3} | sed -e 's/"//g'`
		valueF="'"${valueF}"'"

#		shopID=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select ID from T_ShopUser where Connected_2 = ${valueF} limit 1;"`
		shopID=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select ID from T_ShopUser where Connected_2 = ${valueF} limit 1;"`

		if [ "${errorFLG}" = "FALSE" ]
		then

##T_Housing
			value="insert into T_Housing(Property_Management_Id,Property_Type,Property_Name,Property_Name_Phonetic,Property_1st_Zip_Code,Property_2nd_Zip_Code,Prefectural_Id,Property_Shiku,Property_Choson,Property_Aza,Property_Branch,Branch_FLG,Ido,Keido,Built_Years,Built_New_FLG,Number_Of_Houses,Ground_Floor,Underground_Floor,Structure,Parking,Parking_Distance,Site_Stop_Number,Off_Site_Stop_Number,Parking_Type,Parking_Type_1,Parking_Type_2,Parking_Type_3,Parking_Type_4,Parking_Type_5,Parking_Type_6,Parking_Type_7,Parking_Type_8,Parking_Type_9,Parking_Type_10,Parking_Type_11,Parking_Type_12,Parking_Type_13,Parking_Type_14,Parking_Type_15,Parking_Type_16,Parking_Type_17,Parking_Type_18,Parking_Type_19,Parking_Type_20,External_Insulation_Method,Fire_Resistant_Construction,Insulation,Earthquake_Construction,Wind_resistant_Construction,Appearance_Tiled,Elevator,Elevator_Number,Quiet_Residential_Area,Starting_Station,Flat_Terrain,Lush,Two_Family_Dwelling,Insert_Date,Updata_Date,Show_End_Date,User_Id,Display_FLG,Delete_FLG,Converter_id,Converter_User_Id,Converter_Insert_Date,Converter_Update_Date) value ("
		
		#Property_Management_Id
			valueE=`echo ${4}`
			value=${value}${valueE}${valueC}
		#Property_Type
			valueE=`echo ${7}`
			value=${value}${valueE}${valueC}
		#Property_Name
			valueE=`echo ${8}`
			value=${value}${valueE}${valueC}
		#Property_Name_Phonetic
			valueE=`echo ${9}`
			value=${value}${valueE}${valueC}
		#Property_1st_Zip_Code
			valueE=`echo ${12} | sed -e 's/"//g' | cut -c1-3`
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Property_2nd_Zip_Code
			valueE=`echo ${12} | sed -e 's/"//g' | cut -c4-7`
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Prefectural_Id
			valueE=`echo ${13}`
			value=${value}${valueE}${valueC}
		#Property_Shiku
			valueE=`echo ${14}`
			value=${value}${valueE}${valueC}
		#Property_Choson
			valueE=`echo ${15}`
			value=${value}${valueE}${valueC}
		#Property_Aza
			valueE=`echo ${16}`
			value=${value}${valueE}${valueC}
		#Property_Branch
			valueE=`echo ${17}`
			value=${value}${valueE}${valueC}
		#Branch_FLG
			valueE=`echo ${18}`
			value=${value}${valueE}${valueC}
		#Ido
			valueE=`echo ${19}`
			value=${value}${valueE}${valueC}
		#Keido
			valueE=`echo ${20}`
			value=${value}${valueE}${valueC}
		#Built_Years
			valueX=`echo ${53} | sed -e 's/"//g'`
			valueY=`echo ${54} | sed -e 's/"//g'`
			valueZ=`echo ${55} | sed -e 's/"//g' | sed -e 's/\s//g'`
			if [ "${valueY}" -gt 0 -a "${valueY}" -lt 10 ]
			then
			  	  valueY=`echo 0${valueY}`
			else
			 	   valueY=`echo ${valueY}`
			fi
			if [ -n "${valueZ}" ]
			then
				if [ "${valueZ}" -gt 0 -a "${valueZ}" -lt 32 ]
				then
					valueZ=`echo ${valueZ}`
				else
					valueZ="00"
				fi
			else
			 	   valueZ="00"
			fi
			valueE=`echo ${valueX}${valueY}${valueZ} | sed -e 's/"//g'`
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Built_New_FLG
			valueE=`echo ${56}`
			value=${value}${valueE}${valueC}
		#Number_Of_Houses
			valueE=`echo ${57}`
			value=${value}${valueE}${valueC}
		#Ground_Floor
			valueE=`echo ${58}`
			value=${value}${valueE}${valueC}
		#Underground_Floor
			valueE=`echo ${59}`
			value=${value}${valueE}${valueC}
		#Structure
			valueE=`echo ${60}`
			value=${value}${valueE}${valueC}
		#Parking
			valueE=`echo ${61}`
			value=${value}${valueE}${valueC}
		#Parking_Distance
			valueE=`echo ${62}`
			value=${value}${valueE}${valueC}
		#Site_Stop_Number
			valueE=`echo ${63}`
			value=${value}${valueE}${valueC}
		#Off_Site_Stop_Number
			valueE=`echo ${64}`
			value=${value}${valueE}${valueC}
		#Parking_Type
			valueE=`echo ${65}`
			value=${value}${valueE}${valueC}
		#Parking_Type_1
			valueE=`echo ${66}`
			value=${value}${valueE}${valueC}
		#Parking_Type_2
			valueE=`echo ${67}`
			value=${value}${valueE}${valueC}
		#Parking_Type_3
			valueE=`echo ${68}`
			value=${value}${valueE}${valueC}
		#Parking_Type_4
			valueE=`echo ${69}`
			value=${value}${valueE}${valueC}
		#Parking_Type_5
			valueE=`echo ${70}`
			value=${value}${valueE}${valueC}
		#Parking_Type_6
			valueE=`echo ${71}`
			value=${value}${valueE}${valueC}
		#Parking_Type_7
			valueE=`echo ${72}`
			value=${value}${valueE}${valueC}
		#Parking_Type_8
			valueE=`echo ${73}`
			value=${value}${valueE}${valueC}
		#Parking_Type_9
			valueE=`echo ${74}`
			value=${value}${valueE}${valueC}
		#Parking_Type_10
			valueE=`echo ${75}`
			value=${value}${valueE}${valueC}
		#Parking_Type_11
			valueE=`echo ${76}`
			value=${value}${valueE}${valueC}
		#Parking_Type_12
			valueE=`echo ${77}`
			value=${value}${valueE}${valueC}
		#Parking_Type_13
			valueE=`echo ${78}`
			value=${value}${valueE}${valueC}
		#Parking_Type_14
			valueE=`echo ${79}`
			value=${value}${valueE}${valueC}
		#Parking_Type_15
			valueE=`echo ${80}`
			value=${value}${valueE}${valueC}
		#Parking_Type_16
			valueE=`echo ${81}`
			value=${value}${valueE}${valueC}
		#Parking_Type_17
			valueE=`echo ${82}`
			value=${value}${valueE}${valueC}
		#Parking_Type_18
			valueE=`echo ${83}`
			value=${value}${valueE}${valueC}
		#Parking_Type_19
			valueE=`echo ${84}`
			value=${value}${valueE}${valueC}
		#Parking_Type_20
			valueE=`echo ${85}`
			value=${value}${valueE}${valueC}
		#External_Insulation_Method
			valueE=`echo ${86}`
			value=${value}${valueE}${valueC}
		#Fire_Resistant_Construction
			valueE=`echo ${87}`
			value=${value}${valueE}${valueC}
		#Insulation
			valueE=`echo ${88}`
			value=${value}${valueE}${valueC}
		#Earthquake_Construction
			valueE=`echo ${89}`
			value=${value}${valueE}${valueC}
		#Wind_resistant_Construction
			valueE=`echo ${90}`
			value=${value}${valueE}${valueC}
		#Appearance_Tiled
			valueE=`echo ${91}`
			value=${value}${valueE}${valueC}
		#Elevator
			valueE=`echo ${92}`
			value=${value}${valueE}${valueC}
		#Elevator_Number
			valueE=`echo ${93}`
			value=${value}${valueE}${valueC}
		#Quiet_Residential_Area
			valueE=`echo ${94}`
			value=${value}${valueE}${valueC}
		#Starting_Station
			valueE=`echo ${95}`
			value=${value}${valueE}${valueC}
		#Flat_Terrain
			valueE=`echo ${96}`
			value=${value}${valueE}${valueC}
		#Lush
			valueE=`echo ${97}`
			value=${value}${valueE}${valueC}
		#Two_Family_Dwelling
			valueE=`echo ${98}`
			value=${value}${valueE}${valueC}
		#Insert_Date
			valueE=`date +"%Y-%m-%d %H:%M:%S" | sed -e 's/"//g'`
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Updata_Date
			valueE=`date +"%Y-%m-%d %H:%M:%S" | sed -e 's/"//g'`
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Show_End_Date
			valueE=`echo ${6}`
			value=${value}${valueE}${valueC}
		#User_Id
#			valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Id from T_ShopUser where Connected_2 = ${valueF} limit 1;"`
			valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Id from T_ShopUser where Connected_2 = ${valueF} limit 1;"`
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Display_FLG
			if [ "${10}" = "1" ]
			then
				valueE="1"
			else
				valueE="0"
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Delete_FLG
			if [ "${11}" = "1" ]
			then
				valueE="1"
			else
				valueE="0"
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Converter_id
			valueE="ShowcaseTv"
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Converter_User_Id
			valueE=`echo ${3}`
			value=${value}${valueE}${valueC}
		#Converter_Insert_Date
			valueE=""
			value=${value}${valueD}${valueE}${valueD}${valueC}
		#Converter_Update_Date
			valueE=""
			value=${value}${valueD}${valueE}${valueD}
		
			valueB=');'
			value=${value}${valueB}
			
			
			valueB='select last_insert_id();'
			value=${value}${valueB}
			echo ${value} >> insert_housing.sql
		
#			housing_Id=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "${value}"`
			housing_Id=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "${value}"`


	##T_Traffic
			if [ "${numLine}" = 1 ]
			then
				value="insert into T_Traffic (Housing_Id,Route_1,Station_1,Walk_Time_1,Bus_Line_1,Bus_Stop_1,Bus_Stop_Walk_1,Bus_Ride_Time_1,Other_Transport_1,Route_2,Station_2,Walk_Time_2,Bus_Line_2,Bus_Stop_2,Bus_Stop_Walk_2,Bus_Ride_Time_2,Other_Transport_2,Route_3,Station_3,Walk_Time_3,Bus_Line_3,Bus_Stop_3,Bus_Stop_Walk_3,Bus_Ride_Time_3,Other_Transport_3) value ("
			else
				value=',('
			fi
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	#Route_1
			valueH=`echo ${21}`
			valueI=`echo ${22}`
	
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ ${valueZ} != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Station_1
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Walk_Time_1
			valueE=`echo ${23}`
			value=${value}${valueE}${valueC}
	#Bus_Line_1
			valueE=`echo ${24}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_1
			valueE=`echo ${25}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_Walk_1
			valueE=`echo ${26}`
			value=${value}${valueE}${valueC}
	#Bus_Ride_Time_1
			valueE=`echo ${27}`
			value=${value}${valueE}${valueC}
	#Other_Transport_1
			valueE=`echo ${28}`
			value=${value}${valueE}${valueC}
	#Route_2
			valueH=`echo ${29}`
			valueI=`echo ${30}`
	
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ ${valueZ} != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Station_2
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Walk_Time_2
			valueE=`echo ${31}`
			value=${value}${valueE}${valueC}
	#Bus_Line_2
			valueE=`echo ${32}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_2
			valueE=`echo ${33}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_Walk_2
			valueE=`echo ${34}`
			value=${value}${valueE}${valueC}
	#Bus_Ride_Time_2
			valueE=`echo ${35}`
			value=${value}${valueE}${valueC}
	#Other_Transport_2
			valueE=`echo ${36}`
			value=${value}${valueE}${valueC}
	
	#Route_3
			valueH=`echo ${37}`
			valueI=`echo ${38}`
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ ${valueZ} != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Station_3
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Walk_Time_3
			valueE=`echo ${39}`
			value=${value}${valueE}${valueC}
	#Bus_Line_3
			valueE=`echo ${40}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_3
			valueE=`echo ${41}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_Walk_3
			valueE=`echo ${42}`
			value=${value}${valueE}${valueC}
	#Bus_Ride_Time_3
			valueE=`echo ${43}`
			value=${value}${valueE}${valueC}
	#Other_Transport_3
			valueE=`echo ${44}`
			value=${value}${valueE}
	
			if [ "${numLine}" -lt 1000 ]
			then
				valueB=')'
			else
				valueB=');'
			fi
			value=${value}${valueB}
			echo ${value} >> insert_traffic.sql


			###################################################################
			# 画像ファイルのコピー先フォルダを作成
			###################################################################
			#
			# batch01の場合、最初はローカルのフォルダに格納する。
			# データベース処理後に本来の格納位置に移動する。
			valueH="/export/apahau_batch/work/"
			valueK="/mnt/s3/chintai/"
			valueL="images/"

			# DEV環境の場合、ローカルのフォルダに格納
#			valueH="/var/www/html/"
			valueI="images/"
			valueI=${valueI}${housing_Id}"/"
	
			if [ ! -e "${valueH}${valueI}" ]
			then
				mkdir -m 755 ${valueH}${valueI}
			fi
			valueJ=`echo "./showwork/showwork/${3}/" | sed -e 's/"//g'`

			valueM=${valueH}
			#
			###################################################################


	##T_NearbyAttractions
			if [ "${numLine}" = 1 ]
			then
				value="insert into T_NearbyAttractions(Housing_Id,Neighborhood_Category_Id_1,Neighborhood_Info_Name_1,Neighborhood_Info_Distance_1,Neighborhood_Info_File_1,Neighborhood_Category_Id_2,Neighborhood_Info_Name_2,Neighborhood_Info_Distance_2,Neighborhood_Info_File_2,Neighborhood_Category_Id_3,Neighborhood_Info_Name_3,Neighborhood_Info_Distance_3,Neighborhood_Info_File_3,Neighborhood_Category_Id_4,Neighborhood_Info_Name_4,Neighborhood_Info_Distance_4,Neighborhood_Info_File_4,Neighborhood_Category_Id_5,Neighborhood_Info_Name_5,Neighborhood_Info_Distance_5,Neighborhood_Info_File_5,Neighborhood_Category_Id_6,Neighborhood_Info_Name_6,Neighborhood_Info_Distance_6,Neighborhood_Info_File_6,Neighborhood_Category_Id_7,Neighborhood_Info_Name_7,Neighborhood_Info_Distance_7,Neighborhood_Info_File_7,Neighborhood_Category_Id_8,Neighborhood_Info_Name_8,Neighborhood_Info_Distance_8,Neighborhood_Info_File_8) value ("
			else
				value=',('
			fi
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	#Neighborhood_Category_Id_1
			valueE=`echo ${99}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Name_1
			valueE=`echo ${101}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Distance_1
			valueE=`echo ${100}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_File_1
			valueE=""
			valueZ=`echo ${102} | sed -e 's/"//g'`
			if [ "${valueZ}" != "" ]
			then
				if [ "`echo ${valueZ} | cut -c 1-4 `" = "http" ]
				then
					valueE=${valueZ}
				else
					valueE=${valueI}${valueZ}
					if [ -e "${valueH}${valueE}" ]
					then
						cp ${valueJ}${valueZ} ${valueH}${valueI}
					fi
				fi
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Neighborhood_Category_Id_2
			valueE=`echo ${103}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Name_2
			valueE=`echo ${105}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Distance_2
			valueE=`echo ${104}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_File_2
			valueE=""
			valueZ=`echo ${106} | sed -e 's/"//g'`
			if [ "${valueZ}" != "" ]
			then
				if [ `echo "${valueZ}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${valueZ}
				else
					valueE=${valueI}${valueZ}
					if [ -e "${valueH}${valueE}" ]
					then
						cp ${valueJ}${valueZ} ${valueH}${valueI}
					fi
				fi
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Neighborhood_Category_Id_3
			valueE=`echo ${107}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Name_3
			valueE=`echo ${109}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Distance_3
			valueE=`echo ${108}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_File_3
			valueE=""
			valueZ=`echo ${110} | sed -e 's/"//g'`
			if [ "${valueZ}" != "" ]
			then
				if [ `echo "${valueZ}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${valueZ}
				else
					valueE=${valueI}${valueZ}
					if [ -e "${valueH}${valueE}" ]
					then
						cp ${valueJ}${valueZ} ${valueH}${valueI}
					fi
				fi
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Neighborhood_Category_Id_4
			valueE=`echo ${111}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Name_4
			valueE=`echo ${113}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Distance_4
			valueE=`echo ${112}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_File_4
			valueE=""
			valueZ=`echo ${114} | sed -e 's/"//g'`
			if [ "${valueZ}" != "" ]
			then
				if [ `echo "${valueZ}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${valueZ}
				else
					valueE=${valueI}${valueZ}
					if [ -e "${valueH}${valueE}" ]
					then
						cp ${valueJ}${valueZ} ${valueH}${valueI}
					fi
				fi
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Neighborhood_Category_Id_5
			valueE=`echo ${115}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Name_5
			valueE=`echo ${117}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Distance_5
			valueE=`echo ${116}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_File_5
			valueE=""
			valueZ=`echo ${118} | sed -e 's/"//g'`
			if [ "${valueZ}" != "" ]
			then
				if [ `echo "${valueZ}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${valueZ}
				else
					valueE=${valueI}${valueZ}
					if [ -e "${valueH}${valueE}" ]
					then
						cp ${valueJ}${valueZ} ${valueH}${valueI}
					fi
				fi
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Neighborhood_Category_Id_6
			valueE=`echo ${119}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Name_6
			valueE=`echo ${121}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Distance_6
			valueE=`echo ${120}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_File_6
			valueE=""
			valueZ=`echo ${122} | sed -e 's/"//g'`
			if [ "${valueZ}" != "" ]
			then
				if [ `echo "${valueZ}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${valueZ}
				else
					valueE=${valueI}${valueZ}
					if [ -e "${valueH}${valueE}" ]
					then
						cp ${valueJ}${valueZ} ${valueH}${valueI}
					fi
				fi
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Neighborhood_Category_Id_7
			valueE=`echo ${123}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Name_7
			valueE=`echo ${125}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Distance_7
			valueE=`echo ${124}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_File_7
			valueE=""
			valueZ=`echo ${126} | sed -e 's/"//g'`
			if [ "${valueZ}" != "" ]
			then
				if [ `echo "${valueZ}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${valueZ}
				else
					valueE=${valueI}${valueZ}
					if [ -e "${valueH}${valueE}" ]
					then
						cp ${valueJ}${valueZ} ${valueH}${valueI}
					fi
				fi
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Neighborhood_Category_Id_8
			valueE=`echo ${127}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Name_8
			valueE=`echo ${129}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_Distance_8
			valueE=`echo ${128}`
			value=${value}${valueE}${valueC}
	#Neighborhood_Info_File_8
			valueE=""
			valueZ=`echo ${130} | sed -e 's/"//g'`
			if [ "${valueZ}" != "" ]
			then
				if [ `echo "${valueZ}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${valueZ}
				else
					valueE=${valueI}${valueZ}
					if [ -e "${valueH}${valueE}" ]
					then
						cp ${valueJ}${valueZ} ${valueH}${valueI}
					fi
				fi
			fi
			value=${value}${valueD}${valueE}${valueD}
	
			if [ "${numLine}" -lt 1000 ]
			then
				valueB=')'
			else
				valueB=');'
			fi
			value=${value}${valueB}
			echo ${value} >> insert_nearbyattractions.sql


	##T_Image
		if [ "${numLine}" = 1 ]
			then
				value="insert into T_Image (Housing_Id,Image_Category_Id_1,Image_Name_1,Image_File_1,Image_Category_Id_2,Image_Name_2,Image_File_2,Image_Category_Id_3,Image_Name_3,Image_File_3,Image_Category_Id_4,Image_Name_4,Image_File_4,Image_Category_Id_5,Image_Name_5,Image_File_5,Image_Category_Id_6,Image_Name_6,Image_File_6,Image_Category_Id_7,Image_Name_7,Image_File_7,Image_Category_Id_8,Image_Name_8,Image_File_8,Image_Category_Id_9,Image_Name_9,Image_File_9,Image_Category_Id_10,Image_Name_10,Image_File_10,Image_Category_Id_11,Image_Name_11,Image_File_11,Image_Category_Id_12,Image_Name_12,Image_File_12,Image_Category_Id_13,Image_Name_13,Image_File_13,Image_Category_Id_14,Image_Name_14,Image_File_14,Image_Category_Id_15,Image_Name_15,Image_File_15,Image_Category_Id_16,Image_Name_16,Image_File_16,Image_Category_Id_17,Image_Name_17,Image_File_17,Image_Category_Id_18,Image_Name_18,Image_File_18,Image_Category_Id_19,Image_Name_19,Image_File_19,Image_Category_Id_20,Image_Name_20,Image_File_20,Image_Category_Id_21,Image_Name_21,Image_File_21,Image_Category_Id_22,Image_Name_22,Image_File_22,Image_Category_Id_23,Image_Name_23,Image_File_23,Image_Category_Id_24,Image_Name_24,Image_File_24,Image_Category_Id_25,Image_Name_25,Image_File_25,Image_Category_Id_26,Image_Name_26,Image_File_26,Image_Category_Id_27,Image_Name_27,Image_File_27,Image_Category_Id_28,Image_Name_28,Image_File_28,Image_Category_Id_29,Image_Name_29,Image_File_29,Image_Category_Id_30,Image_Name_30,Image_File_30
	)value("
			else
	
				value=',('
			fi
	
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	
	#Image_Category_Id_1
			valueE=`echo ${453}`
			value=${value}${valueE}${valueC}
	#Image_Name_1
			if [ -n "${453}" ]
			then
				valueE=${454}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_1
			LINE=`echo "${455}" | sed -e 's/"//g'`
			if [ -n "${453}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_2
			valueE=`echo ${456}`
			value=${value}${valueE}${valueC}
	#Image_Name_2
			if [ -n "${456}" ]
			then
				valueE=${457}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_2
				LINE=`echo "${458}" | sed -e 's/"//g'`
			if [ -n "${456}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}${LINE}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_3
			valueE=`echo ${459}`
			value=${value}${valueE}${valueC}
	#Image_Name_3
			if [ -n "${459}" ]
			then
				valueE=${460}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_3
				LINE=`echo "${461}" | sed -e 's/"//g'`
			if [ -n "${459}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}${LINE}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_4
			valueE=`echo ${462}`
			value=${value}${valueE}${valueC}
	#Image_Name_4
			if [ -n "${462}" ]
			then
				valueE=${463}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_4
				LINE=`echo "${464}" | sed -e 's/"//g'`
			if [ -n "${462}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_5
			valueE=`echo ${465}`
			value=${value}${valueE}${valueC}
	#Image_Name_5
			if [ -n "${465}" ]
			then
				valueE=${466}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_5
				LINE=`echo "${467}" | sed -e 's/"//g'`
			if [ -n "${465}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_6
			valueE=`echo ${468}`
			value=${value}${valueE}${valueC}
	#Image_Name_6
			if [ -n "${468}" ]
			then
				valueE=${469}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_6
				LINE=`echo "${470}" | sed -e 's/"//g'`
			if [ -n "${468}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_7
			valueE=`echo ${471}`
			value=${value}${valueE}${valueC}
	#Image_Name_7
			if [ -n "${471}" ]
			then
				valueE=${472}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_7
				LINE=`echo "${473}" | sed -e 's/"//g'`
			if [ -n "${471}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_8
			valueE=`echo ${474}`
			value=${value}${valueE}${valueC}
	#Image_Name_8
			if [ -n "${474}" ]
			then
				valueE=${475}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_8
				LINE=`echo "${476}" | sed -e 's/"//g'`
			if [ -n "${474}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_9
			valueE=`echo ${477}`
			value=${value}${valueE}${valueC}
	#Image_Name_9
			if [ -n "${477}" ]
			then
				valueE=${478}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_9
				LINE=`echo "${479}" | sed -e 's/"//g'`
			if [ -n "${477}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_10
			valueE=`echo ${480}`
			value=${value}${valueE}${valueC}
	#Image_Name_10
			if [ -n "${480}" ]
			then
				valueE=${481}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_10
				LINE=`echo "${482}" | sed -e 's/"//g'`
			if [ -n "${480}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Image_Category_Id_11
			valueE=`echo ${483}`
			value=${value}${valueE}${valueC}
	#Image_Name_11
			if [ -n "${483}" ]
			then
				valueE=${484}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_11
				LINE=`echo "${485}" | sed -e 's/"//g'`
			if [ -n "${483}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_12
			valueE=`echo ${486}`
			value=${value}${valueE}${valueC}
	#Image_Name_12
			if [ -n "${486}" ]
			then
				valueE=${487}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_12
				LINE=`echo "${488}" | sed -e 's/"//g'`
			if [ -n "${486}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_13
			valueE=`echo ${489}`
			value=${value}${valueE}${valueC}
	#Image_Name_13
			if [ -n "${489}" ]
			then
				valueE=${490}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_13
				LINE=`echo "${491}" | sed -e 's/"//g'`
			if [ -n "${489}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_14
			valueE=`echo ${492}`
			value=${value}${valueE}${valueC}
	#Image_Name_14
			if [ -n "${492}" ]
			then
				valueE=${493}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_14
				LINE=`echo "${494}" | sed -e 's/"//g'`
			if [ -n "${492}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_15
			valueE=`echo ${495}`
			value=${value}${valueE}${valueC}
	#Image_Name_15
			if [ -n "${495}" ]
			then
				valueE=${496}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_15
				LINE=`echo "${497}" | sed -e 's/"//g'`
			if [ -n "${495}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_16
			valueE=`echo ${498}`
			value=${value}${valueE}${valueC}
	#Image_Name_16
			if [ -n "${498}" ]
			then
				valueE=${499}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_16
				LINE=`echo "${500}" | sed -e 's/"//g'`
			if [ -n "${498}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_17
			valueE=`echo ${501}`
			value=${value}${valueE}${valueC}
	#Image_Name_17
			if [ -n "${501}" ]
			then
				valueE=${502}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_17
				LINE=`echo "${503}" | sed -e 's/"//g'`
			if [ -n "${501}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_18
			valueE=`echo ${504}`
			value=${value}${valueE}${valueC}
	#Image_Name_18
			if [ -n "${504}" ]
			then
				valueE=${505}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_18
				LINE=`echo "${506}" | sed -e 's/"//g'`
			if [ -n "${504}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_19
			valueE=`echo ${507}`
			value=${value}${valueE}${valueC}
	#Image_Name_19
			if [ -n "${507}" ]
			then
				valueE=${508}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_19
				LINE=`echo "${509}" | sed -e 's/"//g'`
			if [ -n "${507}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_20
			valueE=`echo ${510}`
			value=${value}${valueE}${valueC}
	#Image_Name_20
			if [ -n "${510}" ]
			then
				valueE=${511}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_20
				LINE=`echo "${512}" | sed -e 's/"//g'`
			if [ -n "${510}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Image_Category_Id_21
			valueE=`echo ${513}`
			value=${value}${valueE}${valueC}
	#Image_Name_21
			if [ -n "${513}" ]
			then
				valueE=${514}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_21
				LINE=`echo "${515}" | sed -e 's/"//g'`
			if [ -n "${513}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_22
			valueE=`echo ${516}`
			value=${value}${valueE}${valueC}
	#Image_Name_22
			if [ -n "${516}" ]
			then
				valueE=${517}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_22
				LINE=`echo "${518}" | sed -e 's/"//g'`
			if [ -n "${516}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_23
			valueE=`echo ${519}`
			value=${value}${valueE}${valueC}
	#Image_Name_23
			if [ -n "${519}" ]
			then
				valueE=${520}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_23
				LINE=`echo "${521}" | sed -e 's/"//g'`
			if [ -n "${519}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}

	#Image_Category_Id_24
			valueE=`echo ${522}`
			value=${value}${valueE}${valueC}
	#Image_Name_24
			if [ -n "${522}" ]
			then
				valueE=${523}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_24
				LINE=`echo "${524}" | sed -e 's/"//g'`
			if [ -n "${522}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_25
			valueE=`echo ${525}`
			value=${value}${valueE}${valueC}
	#Image_Name_25
			if [ -n "${525}" ]
			then
				valueE=${526}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_25
				LINE=`echo "${527}" | sed -e 's/"//g'`
			if [ -n "${525}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	
	#Image_Category_Id_26
			valueE=`echo ${528}`
			value=${value}${valueE}${valueC}
	#Image_Name_26
			if [ -n "${528}" ]
			then
				valueE=${529}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_26
				LINE=`echo "${530}" | sed -e 's/"//g'`
			if [ -n "${528}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_27
			valueE=`echo ${531}`
			value=${value}${valueE}${valueC}
	#Image_Name_27
			if [ -n "${531}" ]
			then
				valueE=${532}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_27
				LINE=`echo "${533}" | sed -e 's/"//g'`
			if [ -n "${531}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_28
			valueE=`echo ${534}`
			value=${value}${valueE}${valueC}
	#Image_Name_28
			if [ -n "${534}" ]
			then
				valueE=${535}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_28
				LINE=`echo "${536}" | sed -e 's/"//g'`
			if [ -n "${534}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_29
			valueE=`echo ${537}`
			value=${value}${valueE}${valueC}
	#Image_Name_29
			if [ -n "${537}" ]
			then
				valueE=${538}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_29
				LINE=`echo "${539}" | sed -e 's/"//g'`
			if [ -n "${537}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	
	#Image_Category_Id_30
			valueE=`echo ${540}`
			value=${value}${valueE}${valueC}
	#Image_Name_30
			if [ -n "${540}" ]
			then
				valueE=${541}
			else
				valueE=""
			fi
			value=${value}${valueE}${valueC}
	#Image_File_30
				LINE=`echo "${542}" | sed -e 's/"//g'`
			if [ -n "${540}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
					if [ -e "${valueJ}${LINE}" ]
					then
						cp ${valueJ}${LINE} ${valueH}${valueI}
					fi
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}
	
			if [ "${numLine}" -lt 1000 ]
			then
				valueB=')'
			else
				valueB=');'
			fi
			value=${value}${valueB}
			echo ${value} >> insert_image.sql


	##T_RoomInfo
			if [ "${numLine}" = 1 ]
			then
				value="insert into T_RoomInfo (Housing_Id,Room_Number,Basement,Floor,Opening_Direction,Current_State,Move_In_Date,Move_In_Year,Move_In_Month,Move_In_Time,Reform,Renovation,Rent,Common_Service_Fee,Common_Service_Fee_Classification,Security_Deposits,Amount,Security_Deposits_Classification,Conditions_Increase,Conditions_Pet,Conditions_Tabacco,Conditions_Children,Conditions_Other,Increase,Increase_Classification,Key_Money,Key_Money_Classification,Parking_Fee,Parking_Fee_Tax,Parking_Fee_Deposit,Parking_Fee_Classification,Amortization_Insole_argument,Amortization_Amount,Amortization_Classification,Amortization_Time,Key_Exchange_Fee,Key_Exchange_Tax,Other_Expenses_1,Other_Expenses_Tax_1,Other_Expenses_Nominal_1,Other_Expenses_Time_1,Other_Expenses_2,Other_Expenses_Tax_2,Other_Expenses_Nominal_2,Other_Expenses_Time_2,Other_Expenses_3,Other_Expenses_Tax_3,Other_Expenses_Nominal_3,Other_Expenses_Time_3,Other_Expenses_4,Other_Expenses_Tax_4,Other_Expenses_Nominal_4,Other_Expenses_Time_4,Brokers_Commission,Brokers_Amount,Brokers_Classification,Brokers_Tax,Updated,Update_Amount,Update_Select,Free_Rent,Guarantor,Insurance_Companies_Use,Utilization_Form,Company_Name,Guarantee_Charge,Insurance,Insurance_Number_Of_Years,Premium,Contract_Type,Contract_Select,Contract_Period_Year,Contract_Period_Month,Contract_Deadline_Year,Contract_Deadline_Month,Contract_Deadline_Day,Form_Of_Transaction,Based_Pricing_Service,Service_Person,Service_Phone,Confirmed_Day,Floor_Number,Floor_Plan,Room_Type_1,Room_Size_1,Room_Type_2,Room_Size_2,Room_Type_3,Room_Size_3,Room_Type_4,Room_Size_4,Room_Type_5,Room_Size_5,Room_Type_6,Room_Size_6,Room_Type_7,Room_Size_7,Room_Type_8,Room_Size_8,Room_Type_9,Room_Size_9,Room_Type_10,Room_Size_10,Footprint_Square_Meters,Footprint_Square_Tsubo)value("
			else
				value=',('
			fi
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	#Room_Number
			valueE=`echo ${131}`
			value=${value}${valueE}${valueC}
	#Basement
			valueE=`echo ${132}`
			value=${value}${valueE}${valueC}
	#Floor
			valueE=`echo ${133}`
			value=${value}${valueE}${valueC}
	#Opening_Direction
			valueE=`echo ${134}`
			value=${value}${valueE}${valueC}
	#Current_State
			valueE=`echo ${135}`
			value=${value}${valueE}${valueC}
	#Move_In_Date
			valueE=`echo ${136}`
			value=${value}${valueE}${valueC}
	#Move_In_Year
			valueE=`echo ${137}`
			value=${value}${valueE}${valueC}
	#Move_In_Month
			valueE=`echo ${138}`
			value=${value}${valueE}${valueC}
	#Move_In_Time
			valueE=`echo ${139}`
			value=${value}${valueE}${valueC}
	#Reform
			valueE=`echo ${141}`
			value=${value}${valueE}${valueC}
	#Renovation
			valueE=`echo ${142}`
			value=${value}${valueE}${valueC}
	#Rent
			valueE=`echo ${145}`
			value=${value}${valueE}${valueC}
	#Common_Service_Fee
			valueE=`echo ${146}`
			value=${value}${valueE}${valueC}
	#Common_Service_Fee_Classification
			valueE="0"
			value=${value}${valueE}${valueC}
	#Security_Deposits
			valueE=`echo ${147}`
			value=${value}${valueE}${valueC}
	#Amount
			valueE=`echo ${148}`
			value=${value}${valueE}${valueC}
	#Security_Deposits_Classification
			valueE=`echo ${149}`
			value=${value}${valueE}${valueC}
	#Conditions_Increase
			valueE=`echo ${150}`
			value=${value}${valueE}${valueC}
	#Conditions_Pet
			valueE=`echo ${151}`
			value=${value}${valueE}${valueC}
	#Conditions_Tabacco
			valueE=`echo ${152}`
			value=${value}${valueE}${valueC}
	#Conditions_Children
			valueE=`echo ${153}`
			value=${value}${valueE}${valueC}
	#Conditions_Other
			valueE=`echo ${154}`
			value=${value}${valueE}${valueC}
	#Increase
			valueE=`echo ${155}`
			value=${value}${valueE}${valueC}
	#Increase_Classification
			valueE=`echo ${156}`
			value=${value}${valueE}${valueC}
	#Key_Money
			valueE=`echo ${157}`
			value=${value}${valueE}${valueC}
	#Key_Money_Classification
			valueE=`echo ${158}`
			value=${value}${valueE}${valueC}
	#Parking_Fee
			valueE=`echo ${159}`
			value=${value}${valueE}${valueC}
	#Parking_Fee_Tax
			valueE=`echo ${160}`
			value=${value}${valueE}${valueC}
	#Parking_Fee_Deposit
			valueE=`echo ${161}`
			value=${value}${valueE}${valueC}
	#Parking_Fee_Classification
			valueE=`echo ${162}`
			value=${value}${valueE}${valueC}
	#Amortization_Insole_argument
			valueE=`echo ${163}`
			value=${value}${valueE}${valueC}
	#Amortization_Amount
			valueE=`echo ${164}`
			value=${value}${valueE}${valueC}
	#Amortization_Classification
			valueE=`echo ${165}`
			value=${value}${valueE}${valueC}
	#Amortization_Time
			valueE=`echo ${166}`
			value=${value}${valueE}${valueC}
	#Key_Exchange_Fee
			valueE=`echo ${167}`
			value=${value}${valueE}${valueC}
	#Key_Exchange_Tax
			valueE=`echo ${168}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_1
			valueE=`echo ${169}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Tax_1
			valueE=`echo ${170}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Nominal_1
			valueE=`echo ${171}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Time_1
			valueE=`echo ${172}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_2
			valueE=`echo ${173}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Tax_2
			valueE=`echo ${174}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Nominal_2
			valueE=`echo ${175}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Time_2
			valueE=`echo ${176}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_3
			valueE=`echo ${177}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Tax_3
			valueE=`echo ${178}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Nominal_3
			valueE=`echo ${179}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Time_3
			valueE=`echo ${180}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_4
			valueE=`echo ${181}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Tax_4
			valueE=`echo ${182}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Nominal_4
			valueE=`echo ${183}`
			value=${value}${valueE}${valueC}
	#Other_Expenses_Time_4
			valueE=`echo ${184}`
			value=${value}${valueE}${valueC}
	#Brokers_Commission
			valueE=`echo ${185}`
			value=${value}${valueE}${valueC}
	#Brokers_Amount
			valueE=`echo ${186}`
			value=${value}${valueE}${valueC}
	#Brokers_Classification
			valueE=`echo ${187}`
			value=${value}${valueE}${valueC}
	#Brokers_Tax
			valueE=`echo ${188}`
			value=${value}${valueE}${valueC}
	#Updated
			valueE=`echo ${189}`
			value=${value}${valueE}${valueC}
	#Update_Amount
			valueE=`echo ${190}`
			value=${value}${valueE}${valueC}
	#Update_Select
			valueE=`echo ${191}`
			value=${value}${valueE}${valueC}
	#Free_Rent
			valueE=`echo ${192}`
			value=${value}${valueE}${valueC}
	#Guarantor
			valueE=`echo ${193}`
			value=${value}${valueE}${valueC}
	#Insurance_Companies_Use
			valueE=`echo ${194}`
			value=${value}${valueE}${valueC}
	#Utilization_Form
			valueE=`echo ${195}`
			value=${value}${valueE}${valueC}
	#Company_Name
			valueE=`echo ${196}`
			value=${value}${valueE}${valueC}
	#Guarantee_Charge
			valueE=`echo ${197}`
			value=${value}${valueE}${valueC}
	#Insurance
			valueE=`echo ${198}`
			value=${value}${valueE}${valueC}
	#Insurance_Number_Of_Years
			valueE=`echo ${199}`
			value=${value}${valueE}${valueC}
	#Premium
			valueE=`echo ${200}`
			value=${value}${valueE}${valueC}
	#Contract_Type
			valueE=`echo ${201}`
			value=${value}${valueE}${valueC}
	#Contract_Select
			valueE=`echo ${202}`
			value=${value}${valueE}${valueC}
	#Contract_Period_Year
			valueE=`echo ${203}`
			value=${value}${valueE}${valueC}
	#Contract_Period_Month
			valueE=`echo ${204}`
			value=${value}${valueE}${valueC}
	#Contract_Deadline_Year
			valueE=`echo ${205}`
			value=${value}${valueE}${valueC}
	#Contract_Deadline_Month
			valueE=`echo ${206}`
			value=${value}${valueE}${valueC}
	#Contract_Deadline_Day
			valueE=`echo ${207}`
			value=${value}${valueE}${valueC}
	#Form_Of_Transaction
			valueE=`echo ${208}`
			value=${value}${valueE}${valueC}
	#Based_Pricing_Service
			valueE=`echo ${209}`
			value=${value}${valueE}${valueC}
	#Service_Person
			valueE=`echo ${210}`
			value=${value}${valueE}${valueC}
	#Service_Phone
			valueE=`echo ${211}`
			value=${value}${valueE}${valueC}
	#Confirmed_Day
			valueE=`echo ${212}`
			value=${value}${valueE}${valueC}
	#Floor_Number
			valueE=`echo ${213}`
			value=${value}${valueE}${valueC}
	#Floor_Plan
			valueE=`echo ${214}`
			value=${value}${valueE}${valueC}
	#Room_Type_1
			valueE=`echo ${215}`
			value=${value}${valueE}${valueC}
	#Room_Size_1
			valueE=`echo ${216}`
			value=${value}${valueE}${valueC}
	#Room_Type_2
			valueE=`echo ${217}`
			value=${value}${valueE}${valueC}
	#Room_Size_2
			valueE=`echo ${218}`
			value=${value}${valueE}${valueC}
	#Room_Type_3
			valueE=`echo ${219}`
			value=${value}${valueE}${valueC}
	#Room_Size_3
			valueE=`echo ${220}`
			value=${value}${valueE}${valueC}
	#Room_Type_4
			valueE=`echo ${221}`
			value=${value}${valueE}${valueC}
	#Room_Size_4
			valueE=`echo ${222}`
			value=${value}${valueE}${valueC}
	#Room_Type_5
			valueE=`echo ${223}`
			value=${value}${valueE}${valueC}
	#Room_Size_5
			valueE=`echo ${224}`
			value=${value}${valueE}${valueC}
	#Room_Type_6
			valueE=`echo ${225}`
			value=${value}${valueE}${valueC}
	#Room_Size_6
			valueE=`echo ${226}`
			value=${value}${valueE}${valueC}
	#Room_Type_7
			valueE=`echo ${227}`
			value=${value}${valueE}${valueC}
	#Room_Size_7
			valueE=`echo ${228}`
			value=${value}${valueE}${valueC}
	#Room_Type_8
			valueE=`echo ${229}`
			value=${value}${valueE}${valueC}
	#Room_Size_8
			valueE=`echo ${230}`
			value=${value}${valueE}${valueC}
	#Room_Type_9
			valueE=`echo ${231}`
			value=${value}${valueE}${valueC}
	#Room_Size_9
			valueE=`echo ${232}`
			value=${value}${valueE}${valueC}
	#Room_Type_10
			valueE=`echo ${233}`
			value=${value}${valueE}${valueC}
	#Room_Size_10
			valueE=`echo ${234}`
			value=${value}${valueE}${valueC}
	#Footprint_Square_Meters
			valueE=`echo ${235}`
			value=${value}${valueE}${valueC}
	#Footprint_Square_Tsubo
			if [ -n ${236} ]
			then
				valueE=`echo ${236}`
			else
				valueE=`echo "${236} * 0.3025" | bc`
			fi
			value=${value}${valueE}
	
			if [ "${numLine}" -lt 1000 ]
			then
				valueB=')'
			else
				valueB=');'
			fi
			value=${value}${valueB}
			echo ${value} >> insert_roominfo.sql


	##T_Equipment
			if [ ${numLine} = 1 ]
			then
				value="insert into T_Equipment (Housing_Id,Water_Supply,Sewer,Gas,All_Electric,Eco_Will,Eco_Cute,Eco_Jaws,Solar_Power,Occupancy_Children,Occupancy_Instrument,Occupancy_Office,Occupancy_Two_Person,Occupancy_Family,Occupancy_Sale_In_Lots,Occupancy_Excellent_People_Wage,Occupancy_Student,Occupancy_Single_Person,Occupancy_Elderly,Occupancy_Corporation,Occupancy_Pet,Occupancy_Room_Share,Occupancy_Sex,Occupancy_Alien,Management_From,Emergency_Response,Strengthening_District,Intercom,Security_Camera,Security_Motion_Sensors,Security_Fingerprint_Authentication,Security_Vein_Authentication,Security_Key_Card,Security_Key,Security_Door_Touch_Key,Security_Door_Remote_Control_Key,Security_Dimple_Key,Security_Double_Lock,Security_Electronic_Lock,Security_Auto_Lock,Security_Shutter,Security_Glass,Fire_Alarm_System,Common_Use_Delivery_Box,Common_Use_Cleaning_Box,Common_Use_Garbage_Storage,Common_Use_Parking_Bicycles,Common_Use_Motorcycle_parking,Common_Use_Athletic,Common_Use_Pool,Common_Use_Kids_Room,Common_Use_Launderette,Common_Use_Theater_Room,Common_Use_Pet_Only_Facility,Common_Use_Big_Bath,Common_Use_On_Site_Playground,Common_Use_Set_Post,Common_Use_Sunny,Light_court,Two_surface_lighting,Three_surface_lighting,All_Rooms_lighting,Three_way_House,Four_way_House,Without_Front_Building,All_Rooms_Direction,Second_Floor_Ldk,Second_Floor_Living,Corner_Room,Top_Floor,Onsen,Designers,Good_Ventilation,Good_View,Rooms_Facing_South,Barrier_Free,Distribution,Flooring,Bay_Window,Skylight,Blow_By,Maisonette,Under_Ground_Room,Loft,Breadth_Loft,Free_Space,Sunroom,Soundproof_Room,Cushion_Floor,Entrance_Porch,Entrance_Hall,Entrance_Handrail,Carpet_Upholstery,Bed,Storage_Bed,Lighting_Equipment,Auto_Light,Indirect_Lighting,All_Rooms_With_Lighting,Curtains,Blind_With,Roll_Screen_With,Fully_Furnished,All_Rooms_Flooring,South_Side_Living,Double_Glazing,Veranda_Balcony,Balcony_Breadth,Terrace,Roof_Balcony,Two_wey_Balcony,Two_Side_Balcony,Three_Surface_Balcony,L_Shaped_Balcony,Inner_Balcony,Wood_Deck,Outdoor_Power,Shutter_Shutters,Private_Garden,Courtyard,Spot_Garden,Storage_Space,Closet,Closet_number,Walk_In_Closet,Walk_In_Closet_Number,Shoes_Walk_in_Closet,Walk_Through_Closet,Under_Floor_Storage,Attic_Storage,Under_Stairs_Storage,Entrance_Storage,Lift_Wall_Storage,All_Living_Room_Storage,Storeroom,Trunk_Room,Warehouse,Armoire,Shoes_Box,Ceiling_High_Clogs_Box,Bus_Toilet,Shower,Reheating,Bathroom_Dryer,Auto_Bus,Dressing_Room,Hot_Water_Supply,Three_Point_Hot_Water_Supply,Tv_With_Bus,With_Audio_Bus,Sauna,Whirlpool,Mist_Sauna,One_Tsubo,Two_Places,Cleaning_Toilet_Seat,Toilet_Seat,Tankless,Two_Places_Toilet,Three_Places_Toilet,The_Door_To_The_Toilet,The_Window_To_The_Washroom,Wash_Basin,Seperate,Shampoo_Dresser,Tree_Sided_mirror_With_Vanity,Two_bowl_Basin,Washing_Machine,Washing_Machine_Storage,Stove_Installation_Possible,Stove_Installed,Stove_Installation_Number,Stove_Type,Glass_Top_Stove,Grill,Gas_Oven_Range,Gas_Oven,Two_Places_Kitchen,Back_Door,Water_Filter,Dishwashers_Dryer,Pantry,Cupboard,Microwave,Fridge,System_Kitchen,Kitchen_Counter,Kitchen_Face_To_Face,L_shaped_Kitchen,Island_Kitchen,Only_Cooling,Heating_Only,Heating_Type,Air_Conditioning,Air_Conditioning_Number,Floor_Heating,Ventilation_System,Central_Air_Conditioning,Passive_Ventilation,Built_In_Air_Conditioning,Hot_Water_Heater_Room,Please_Stand_Digging,Underfloor_Ventilation,Heating,Ceiling_Fan,Line_Bs,Line_Cs,Line_Catv,Line_Wired,Line_Uhf,Internet_Optical_Fiber,Internet_Catv,Internet_Lan,Internet_Isdn,Internet_Free)value("
			else
				value=",("
			fi
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	#Water_Supply
			valueE=`echo ${237}`
			value=${value}${valueE}${valueC}
	#Sewer
			valueE=`echo ${238}`
			value=${value}${valueE}${valueC}
	#Gas
			valueE=`echo ${239}`
			value=${value}${valueE}${valueC}
	#All_Electric
			valueE=`echo ${240}`
			value=${value}${valueE}${valueC}
	#Eco_Will
			valueE=`echo ${241}`
			value=${value}${valueE}${valueC}
	#Eco_Cute
			valueE=`echo ${242}`
			value=${value}${valueE}${valueC}
	#Eco_Jaws
			valueE=`echo ${243}`
			value=${value}${valueE}${valueC}
	#Solar_Power
			valueE=`echo ${244}`
			value=${value}${valueE}${valueC}
	#Occupancy_Children
			valueE=`echo ${245}`
			value=${value}${valueE}${valueC}
	#Occupancy_Instrument
			valueE=`echo ${246}`
			value=${value}${valueE}${valueC}
	#Occupancy_Office
			valueE=`echo ${247}`
			value=${value}${valueE}${valueC}
	#Occupancy_Two_Person
			valueE=`echo ${248}`
			value=${value}${valueE}${valueC}
	#Occupancy_Family
			valueE=`echo ${249}`
			value=${value}${valueE}${valueC}
	#Occupancy_Sale_In_Lots
			valueE=`echo ${250}`
			value=${value}${valueE}${valueC}
	#Occupancy_Excellent_People_Wage
			valueE=`echo ${251}`
			value=${value}${valueE}${valueC}
	#Occupancy_Student
			valueE=`echo ${252}`
			value=${value}${valueE}${valueC}
	#Occupancy_Single_Person
			valueE=`echo ${253}`
			value=${value}${valueE}${valueC}
	#Occupancy_Elderly
			valueE=`echo ${254}`
			value=${value}${valueE}${valueC}
	#Occupancy_Corporation
			valueE=`echo ${255}`
			value=${value}${valueE}${valueC}
	#Occupancy_Pet
			valueE=`echo ${256}`
			value=${value}${valueE}${valueC}
	#Occupancy_Room_Share
			valueE=`echo ${257}`
			value=${value}${valueE}${valueC}
	#Occupancy_Sex
			valueE=`echo ${258}`
			value=${value}${valueE}${valueC}
	#Occupancy_Alien
			valueE=`echo ${259}`
			value=${value}${valueE}${valueC}
	#Management_From
			valueE=`echo ${260}`
			value=${value}${valueE}${valueC}
	#Emergency_Response
			valueE=`echo ${261}`
			value=${value}${valueE}${valueC}
	#Strengthening_District
			valueE=`echo ${262}`
			value=${value}${valueE}${valueC}
	#Intercom
			valueE=`echo ${263}`
			value=${value}${valueE}${valueC}
	#Security_Camera
			valueE=`echo ${264}`
			value=${value}${valueE}${valueC}
	#Security_Motion_Sensors
			valueE=`echo ${265}`
			value=${value}${valueE}${valueC}
	#Security_Fingerprint_Authentication
			valueE=`echo ${266}`
			value=${value}${valueE}${valueC}
	#Security_Vein_Authentication
			valueE=`echo ${267}`
			value=${value}${valueE}${valueC}
	#Security_Key_Card
			valueE=`echo ${268}`
			value=${value}${valueE}${valueC}
	#Security_Key
			valueE=`echo ${269}`
			value=${value}${valueE}${valueC}
	#Security_Door_Touch_Key
			valueE=`echo ${270}`
			value=${value}${valueE}${valueC}
	#Security_Door_Remote_Control_Key
			valueE=`echo ${271}`
			value=${value}${valueE}${valueC}
	#Security_Dimple_Key
			valueE=`echo ${272}`
			value=${value}${valueE}${valueC}
	#Security_Double_Lock
			valueE=`echo ${273}`
			value=${value}${valueE}${valueC}
	#Security_Electronic_Lock
			valueE=`echo ${274}`
			value=${value}${valueE}${valueC}
	#Security_Auto_Lock
			valueE=`echo ${275}`
			value=${value}${valueE}${valueC}
	#Security_Shutter
			valueE=`echo ${276}`
			value=${value}${valueE}${valueC}
	#Security_Glass
			valueE=`echo ${277}`
			value=${value}${valueE}${valueC}
	#Fire_Alarm_System
			valueE=`echo ${278}`
			value=${value}${valueE}${valueC}
	#Common_Use_Delivery_Box
			valueE=`echo ${279}`
			value=${value}${valueE}${valueC}
	#Common_Use_Cleaning_Box
			valueE=`echo ${280}`
			value=${value}${valueE}${valueC}
	#Common_Use_Garbage_Storage
			valueE=`echo ${281}`
			value=${value}${valueE}${valueC}
	#Common_Use_Parking_Bicycles
			valueE=`echo ${282}`
			value=${value}${valueE}${valueC}
	#Common_Use_Motorcycle_parking
			valueE=`echo ${283}`
			value=${value}${valueE}${valueC}
	#Common_Use_Athletic
			valueE=`echo ${284}`
			value=${value}${valueE}${valueC}
	#Common_Use_Pool
			valueE=`echo ${285}`
			value=${value}${valueE}${valueC}
	#Common_Use_Kids_Room
			valueE=`echo ${286}`
			value=${value}${valueE}${valueC}
	#Common_Use_Launderette
			valueE=`echo ${287}`
			value=${value}${valueE}${valueC}
	#Common_Use_Theater_Room
			valueE=`echo ${288}`
			value=${value}${valueE}${valueC}
	#Common_Use_Pet_Only_Facility
			valueE=`echo ${289}`
			value=${value}${valueE}${valueC}
	#Common_Use_Big_Bath
			valueE=`echo ${290}`
			value=${value}${valueE}${valueC}
	#Common_Use_On_Site_Playground
			valueE=`echo ${291}`
			value=${value}${valueE}${valueC}
	#Common_Use_Set_Post
			valueE=`echo ${292}`
			value=${value}${valueE}${valueC}
	#Common_Use_Sunny
			valueE=`echo ${293}`
			value=${value}${valueE}${valueC}
	#Light_court
			valueE=`echo ${294}`
			value=${value}${valueE}${valueC}
	#Two_surface_lighting
			valueE=`echo ${295}`
			value=${value}${valueE}${valueC}
	#Three_surface_lighting
			valueE=`echo ${296}`
			value=${value}${valueE}${valueC}
	#All_Rooms_lighting
			valueE=`echo ${297}`
			value=${value}${valueE}${valueC}
	#Three_way_House
			valueE=`echo ${298}`
			value=${value}${valueE}${valueC}
	#Four_way_House
			valueE=`echo ${299}`
			value=${value}${valueE}${valueC}
	#Without_Front_Building
			valueE=`echo ${300}`
			value=${value}${valueE}${valueC}
	#All_Rooms_Direction
			valueE=`echo ${301}`
			value=${value}${valueE}${valueC}
	#Second_Floor_Ldk
			valueE=`echo ${302}`
			value=${value}${valueE}${valueC}
	#Second_Floor_Living
			valueE=`echo ${303}`
			value=${value}${valueE}${valueC}
	#Corner_Room
			valueE=`echo ${304}`
			value=${value}${valueE}${valueC}
	#Top_Floor
			valueE=`echo ${305}`
			value=${value}${valueE}${valueC}
	#Onsen
			valueE=`echo ${306}`
			value=${value}${valueE}${valueC}
	#Designers
			valueE=`echo ${307}`
			value=${value}${valueE}${valueC}
	#Good_Ventilation
			valueE=`echo ${308}`
			value=${value}${valueE}${valueC}
	#Good_View
			valueE=`echo ${309}`
			value=${value}${valueE}${valueC}
	#Rooms_Facing_South
			valueE=`echo ${310}`
			value=${value}${valueE}${valueC}
	#Barrier_Free
			valueE=`echo ${311}`
			value=${value}${valueE}${valueC}
	#Slope
	#Distribution
			valueE=`echo ${312}`
			value=${value}${valueE}${valueC}
	#Flooring
			valueE=`echo ${313}`
			value=${value}${valueE}${valueC}
	#Bay_Window
			valueE=`echo ${314}`
			value=${value}${valueE}${valueC}
	#Skylight
			valueE=`echo ${315}`
			value=${value}${valueE}${valueC}
	#Blow_By
			valueE=`echo ${316}`
			value=${value}${valueE}${valueC}
	#Maisonette
			valueE=`echo ${317}`
			value=${value}${valueE}${valueC}
	#Under_Ground_Room
			valueE=`echo ${318}`
			value=${value}${valueE}${valueC}
	#Loft
			valueE=`echo ${319}`
			value=${value}${valueE}${valueC}
	#Breadth_Loft
			valueE=`echo ${320}`
			value=${value}${valueE}${valueC}
	#Free_Space
			valueE=`echo ${321}`
			value=${value}${valueE}${valueC}
	#Sunroom
			valueE=`echo ${322}`
			value=${value}${valueE}${valueC}
	#Soundproof_Room
			valueE=`echo ${323}`
			value=${value}${valueE}${valueC}
	#Cushion_Floor
			valueE=`echo ${324}`
			value=${value}${valueE}${valueC}
	#Entrance_Porch
			valueE=`echo ${325}`
			value=${value}${valueE}${valueC}
	#Entrance_Hall
			valueE=`echo ${326}`
			value=${value}${valueE}${valueC}
	#Entrance_Handrail
			valueE=`echo ${327}`
			value=${value}${valueE}${valueC}
	#Carpet_Upholstery
			valueE=`echo ${328}`
			value=${value}${valueE}${valueC}
	#Bed
			valueE=`echo ${329}`
			value=${value}${valueE}${valueC}
	#Storage_Bed
			valueE=`echo ${330}`
			value=${value}${valueE}${valueC}
	#Lighting_Equipment
			valueE=`echo ${331}`
			value=${value}${valueE}${valueC}
	#Auto_Light
			valueE=`echo ${332}`
			value=${value}${valueE}${valueC}
	#Indirect_Lighting
			valueE=`echo ${333}`
			value=${value}${valueE}${valueC}
	#All_Rooms_With_Lighting
			valueE=`echo ${334}`
			value=${value}${valueE}${valueC}
	#Curtains
			valueE=`echo ${335}`
			value=${value}${valueE}${valueC}
	#Blind_With
			valueE=`echo ${336}`
			value=${value}${valueE}${valueC}
	#Roll_Screen_With
			valueE=`echo ${337}`
			value=${value}${valueE}${valueC}
	#Fully_Furnished
			valueE=`echo ${338}`
			value=${value}${valueE}${valueC}
	#All_Rooms_Flooring
			valueE=`echo ${339}`
			value=${value}${valueE}${valueC}
	#South_Side_Living
			valueE=`echo ${340}`
			value=${value}${valueE}${valueC}
	#Double_Glazing
			valueE=`echo ${341}`
			value=${value}${valueE}${valueC}
	#Veranda_Balcony
			valueE=`echo ${342}`
			value=${value}${valueE}${valueC}
	#Balcony_Breadth
			valueE=`echo ${343}`
			value=${value}${valueE}${valueC}
	#Terrace
			valueE=`echo ${344}`
			value=${value}${valueE}${valueC}
	#Roof_Balcony
			valueE=`echo ${345}`
			value=${value}${valueE}${valueC}
	#Two_wey_Balcony
			valueE=`echo ${346}`
			value=${value}${valueE}${valueC}
	#Two_Side_Balcony
			valueE=`echo ${347}`
			value=${value}${valueE}${valueC}
	#Three_Surface_Balcony
			valueE=`echo ${348}`
			value=${value}${valueE}${valueC}
	#L_Shaped_Balcony
			valueE=`echo ${349}`
			value=${value}${valueE}${valueC}
	#Inner_Balcony
			valueE=`echo ${350}`
			value=${value}${valueE}${valueC}
	#Wood_Deck
			valueE=`echo ${351}`
			value=${value}${valueE}${valueC}
	#Outdoor_Power
			valueE=`echo ${352}`
			value=${value}${valueE}${valueC}
	#Shutter_Shutters
			valueE=`echo ${353}`
			value=${value}${valueE}${valueC}
	#Private_Garden
			valueE=`echo ${354}`
			value=${value}${valueE}${valueC}
	#Courtyard
			valueE=`echo ${355}`
			value=${value}${valueE}${valueC}
	#Spot_Garden
			valueE=`echo ${356}`
			value=${value}${valueE}${valueC}
	#Storage_Space
			valueE=`echo ${357}`
			value=${value}${valueE}${valueC}
	#Closet
			valueE=`echo ${358}`
			value=${value}${valueE}${valueC}
	#Closet_number
			valueE=`echo ${359}`
			value=${value}${valueE}${valueC}
	#Walk_In_Closet
			valueE=`echo ${360}`
			value=${value}${valueE}${valueC}
	#Walk_In_Closet_Number
			valueE=`echo ${361}`
			value=${value}${valueE}${valueC}
	#Shoes_Walk_in_Closet
			valueE=`echo ${362}`
			value=${value}${valueE}${valueC}
	#Walk_Through_Closet
			valueE=`echo ${363}`
			value=${value}${valueE}${valueC}
	#Under_Floor_Storage
			valueE=`echo ${364}`
			value=${value}${valueE}${valueC}
	#Attic_Storage
			valueE=`echo ${365}`
			value=${value}${valueE}${valueC}
	#Under_Stairs_Storage
			valueE=`echo ${366}`
			value=${value}${valueE}${valueC}
	#Entrance_Storage
			valueE=`echo ${367}`
			value=${value}${valueE}${valueC}
	#Lift_Wall_Storage
			valueE=`echo ${368}`
			value=${value}${valueE}${valueC}
	#All_Living_Room_Storage
			valueE=`echo ${369}`
			value=${value}${valueE}${valueC}
	#Storeroom
			valueE=`echo ${370}`
			value=${value}${valueE}${valueC}
	#Trunk_Room
			valueE=`echo ${371}`
			value=${value}${valueE}${valueC}
	#Warehouse
			valueE=`echo ${372}`
			value=${value}${valueE}${valueC}
	#Armoire
			valueE=`echo ${373}`
			value=${value}${valueE}${valueC}
	#Shoes_Box
			valueE=`echo ${374}`
			value=${value}${valueE}${valueC}
	#Ceiling_High_Clogs_Box
			valueE=`echo ${375}`
			value=${value}${valueE}${valueC}
	#Bus_Toilet
			valueE=`echo ${376}`
			value=${value}${valueE}${valueC}
	#Shower
			valueE=`echo ${377}`
			value=${value}${valueE}${valueC}
	#Reheating
			valueE=`echo ${378}`
			value=${value}${valueE}${valueC}
	#Bathroom_Dryer
			valueE=`echo ${379}`
			value=${value}${valueE}${valueC}
	#Auto_Bus
			valueE=`echo ${380}`
			value=${value}${valueE}${valueC}
	#Dressing_Room
			valueE=`echo ${381}`
			value=${value}${valueE}${valueC}
	#Hot_Water_Supply
			valueE=`echo ${382}`
			value=${value}${valueE}${valueC}
	#Three_Point_Hot_Water_Supply
			valueE=`echo ${383}`
			value=${value}${valueE}${valueC}
	#Tv_With_Bus
			valueE=`echo ${384}`
			value=${value}${valueE}${valueC}
	#With_Audio_Bus
			valueE=`echo ${385}`
			value=${value}${valueE}${valueC}
	#Sauna
			valueE=`echo ${386}`
			value=${value}${valueE}${valueC}
	#Whirlpool
			valueE=`echo ${387}`
			value=${value}${valueE}${valueC}
	#Mist_Sauna
			valueE=`echo ${388}`
			value=${value}${valueE}${valueC}
	#One_Tsubo
			valueE=`echo ${389}`
			value=${value}${valueE}${valueC}
	#Two_Places
			valueE=`echo ${390}`
			value=${value}${valueE}${valueC}
	#Cleaning_Toilet_Seat
			valueE=`echo ${391}`
			value=${value}${valueE}${valueC}
	#Toilet_Seat
			valueE=`echo ${392}`
			value=${value}${valueE}${valueC}
	#Tankless
			valueE=`echo ${393}`
			value=${value}${valueE}${valueC}
	#Two_Places_Toilet
			valueE=`echo ${394}`
			value=${value}${valueE}${valueC}
	#Three_Places_Toilet
			valueE=`echo ${395}`
			value=${value}${valueE}${valueC}
	#The_Door_To_The_Toilet
			valueE=`echo ${396}`
			value=${value}${valueE}${valueC}
	#The_Window_To_The_Washroom
			valueE=`echo ${397}`
			value=${value}${valueE}${valueC}
	#Wash_Basin
			valueE=`echo ${398}`
			value=${value}${valueE}${valueC}
	#Seperate
			valueE=`echo ${399}`
			value=${value}${valueE}${valueC}
	#Shampoo_Dresser
			valueE=`echo ${400}`
			value=${value}${valueE}${valueC}
	#Tree_Sided_mirror_With_Vanity
			valueE=`echo ${401}`
			value=${value}${valueE}${valueC}
	#Two_bowl_Basin
			valueE=`echo ${402}`
			value=${value}${valueE}${valueC}
	#Washing_Machine
			valueE=`echo ${403}`
			value=${value}${valueE}${valueC}
	#Washing_Machine_Storage
			valueE=`echo ${404}`
			value=${value}${valueE}${valueC}
	#Stove_Installation_Possible
			valueE=`echo ${405}`
			value=${value}${valueE}${valueC}
	#Stove_Installed
			if [ ${406} = "0" ]
			then
				valueE="1"
			else
				valueE="0"
			fi
			value=${value}${valueE}${valueC}
	#Stove_Installation_Number
			valueE=`echo ${406}`
			value=${value}${valueE}${valueC}
	#Stove_Type
			valueE=`echo ${407}`
			value=${value}${valueE}${valueC}
	#Glass_Top_Stove
			valueE=`echo ${408}`
			value=${value}${valueE}${valueC}
	#Grill
			valueE=`echo ${409}`
			value=${value}${valueE}${valueC}
	#Gas_Oven_Range
			valueE=`echo ${410}`
			value=${value}${valueE}${valueC}
	#Gas_Oven
			valueE=`echo ${411}`
			value=${value}${valueE}${valueC}
	#Two_Places_Kitchen
			valueE=`echo ${412}`
			value=${value}${valueE}${valueC}
	#Back_Door
			valueE=`echo ${413}`
			value=${value}${valueE}${valueC}
	#Water_Filter
			valueE=`echo ${414}`
			value=${value}${valueE}${valueC}
	#Dishwashers_Dryer
			valueE=`echo ${415}`
			value=${value}${valueE}${valueC}
	#Pantry
			valueE=`echo ${416}`
			value=${value}${valueE}${valueC}
	#Cupboard
			valueE=`echo ${417}`
			value=${value}${valueE}${valueC}
	#Microwave
			valueE=`echo ${418}`
			value=${value}${valueE}${valueC}
	#Fridge
			valueE=`echo ${419}`
			value=${value}${valueE}${valueC}
	#System_Kitchen
			valueE=`echo ${420}`
			value=${value}${valueE}${valueC}
	#Kitchen_Counter
			valueE=`echo ${421}`
			value=${value}${valueE}${valueC}
	#Kitchen_Face_To_Face
			valueE=`echo ${422}`
			value=${value}${valueE}${valueC}
	#L_shaped_Kitchen
			valueE=`echo ${423}`
			value=${value}${valueE}${valueC}
	#Island_Kitchen
			valueE=`echo ${424}`
			value=${value}${valueE}${valueC}
	#Only_Cooling
			valueE=`echo ${425}`
			value=${value}${valueE}${valueC}
	#Heating_Only
			valueE=`echo ${426}`
			value=${value}${valueE}${valueC}
	#Heating_Type
			valueE=`echo ${427}`
			value=${value}${valueE}${valueC}
	#Air_Conditioning
			valueE=`echo ${428}`
			value=${value}${valueE}${valueC}
	#Air_Conditioning_Number
			valueE=`echo ${429}`
			value=${value}${valueE}${valueC}
	#Floor_Heating
			valueE=`echo ${430}`
			value=${value}${valueE}${valueC}
	#Ventilation_System
			valueE=`echo ${431}`
			value=${value}${valueE}${valueC}
	#Central_Air_Conditioning
			valueE=`echo ${432}`
			value=${value}${valueE}${valueC}
	#Passive_Ventilation
			valueE=`echo ${433}`
			value=${value}${valueE}${valueC}
	#Built_In_Air_Conditioning
			valueE=`echo ${434}`
			value=${value}${valueE}${valueC}
	#Hot_Water_Heater_Room
			valueE=`echo ${435}`
			value=${value}${valueE}${valueC}
	#Please_Stand_Digging
			valueE=`echo ${436}`
			value=${value}${valueE}${valueC}
	#Underfloor_Ventilation
			valueE=`echo ${437}`
			value=${value}${valueE}${valueC}
	#Heating
			valueE=`echo ${438}`
			value=${value}${valueE}${valueC}
	#Ceiling_Fan
			valueE=`echo ${439}`
			value=${value}${valueE}${valueC}
	#Line_Bs
			valueE=`echo ${440}`
			value=${value}${valueE}${valueC}
	#Line_Cs
			valueE=`echo ${441}`
			value=${value}${valueE}${valueC}
	#Line_Catv
			valueE=`echo ${442}`
			value=${value}${valueE}${valueC}
	#Line_Wired
			valueE=`echo ${443}`
			value=${value}${valueE}${valueC}
	#Line_Uhf
			valueE=`echo ${444}`
			value=${value}${valueE}${valueC}
	#Internet_Optical_Fiber
			valueE=`echo ${445}`
			value=${value}${valueE}${valueC}
	#Internet_Catv
			valueE=`echo ${446}`
			value=${value}${valueE}${valueC}
	#Internet_Lan
			valueE=`echo ${447}`
			value=${value}${valueE}${valueC}
	#Internet_Isdn
			valueE=`echo ${448}`
			value=${value}${valueE}${valueC}
	#Internet_Free
			valueE=`echo ${449}`
			value=${value}${valueE}
	
			if [ "${numLine}" -lt 1000 ]
			then
				valueB=')'
			else
				valueB=');'
			fi
			value=${value}${valueB}
			echo ${value} >> insert_equipment.sql


	##T_ChatchCopy
			if [ "${numLine}" = 1 ]
			then
				value="insert into T_ChatchCopy ( Housing_Id,Chatch_Copy,Remarks,Memo)VALUE("
			else
				value=',('
			fi
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	#Chatch_Copy
			valueE=`echo ${450} | sed -e 's/\n/\s/g'`
			value=${value}${valueE}${valueC}
	#Remarks
			valueE=`echo ${451} | sed -e 's/\n/\s/g'`
			value=${value}${valueE}${valueC}
	#Memo
			valueE=`echo ${452} | sed -e 's/\n/\s/g'`
			value=${value}${valueE}
	
	
			if [ "${numLine}" -lt 1000 ]
			then
				valueB=')'
			else
				valueB=');'
			fi
			value=${value}${valueB}
			echo ${value} >> insert_chatchcopy.sql


	##T_Article
			if [ "${numLine}" = 1 ]
			then
				value="insert into T_Article ( Housing_Id,Article_FLG)VALUE("
			else
				value=',('
			fi
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	#Article_FLG
			valueE="0"
			value=${value}${valueE}

			if [ "${numLine}" -lt 1000 ]
			then
				valueB=')'
			else
				valueB=');'
			fi
			value=${value}${valueB}
			echo ${value} >> insert_article.sql


	##T_SearchHousing
			if [ ${numLine} = 1 ]
			then
				value="insert into T_SearchHousing (Housing_Id,ADD_Pref,ADD_Shiku,ADD_Choson,ADD_Aza,ADD_Branch,Branch_FLG,Rent,Common_Service_Fee,Common_Service_Fee_Classification,Security_Deposits,Amount,Security_Deposits_Classification,Key_Money,Key_Money_Classification,Floor_Number,Floor_Plan,Updated,Built_Years,Built_New_FLG,Footprint_Square_Meters,Floor,Ground_Floor,Chatch_Copy,Structure,Property_Type,Flooring,Loft,Bay_Window,Veranda_Balcony,Private_Garden,Top_Floor,Corner_Room,Facing_South,Air_Conditioning,Floor_Heating,Heating_Type,Stove_Type,Stove_Installation_Number,System_Kitchen,Kitchen_Counter,Bus_Toilet,Shower,Reheating,Bathroom_Dryer,Cleaning_Toilet_Seat,Shoes_Box,Walk_In_Closet,Under_Floor_Storage,Trunk_Room,Intercom,Security_Auto_Lock,Security_Dimple_Key,Security_Key_Card,Security_Camera,Common_Use_Delivery_Box,Management_From,Move_In_Date,Occupancy_Sex,Occupancy_Single_Person,Occupancy_Two_Person,Occupancy_Family,Occupancy_Room_Share,Occupancy_Pet,Occupancy_Office,Occupancy_Alien,Occupancy_Instrument,Occupancy_Elderly,Guarantor,Broadband,Line_Cs,Line_Bs,Line_Catv,Line_Wired,Elevator,Washing_Machine_Storage,Shampoo_Dresser,Water_Filter,All_Electric,Fully_Furnished,Gas,Parking,Site_Stop_Number,Common_Use_Parking_Bicycles,Common_Use_Motorcycle_parking,Ventilation_System,Reform,Free_Rent,Internet_Free,Image_G_URL,Image_M_URL,Contract_Type,Entrance_Handrail,Barrier_Free,Shop_Id,Shop_Rank,View_FLG,Delete_FLG)value("
			else
				value=',('
			fi
	
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	#ADD_Pref
			valueE=`echo ${13}`
			value=${value}${valueE}${valueC}
	#ADD_Shiku
			valueE=`echo ${14}`
			value=${value}${valueE}${valueC}
	#ADD_Choson
			valueE=`echo ${15}`
			value=${value}${valueE}${valueC}
	#ADD_Aza
			valueE=`echo ${16}`
			value=${value}${valueE}${valueC}
	#ADD_Branch
			valueE=`echo ${17}`
			value=${value}${valueE}${valueC}
	#Branch_FLG
			valueE=`echo ${18}`
			value=${value}${valueE}${valueC}
	#Rent
			valueE=`echo ${145}`
			value=${value}${valueE}${valueC}
	#Common_Service_Fee
			valueE=`echo ${146}`
			value=${value}${valueE}${valueC}
	#Common_Service_Fee_Classification
			valueE="0"
			value=${value}${valueE}${valueC}
	#Security_Deposits
			valueE=`echo ${147}`
			value=${value}${valueE}${valueC}
	#Amount
			valueE=`echo ${148}`
			value=${value}${valueE}${valueC}
	#Security_Deposits_Classification
			valueE=`echo ${149}`
			value=${value}${valueE}${valueC}
	#Key_Money
			valueE=`echo ${157}`
			value=${value}${valueE}${valueC}
	#Key_Money_Classification
			valueE=`echo ${158}`
			value=${value}${valueE}${valueC}
	#Floor_Number
			valueE=`echo ${213}`
			value=${value}${valueE}${valueC}
	#Floor_Plan
			valueE=`echo ${214}`
			value=${value}${valueE}${valueC}
	#Updated
			valueE=`echo ${189}`
			value=${value}${valueE}${valueC}
	#Built_Years
			valueX=`echo ${53} | sed -e 's/"//g'`
			valueY=`echo ${54} | sed -e 's/"//g'`
			valueZ=`echo ${55} | sed -e 's/"//g'`
			if [ "${valueY}" -gt 0 -a "${valueY}" -lt 10 ]
			then
			  	  valueY=`echo 0${valueY}`
			else
			 	   valueY=`echo ${valueY}`
			fi
			if [ -n "${valueZ}" ]
			then
				if [ "${valueZ}" -gt 0 -a "${valueZ}" -lt 32 ]
				then
					valueZ=`echo ${valueZ}`
				else
					valueZ="00"
				fi
			else
			 	   valueZ="00"
			fi
			valueE=`echo ${valueX}${valueY}${valueZ} | sed -e 's/"//g'`
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Built_New_FLG
			valueE=`echo ${56}`
			value=${value}${valueE}${valueC}
	#Footprint_Square_Meters
			valueE=`echo ${235}`
			value=${value}${valueE}${valueC}
	#Floor
			valueE=`echo ${133}`
			value=${value}${valueE}${valueC}
	#Ground_Floor
			valueE=`echo ${58}`
			value=${value}${valueE}${valueC}
	#Chatch_Copy
			valueE=`echo ${450}`
			value=${value}${valueE}${valueC}
	#Structure
			valueE=`echo ${60}`
			value=${value}${valueE}${valueC}
	#Property_Type
			valueE=`echo ${7}`
			value=${value}${valueE}${valueC}
	#Flooring
			valueE=`echo ${313}`
			value=${value}${valueE}${valueC}
	#Loft
			valueE=`echo ${319}`
			value=${value}${valueE}${valueC}
	#Bay_Window
			valueE=`echo ${314}`
			value=${value}${valueE}${valueC}
	#Veranda_Balcony
			valueE=`echo ${342}`
			value=${value}${valueE}${valueC}
	#Private_Garden
			valueE=`echo ${354}`
			value=${value}${valueE}${valueC}
	#Top_Floor
			valueE=`echo ${305}`
			value=${value}${valueE}${valueC}
	#Corner_Room
			valueE=`echo ${304}`
			value=${value}${valueE}${valueC}
	#Facing_South
			if [ "${304}" = "1" ]
			then
				valueE=`echo ${304}`
			else
				valueE=0
			fi
			value=${value}${valueE}${valueC}
	#Air_Conditioning
			valueE=`echo ${428}`
			value=${value}${valueE}${valueC}
	#Floor_Heating
			valueE=`echo ${430}`
			value=${value}${valueE}${valueC}
	#Heating_Type
			valueE=`echo ${427}`
			value=${value}${valueE}${valueC}
	#Stove_Type
			valueE=`echo ${407}`
			value=${value}${valueE}${valueC}
	#Stove_Installation_Number
			valueE=`echo ${406}`
			value=${value}${valueE}${valueC}
	#System_Kitchen
			valueE=`echo ${420}`
			value=${value}${valueE}${valueC}
	#Kitchen_Counter
			valueE=`echo ${421}`
			value=${value}${valueE}${valueC}
	#Bus_Toilet
			valueE=`echo ${376}`
			value=${value}${valueE}${valueC}
	#Shower
			valueE=`echo ${377}`
			value=${value}${valueE}${valueC}
	#Reheating
			valueE=`echo ${378}`
			value=${value}${valueE}${valueC}
	#Bathroom_Dryer
			valueE=`echo ${379}`
			value=${value}${valueE}${valueC}
	#Cleaning_Toilet_Seat
			valueE=`echo ${391}`
			value=${value}${valueE}${valueC}
	#Shoes_Box
			valueE=`echo ${374}`
			value=${value}${valueE}${valueC}
	#Walk_In_Closet
			valueE=`echo ${360}`
			value=${value}${valueE}${valueC}
	#Under_Floor_Storage
			valueE=`echo ${364}`
			value=${value}${valueE}${valueC}
	#Trunk_Room
			valueE=`echo ${371}`
			value=${value}${valueE}${valueC}
	#Intercom
			valueE=`echo ${263}`
			value=${value}${valueE}${valueC}
	#Security_Auto_Lock
			valueE=`echo ${275}`
			value=${value}${valueE}${valueC}
	#Security_Dimple_Key
			valueE=`echo ${272}`
			value=${value}${valueE}${valueC}
	#Security_Key_Card
			valueE=`echo ${268}`
			value=${value}${valueE}${valueC}
	#Security_Camera
			valueE=`echo ${264}`
			value=${value}${valueE}${valueC}
	#Common_Use_Delivery_Box
			valueE=`echo ${279}`
			value=${value}${valueE}${valueC}
	#Management_From
			valueE=`echo ${260}`
			value=${value}${valueE}${valueC}
	#Move_In_Date
			valueE=`echo ${136}`
			value=${value}${valueE}${valueC}
	#Occupancy_Sex
			valueE=`echo ${258}`
			value=${value}${valueE}${valueC}
	#Occupancy_Single_Person
			valueE=`echo ${253}`
			value=${value}${valueE}${valueC}
	#Occupancy_Two_Person
			valueE=`echo ${248}`
			value=${value}${valueE}${valueC}
	#Occupancy_Family
			valueE=`echo ${249}`
			value=${value}${valueE}${valueC}
	#Occupancy_Room_Share
			valueE=`echo ${257}`
			value=${value}${valueE}${valueC}
	#Occupancy_Pet
			valueE=`echo ${256}`
			value=${value}${valueE}${valueC}
	#Occupancy_Office
			valueE=`echo ${247}`
			value=${value}${valueE}${valueC}
	#Occupancy_Alien
			valueE=`echo ${259}`
			value=${value}${valueE}${valueC}
	#Occupancy_Instrument
			valueE=`echo ${246}`
			value=${value}${valueE}${valueC}
	#Occupancy_Elderly
			valueE=`echo ${254}`
			value=${value}${valueE}${valueC}
	#Guarantor
			valueE=`echo ${193}`
			value=${value}${valueE}${valueC}
	#Broadband
			if [ "${445}" = "1" -o "${446}" = "1" -o "${447}" = "1" -o "${448}" = "1" ]
			then
				valueE=1
			else
				valueE=0
			fi
			value=${value}${valueE}${valueC}
	#Line_Cs
			valueE=`echo ${441}`
			value=${value}${valueE}${valueC}
	#Line_Bs
			valueE=`echo ${440}`
			value=${value}${valueE}${valueC}
	#Line_Catv
			valueE=`echo ${442}`
			value=${value}${valueE}${valueC}
	#Line_Wired
			valueE=`echo ${443}`
			value=${value}${valueE}${valueC}
	#Elevator
			valueE=`echo ${92}`
			value=${value}${valueE}${valueC}
	#Washing_Machine_Storage
			valueE=`echo ${404}`
			value=${value}${valueE}${valueC}
	#Shampoo_Dresser
			valueE=`echo ${400}`
			value=${value}${valueE}${valueC}
	#Water_Filter
			valueE=`echo ${414}`
			value=${value}${valueE}${valueC}
	#All_Electric
			valueE=`echo ${240}`
			value=${value}${valueE}${valueC}
	#Fully_Furnished
			valueE=`echo ${338}`
			value=${value}${valueE}${valueC}
	#Gas
			valueE=`echo ${239}`
			value=${value}${valueE}${valueC}
	#Parking
			valueE=`echo ${6}`
			value=${value}${valueE}${valueC}
	#Site_Stop_Number
			valueE=`echo ${63}`
			value=${value}${valueE}${valueC}
	#Common_Use_Parking_Bicycles
			valueE=`echo ${282}`
			value=${value}${valueE}${valueC}
	#Common_Use_Motorcycle_parking
			valueE=`echo ${283}`
			value=${value}${valueE}${valueC}
	#Ventilation_System
			valueE=`echo ${431}`
			value=${value}${valueE}${valueC}
	#Reform
			if [ -n ${141} -a -n ${142} ]
			then
				if [ ${141} = "1" -o ${142} = "1" ]
				then
					valueE=1
				else
					valueE=0
				fi
			else
				valueE=0
			fi
			value=${value}${valueE}${valueC}
	#Free_Rent
			valueE=`echo ${192}`
			value=${value}${valueE}${valueC}
	#Internet_Free
			valueE=`echo ${449}`
			value=${value}${valueE}${valueC}
	#Image_Number
	#Image_G_URL
			LINE=`echo "${455}" | sed -e 's/"//g'`
			if [ -n "${453}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Image_M_URL
			LINE=`echo "${458}" | sed -e 's/"//g'`
			if [ -n "${456}" -a -n "${LINE}" ]
			then
				if [ `echo "${LINE}" | cut -c 1-4 ` = "http" ]
				then
					valueE=${LINE}
				else
					valueE=${valueI}${LINE}
				fi
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Contract_Type
			valueE=`echo ${201}`
			value=${value}${valueE}${valueC}
	#Entrance_Handrail
			valueE=`echo ${327}`
			value=${value}${valueE}${valueC}
	#Slope
	#Barrier_Free
			valueE=`echo ${311}`
			value=${value}${valueE}${valueC}
	#Shop_Id
#			valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Id from T_ShopUser where Connected_2 = ${valueF} limit 1;"`
			valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Id from T_ShopUser where Connected_2 = ${valueF} limit 1;"`
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Shop_Rank
			valueE="0"
			value=${value}${valueE}${valueC}
	#View_FLG
			if [ "${10}" = "1" ]
			then
				valueE="1"
			else
				valueE="0"
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Delete_FLG
			if [ "${11}" = "1" ]
			then
				valueE="1"
			else
				valueE="0"
			fi
			value=${value}${valueD}${valueE}${valueD}
	
			if [ ${numLine} -lt 1000 ]
			then
				valueB=')'
			else
				valueB=');'
			fi
			value=${value}${valueB}
			echo ${value} >> insert_searchhousing.sql


	##T_SearchTraffic
			if [ "${numLine}" = 1 ]
			then
				value="insert into T_SearchTraffic (Housing_Id,Route_1,Station_1,Walk_Time_1,Bus_Line_1,Bus_Stop_1,Bus_Stop_Walk_1,Bus_Ride_Time_1,Other_Transport_1,Route_2,Station_2,Walk_Time_2,Bus_Line_2,Bus_Stop_2,Bus_Stop_Walk_2,Bus_Ride_Time_2,Other_Transport_2,Route_3,Station_3,Walk_Time_3,Bus_Line_3,Bus_Stop_3,Bus_Stop_Walk_3,Bus_Ride_Time_3,Other_Transport_3)value("
			else
				value=',('
			fi
	#Housing_Id
			value=${value}${valueD}${housing_Id}${valueD}${valueC}
	#Route_1
			valueH=`echo ${21}`
			valueI=`echo ${22}`
	
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ ${valueZ} != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Station_1
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Walk_Time_1
			valueE=`echo ${23}`
			value=${value}${valueE}${valueC}
	#Bus_Line_1
			valueE=`echo ${24}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_1
			valueE=`echo ${25}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_Walk_1
			valueE=`echo ${26}`
			value=${value}${valueE}${valueC}
	#Bus_Ride_Time_1
			valueE=`echo ${27}`
			value=${value}${valueE}${valueC}
	#Other_Transport_1
			valueE=`echo ${28}`
			value=${value}${valueE}${valueC}
	#Route_2
			valueH=`echo ${29}`
			valueI=`echo ${30}`
	
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ ${valueZ} != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Station_2
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Walk_Time_2
			valueE=`echo ${31}`
			value=${value}${valueE}${valueC}
	#Bus_Line_2
			valueE=`echo ${32}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_2
			valueE=`echo ${33}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_Walk_2
			valueE=`echo ${34}`
			value=${value}${valueE}${valueC}
	#Bus_Ride_Time_2
			valueE=`echo ${35}`
			value=${value}${valueE}${valueC}
	#Other_Transport_2
			valueE=`echo ${36}`
			value=${value}${valueE}${valueC}
	
	#Route_3
			valueH=`echo ${37}`
			valueI=`echo ${38}`
			valueZ=`echo ${valueH}-${valueI} | sed -e 's/"//g'`
			if [ ${valueZ} != "-" ]
			then
#				valueE=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
				valueE=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Kin_Cd from M_Train where Homes_Cd = \"${valueZ}\""`
			else
				valueE=""
			fi
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Station_3
			value=${value}${valueD}${valueE}${valueD}${valueC}
	#Walk_Time_3
			valueE=`echo ${39}`
			value=${value}${valueE}${valueC}
	#Bus_Line_3
			valueE=`echo ${40}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_3
			valueE=`echo ${41}`
			value=${value}${valueE}${valueC}
	#Bus_Stop_Walk_3
			valueE=`echo ${42}`
			value=${value}${valueE}${valueC}
	#Bus_Ride_Time_3
			valueE=`echo ${43}`
			value=${value}${valueE}${valueC}
	#Other_Transport_3
			valueE=`echo ${44}`
			value=${value}${valueE}

			if [ "${numLine}" -lt 1000 ]
			then
				valueB=')'
				numLine=`expr ${numLine} + 1`
			else
				valueB=');'
				numLine=1
			fi
			value=${value}${valueB}
			echo ${value} >> insert_searchtraffic.sql
		fi

	fi
done

if [ -e ./insert_housing.sql ]
then
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < insert_traffic.sql
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < insert_nearbyattractions.sql
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < insert_image.sql
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < insert_roominfo.sql
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < insert_equipment.sql
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < insert_chatchcopy.sql
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_chintai < insert_article.sql
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_search < insert_searchhousing.sql
	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn apahau_search < insert_searchtraffic.sql

#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < insert_traffic.sql
#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < insert_nearbyattractions.sql
#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < insert_image.sql
#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < insert_roominfo.sql
#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < insert_equipment.sql
#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < insert_chatchcopy.sql
#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < insert_article.sql
#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_search < insert_searchhousing.sql
#	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_search < insert_searchtraffic.sql

	###################################################################
	# 画像ファイルを本来の格納位置に移動する。（batch01 のみ）
	###################################################################
	#
	valueZ=`pwd`
	cd ${valueM}${valueL}
	mv * ${valueK}${valueL}
	cd ${valueZ}

else
	echo 処理対象データはありませんでした。
fi

#nohup /bin/sh delete_housing.sh &

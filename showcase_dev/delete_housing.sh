#!/bin/sh

if [ -e ./delete_housing.sql ]
then
	rm ./delete_housing.sql
fi

if [ -e ./delete_searchhousing.sql ]
then
	rm ./delete_searchhousing.sql
fi

valueC=','
valueD='"'

CSV_TARGET=./showcase_bukken.csv

cat $CSV_TARGET | while read valueData
do
	if [ -n "${valueData}" ]
	then

		# レコードタイプ
		type=`echo ${valueData} | cut -d " " -f 1 | sed -e 's/"//g'`
		# レコードタイプが 4:[物件情報削除] 以外は読み飛ばす
		if [ "${type}" != "4" ]
		then
			continue
		fi
		# 貴社サイト店舗ID
		Property_Management_Id=`echo ${valueData} | cut -d " " -f 3 | sed -e 's/"//g'`
		# 貴社物件管理番号
		Converter_User_Id=`echo ${valueData} | cut -d " " -f 4 | sed -e 's/"//g'`

#		echo 'type:['$type']'
#		echo 'Property_Management_Id:['$Property_Management_Id']'
#		echo 'Converter_User_Id:['$Converter_User_Id']'

		# 貴社サイト店舗IDおよび貴社物件管理番号に値があること
		if [ "${Property_Management_Id}" == "" -o "${Converter_User_Id}" == "" ]
		then
#			echo 貴社サイト店舗IDおよびおよび貴社物件管理番号に値がありません。処理をパスします。
			continue
		fi

		# ------------------------------------------------------------
		# 登録済みレコードの物件ID（PRIMARY KEY）を取得
		# ------------------------------------------------------------
		
		valueF="'"`echo ${Property_Management_Id}`"'"
		valueG="'"`echo ${Converter_User_Id}`"'"
		#Housing_Id=`mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai -e "select Housing_Id from T_Housing where Property_Management_Id = ${valueG} and Converter_User_Id = ${valueF};"`
		Housing_Id=`mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai -e "select Housing_Id from T_Housing where Property_Management_Id = ${valueG} and Converter_User_Id = ${valueF};"`

		# ------------------------------------------------------------
		# 登録済みレコードがなければ処理しない
		# ------------------------------------------------------------
		if [ "${Housing_Id}" == "" ]
		then
			echo T_Housing にレコードがありません。処理をパスします。
			continue
		fi

		##############################################################
		# T_Housing
		##############################################################
		value="delete from T_Housing "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_housing.sql

		##############################################################
		# T_Traffic
		##############################################################
		value="delete from T_Traffic "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_housing.sql

		##############################################################
		# T_RoomInfo
		##############################################################
		value="delete from T_RoomInfo "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_housing.sql

		##############################################################
		# T_Equipment
		##############################################################
		value="delete from T_Equipment "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_housing.sql

		##############################################################
		# T_ChatchCopy
		##############################################################
		value="delete from T_ChatchCopy "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_housing.sql

		##############################################################
		# T_NearbyAttractions
		##############################################################
		value="delete from T_NearbyAttractions "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_housing.sql

		##############################################################
		# T_Image
		##############################################################
		value="delete from T_Image "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_housing.sql

		##############################################################
		# T_Article
		##############################################################
		value="delete from T_Article "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_housing.sql

		##############################################################
		# T_SearchHousing
		##############################################################
		value="delete from T_SearchHousing "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_searchhousing.sql

		##############################################################
		# T_SearchTraffic
		##############################################################
		value="delete from T_SearchTraffic "
		valueB="WHERE Housing_Id="
		value=${value}${valueB}${valueD}${Housing_Id}${valueD}
		valueB=";"
		value=${value}${valueB}

		echo ${value} >> delete_searchhousing.sql
	fi
done

if [ -e ./delete_housing.sql ]
then
#	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_chintai < delete_housing.sql
	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_chintai < delete_housing.sql
fi

if [ -e ./delete_searchhousing.sql ]
then
#	mysql -h apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com -uapahau -pFByPy7Nn -N apahau_search < delete_searchhousing.sql
	mysql -h localhost -u apadb -pdak5t3a3ahfi5 -N apahau_search < delete_searchhousing.sql
fi
